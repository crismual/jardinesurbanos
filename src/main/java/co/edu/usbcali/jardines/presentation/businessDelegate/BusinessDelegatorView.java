package co.edu.usbcali.jardines.presentation.businessDelegate;

import co.edu.usbcali.jardines.model.Adjuntosxresena;
import co.edu.usbcali.jardines.model.Adjuntoxpregunta;
import co.edu.usbcali.jardines.model.Auditoria;
import co.edu.usbcali.jardines.model.Especie;
import co.edu.usbcali.jardines.model.Galeria;
import co.edu.usbcali.jardines.model.GaleriaId;
import co.edu.usbcali.jardines.model.Jardin;
import co.edu.usbcali.jardines.model.JardinEspecie;
import co.edu.usbcali.jardines.model.Pregunta;
import co.edu.usbcali.jardines.model.Resena;
import co.edu.usbcali.jardines.model.Respuesta;
import co.edu.usbcali.jardines.model.Respuestaxusuario;
import co.edu.usbcali.jardines.model.TipoUsuario;
import co.edu.usbcali.jardines.model.Usuario;
import co.edu.usbcali.jardines.model.control.AdjuntosxresenaLogic;
import co.edu.usbcali.jardines.model.control.AdjuntoxpreguntaLogic;
import co.edu.usbcali.jardines.model.control.AuditoriaLogic;
import co.edu.usbcali.jardines.model.control.EspecieLogic;
import co.edu.usbcali.jardines.model.control.GaleriaLogic;
import co.edu.usbcali.jardines.model.control.IAdjuntosxresenaLogic;
import co.edu.usbcali.jardines.model.control.IAdjuntoxpreguntaLogic;
import co.edu.usbcali.jardines.model.control.IAuditoriaLogic;
import co.edu.usbcali.jardines.model.control.IEspecieLogic;
import co.edu.usbcali.jardines.model.control.IGaleriaLogic;
import co.edu.usbcali.jardines.model.control.IJardinEspecieLogic;
import co.edu.usbcali.jardines.model.control.IJardinLogic;
import co.edu.usbcali.jardines.model.control.IPreguntaLogic;
import co.edu.usbcali.jardines.model.control.IResenaLogic;
import co.edu.usbcali.jardines.model.control.IRespuestaLogic;
import co.edu.usbcali.jardines.model.control.IRespuestaxusuarioLogic;
import co.edu.usbcali.jardines.model.control.ITipoUsuarioLogic;
import co.edu.usbcali.jardines.model.control.IUsuarioLogic;
import co.edu.usbcali.jardines.model.control.JardinEspecieLogic;
import co.edu.usbcali.jardines.model.control.JardinLogic;
import co.edu.usbcali.jardines.model.control.PreguntaLogic;
import co.edu.usbcali.jardines.model.control.ResenaLogic;
import co.edu.usbcali.jardines.model.control.RespuestaLogic;
import co.edu.usbcali.jardines.model.control.RespuestaxusuarioLogic;
import co.edu.usbcali.jardines.model.control.TipoUsuarioLogic;
import co.edu.usbcali.jardines.model.control.UsuarioLogic;
import co.edu.usbcali.jardines.model.dto.AdjuntosxresenaDTO;
import co.edu.usbcali.jardines.model.dto.AdjuntoxpreguntaDTO;
import co.edu.usbcali.jardines.model.dto.AuditoriaDTO;
import co.edu.usbcali.jardines.model.dto.EspecieDTO;
import co.edu.usbcali.jardines.model.dto.GaleriaDTO;
import co.edu.usbcali.jardines.model.dto.JardinDTO;
import co.edu.usbcali.jardines.model.dto.JardinEspecieDTO;
import co.edu.usbcali.jardines.model.dto.PreguntaDTO;
import co.edu.usbcali.jardines.model.dto.ResenaDTO;
import co.edu.usbcali.jardines.model.dto.RespuestaDTO;
import co.edu.usbcali.jardines.model.dto.RespuestaxusuarioDTO;
import co.edu.usbcali.jardines.model.dto.TipoUsuarioDTO;
import co.edu.usbcali.jardines.model.dto.UsuarioDTO;
import co.edu.usbcali.jardines.presentation.businessDelegate.IBusinessDelegatorView;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.Scope;

import org.springframework.stereotype.Service;

import java.math.BigDecimal;

import java.sql.*;

import java.util.Date;
import java.util.List;
import java.util.Set;


/**
* Use a Business Delegate to reduce coupling between presentation-tier clients and business services.
* The Business Delegate hides the underlying implementation details of the business service, such as lookup and access details of the EJB architecture.
*
* The Business Delegate acts as a client-side business abstraction; it provides an abstraction for, and thus hides,
* the implementation of the business services. Using a Business Delegate reduces the coupling between presentation-tier clients and
* the system's business services. Depending on the implementation strategy, the Business Delegate may shield clients from possible
* volatility in the implementation of the business service API. Potentially, this reduces the number of changes that must be made to the
* presentation-tier client code when the business service API or its underlying implementation changes.
*
* However, interface methods in the Business Delegate may still require modification if the underlying business service API changes.
* Admittedly, though, it is more likely that changes will be made to the business service rather than to the Business Delegate.
*
* Often, developers are skeptical when a design goal such as abstracting the business layer causes additional upfront work in return
* for future gains. However, using this pattern or its strategies results in only a small amount of additional upfront work and provides
* considerable benefits. The main benefit is hiding the details of the underlying service. For example, the client can become transparent
* to naming and lookup services. The Business Delegate also handles the exceptions from the business services, such as java.rmi.Remote
* exceptions, Java Messages Service (JMS) exceptions and so on. The Business Delegate may intercept such service level exceptions and
* generate application level exceptions instead. Application level exceptions are easier to handle by the clients, and may be user friendly.
* The Business Delegate may also transparently perform any retry or recovery operations necessary in the event of a service failure without
* exposing the client to the problem until it is determined that the problem is not resolvable. These gains present a compelling reason to
* use the pattern.
*
* Another benefit is that the delegate may cache results and references to remote business services. Caching can significantly improve performance,
* because it limits unnecessary and potentially costly round trips over the network.
*
* A Business Delegate uses a component called the Lookup Service. The Lookup Service is responsible for hiding the underlying implementation
* details of the business service lookup code. The Lookup Service may be written as part of the Delegate, but we recommend that it be
* implemented as a separate component, as outlined in the Service Locator pattern (See "Service Locator" on page 368.)
*
* When the Business Delegate is used with a Session Facade, typically there is a one-to-one relationship between the two.
* This one-to-one relationship exists because logic that might have been encapsulated in a Business Delegate relating to its interaction
* with multiple business services (creating a one-to-many relationship) will often be factored back into a Session Facade.
*
* Finally, it should be noted that this pattern could be used to reduce coupling between other tiers, not simply the presentation and the
* business tiers.
*
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
@Scope("singleton")
@Service("BusinessDelegatorView")
public class BusinessDelegatorView implements IBusinessDelegatorView {
    private static final Logger log = LoggerFactory.getLogger(BusinessDelegatorView.class);
    @Autowired
    private IAdjuntosxresenaLogic adjuntosxresenaLogic;
    @Autowired
    private IAdjuntoxpreguntaLogic adjuntoxpreguntaLogic;
    @Autowired
    private IAuditoriaLogic auditoriaLogic;
    @Autowired
    private IEspecieLogic especieLogic;
    @Autowired
    private IGaleriaLogic galeriaLogic;
    @Autowired
    private IJardinLogic jardinLogic;
    @Autowired
    private IJardinEspecieLogic jardinEspecieLogic;
    @Autowired
    private IPreguntaLogic preguntaLogic;
    @Autowired
    private IResenaLogic resenaLogic;
    @Autowired
    private IRespuestaLogic respuestaLogic;
    @Autowired
    private IRespuestaxusuarioLogic respuestaxusuarioLogic;
    @Autowired
    private ITipoUsuarioLogic tipoUsuarioLogic;
    @Autowired
    private IUsuarioLogic usuarioLogic;

    public List<Adjuntosxresena> getAdjuntosxresena() throws Exception {
        return adjuntosxresenaLogic.getAdjuntosxresena();
    }

    public void saveAdjuntosxresena(Adjuntosxresena entity)
        throws Exception {
        adjuntosxresenaLogic.saveAdjuntosxresena(entity);
    }

    public void deleteAdjuntosxresena(Adjuntosxresena entity)
        throws Exception {
        adjuntosxresenaLogic.deleteAdjuntosxresena(entity);
    }

    public void updateAdjuntosxresena(Adjuntosxresena entity)
        throws Exception {
        adjuntosxresenaLogic.updateAdjuntosxresena(entity);
    }

    public Adjuntosxresena getAdjuntosxresena(Long adresId)
        throws Exception {
        Adjuntosxresena adjuntosxresena = null;

        try {
            adjuntosxresena = adjuntosxresenaLogic.getAdjuntosxresena(adresId);
        } catch (Exception e) {
            throw e;
        }

        return adjuntosxresena;
    }

    public List<Adjuntosxresena> findByCriteriaInAdjuntosxresena(
        Object[] variables, Object[] variablesBetween,
        Object[] variablesBetweenDates) throws Exception {
        return adjuntosxresenaLogic.findByCriteria(variables, variablesBetween,
            variablesBetweenDates);
    }

    public List<Adjuntosxresena> findPageAdjuntosxresena(
        String sortColumnName, boolean sortAscending, int startRow,
        int maxResults) throws Exception {
        return adjuntosxresenaLogic.findPageAdjuntosxresena(sortColumnName,
            sortAscending, startRow, maxResults);
    }

    public Long findTotalNumberAdjuntosxresena() throws Exception {
        return adjuntosxresenaLogic.findTotalNumberAdjuntosxresena();
    }

    public List<AdjuntosxresenaDTO> getDataAdjuntosxresena()
        throws Exception {
        return adjuntosxresenaLogic.getDataAdjuntosxresena();
    }

    public void validateAdjuntosxresena(Adjuntosxresena adjuntosxresena)
        throws Exception {
        adjuntosxresenaLogic.validateAdjuntosxresena(adjuntosxresena);
    }

    public List<Adjuntoxpregunta> getAdjuntoxpregunta()
        throws Exception {
        return adjuntoxpreguntaLogic.getAdjuntoxpregunta();
    }

    public void saveAdjuntoxpregunta(Adjuntoxpregunta entity)
        throws Exception {
        adjuntoxpreguntaLogic.saveAdjuntoxpregunta(entity);
    }

    public void deleteAdjuntoxpregunta(Adjuntoxpregunta entity)
        throws Exception {
        adjuntoxpreguntaLogic.deleteAdjuntoxpregunta(entity);
    }

    public void updateAdjuntoxpregunta(Adjuntoxpregunta entity)
        throws Exception {
        adjuntoxpreguntaLogic.updateAdjuntoxpregunta(entity);
    }

    public Adjuntoxpregunta getAdjuntoxpregunta(Long adjpreId)
        throws Exception {
        Adjuntoxpregunta adjuntoxpregunta = null;

        try {
            adjuntoxpregunta = adjuntoxpreguntaLogic.getAdjuntoxpregunta(adjpreId);
        } catch (Exception e) {
            throw e;
        }

        return adjuntoxpregunta;
    }

    public List<Adjuntoxpregunta> findByCriteriaInAdjuntoxpregunta(
        Object[] variables, Object[] variablesBetween,
        Object[] variablesBetweenDates) throws Exception {
        return adjuntoxpreguntaLogic.findByCriteria(variables,
            variablesBetween, variablesBetweenDates);
    }

    public List<Adjuntoxpregunta> findPageAdjuntoxpregunta(
        String sortColumnName, boolean sortAscending, int startRow,
        int maxResults) throws Exception {
        return adjuntoxpreguntaLogic.findPageAdjuntoxpregunta(sortColumnName,
            sortAscending, startRow, maxResults);
    }

    public Long findTotalNumberAdjuntoxpregunta() throws Exception {
        return adjuntoxpreguntaLogic.findTotalNumberAdjuntoxpregunta();
    }

    public List<AdjuntoxpreguntaDTO> getDataAdjuntoxpregunta()
        throws Exception {
        return adjuntoxpreguntaLogic.getDataAdjuntoxpregunta();
    }

    public void validateAdjuntoxpregunta(Adjuntoxpregunta adjuntoxpregunta)
        throws Exception {
        adjuntoxpreguntaLogic.validateAdjuntoxpregunta(adjuntoxpregunta);
    }

    public List<Auditoria> getAuditoria() throws Exception {
        return auditoriaLogic.getAuditoria();
    }

    public void saveAuditoria(Auditoria entity) throws Exception {
        auditoriaLogic.saveAuditoria(entity);
    }

    public void deleteAuditoria(Auditoria entity) throws Exception {
        auditoriaLogic.deleteAuditoria(entity);
    }

    public void updateAuditoria(Auditoria entity) throws Exception {
        auditoriaLogic.updateAuditoria(entity);
    }

    public Auditoria getAuditoria(Long audId) throws Exception {
        Auditoria auditoria = null;

        try {
            auditoria = auditoriaLogic.getAuditoria(audId);
        } catch (Exception e) {
            throw e;
        }

        return auditoria;
    }

    public List<Auditoria> findByCriteriaInAuditoria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception {
        return auditoriaLogic.findByCriteria(variables, variablesBetween,
            variablesBetweenDates);
    }

    public List<Auditoria> findPageAuditoria(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception {
        return auditoriaLogic.findPageAuditoria(sortColumnName, sortAscending,
            startRow, maxResults);
    }

    public Long findTotalNumberAuditoria() throws Exception {
        return auditoriaLogic.findTotalNumberAuditoria();
    }

    public List<AuditoriaDTO> getDataAuditoria() throws Exception {
        return auditoriaLogic.getDataAuditoria();
    }

    public void validateAuditoria(Auditoria auditoria)
        throws Exception {
        auditoriaLogic.validateAuditoria(auditoria);
    }

    public List<Especie> getEspecie() throws Exception {
        return especieLogic.getEspecie();
    }

    public void saveEspecie(Especie entity) throws Exception {
        especieLogic.saveEspecie(entity);
    }

    public void deleteEspecie(Especie entity) throws Exception {
        especieLogic.deleteEspecie(entity);
    }

    public void updateEspecie(Especie entity) throws Exception {
        especieLogic.updateEspecie(entity);
    }

    public Especie getEspecie(Long espeId) throws Exception {
        Especie especie = null;

        try {
            especie = especieLogic.getEspecie(espeId);
        } catch (Exception e) {
            throw e;
        }

        return especie;
    }

    public List<Especie> findByCriteriaInEspecie(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception {
        return especieLogic.findByCriteria(variables, variablesBetween,
            variablesBetweenDates);
    }

    public List<Especie> findPageEspecie(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception {
        return especieLogic.findPageEspecie(sortColumnName, sortAscending,
            startRow, maxResults);
    }

    public Long findTotalNumberEspecie() throws Exception {
        return especieLogic.findTotalNumberEspecie();
    }

    public List<EspecieDTO> getDataEspecie() throws Exception {
        return especieLogic.getDataEspecie();
    }

    public void validateEspecie(Especie especie) throws Exception {
        especieLogic.validateEspecie(especie);
    }

    public List<Galeria> getGaleria() throws Exception {
        return galeriaLogic.getGaleria();
    }

    public void saveGaleria(Galeria entity) throws Exception {
        galeriaLogic.saveGaleria(entity);
    }

    public void deleteGaleria(Galeria entity) throws Exception {
        galeriaLogic.deleteGaleria(entity);
    }

    public void updateGaleria(Galeria entity) throws Exception {
        galeriaLogic.updateGaleria(entity);
    }

    public Galeria getGaleria(GaleriaId id) throws Exception {
        Galeria galeria = null;

        try {
            galeria = galeriaLogic.getGaleria(id);
        } catch (Exception e) {
            throw e;
        }

        return galeria;
    }

    public List<Galeria> findByCriteriaInGaleria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception {
        return galeriaLogic.findByCriteria(variables, variablesBetween,
            variablesBetweenDates);
    }

    public List<Galeria> findPageGaleria(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception {
        return galeriaLogic.findPageGaleria(sortColumnName, sortAscending,
            startRow, maxResults);
    }

    public Long findTotalNumberGaleria() throws Exception {
        return galeriaLogic.findTotalNumberGaleria();
    }

    public List<GaleriaDTO> getDataGaleria() throws Exception {
        return galeriaLogic.getDataGaleria();
    }

    public void validateGaleria(Galeria galeria) throws Exception {
        galeriaLogic.validateGaleria(galeria);
    }

    public List<Jardin> getJardin() throws Exception {
        return jardinLogic.getJardin();
    }

    public void saveJardin(Jardin entity) throws Exception {
        jardinLogic.saveJardin(entity);
    }

    public void deleteJardin(Jardin entity) throws Exception {
        jardinLogic.deleteJardin(entity);
    }

    public void updateJardin(Jardin entity) throws Exception {
        jardinLogic.updateJardin(entity);
    }

    public Jardin getJardin(Long jarId) throws Exception {
        Jardin jardin = null;

        try {
            jardin = jardinLogic.getJardin(jarId);
        } catch (Exception e) {
            throw e;
        }

        return jardin;
    }

    public List<Jardin> findByCriteriaInJardin(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception {
        return jardinLogic.findByCriteria(variables, variablesBetween,
            variablesBetweenDates);
    }

    public List<Jardin> findPageJardin(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception {
        return jardinLogic.findPageJardin(sortColumnName, sortAscending,
            startRow, maxResults);
    }

    public Long findTotalNumberJardin() throws Exception {
        return jardinLogic.findTotalNumberJardin();
    }

    public List<JardinDTO> getDataJardin() throws Exception {
        return jardinLogic.getDataJardin();
    }

    public void validateJardin(Jardin jardin) throws Exception {
        jardinLogic.validateJardin(jardin);
    }

    public List<JardinEspecie> getJardinEspecie() throws Exception {
        return jardinEspecieLogic.getJardinEspecie();
    }

    public void saveJardinEspecie(JardinEspecie entity)
        throws Exception {
        jardinEspecieLogic.saveJardinEspecie(entity);
    }

    public void deleteJardinEspecie(JardinEspecie entity)
        throws Exception {
        jardinEspecieLogic.deleteJardinEspecie(entity);
    }

    public void updateJardinEspecie(JardinEspecie entity)
        throws Exception {
        jardinEspecieLogic.updateJardinEspecie(entity);
    }

    public JardinEspecie getJardinEspecie(Long jarespId)
        throws Exception {
        JardinEspecie jardinEspecie = null;

        try {
            jardinEspecie = jardinEspecieLogic.getJardinEspecie(jarespId);
        } catch (Exception e) {
            throw e;
        }

        return jardinEspecie;
    }

    public List<JardinEspecie> findByCriteriaInJardinEspecie(
        Object[] variables, Object[] variablesBetween,
        Object[] variablesBetweenDates) throws Exception {
        return jardinEspecieLogic.findByCriteria(variables, variablesBetween,
            variablesBetweenDates);
    }

    public List<JardinEspecie> findPageJardinEspecie(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception {
        return jardinEspecieLogic.findPageJardinEspecie(sortColumnName,
            sortAscending, startRow, maxResults);
    }

    public Long findTotalNumberJardinEspecie() throws Exception {
        return jardinEspecieLogic.findTotalNumberJardinEspecie();
    }

    public List<JardinEspecieDTO> getDataJardinEspecie()
        throws Exception {
        return jardinEspecieLogic.getDataJardinEspecie();
    }

    public void validateJardinEspecie(JardinEspecie jardinEspecie)
        throws Exception {
        jardinEspecieLogic.validateJardinEspecie(jardinEspecie);
    }

    public List<Pregunta> getPregunta() throws Exception {
        return preguntaLogic.getPregunta();
    }

    public void savePregunta(Pregunta entity) throws Exception {
        preguntaLogic.savePregunta(entity);
    }

    public void deletePregunta(Pregunta entity) throws Exception {
        preguntaLogic.deletePregunta(entity);
    }

    public void updatePregunta(Pregunta entity) throws Exception {
        preguntaLogic.updatePregunta(entity);
    }

    public Pregunta getPregunta(Long preId) throws Exception {
        Pregunta pregunta = null;

        try {
            pregunta = preguntaLogic.getPregunta(preId);
        } catch (Exception e) {
            throw e;
        }

        return pregunta;
    }

    public List<Pregunta> findByCriteriaInPregunta(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception {
        return preguntaLogic.findByCriteria(variables, variablesBetween,
            variablesBetweenDates);
    }

    public List<Pregunta> findPagePregunta(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception {
        return preguntaLogic.findPagePregunta(sortColumnName, sortAscending,
            startRow, maxResults);
    }

    public Long findTotalNumberPregunta() throws Exception {
        return preguntaLogic.findTotalNumberPregunta();
    }

    public List<PreguntaDTO> getDataPregunta() throws Exception {
        return preguntaLogic.getDataPregunta();
    }

    public void validatePregunta(Pregunta pregunta) throws Exception {
        preguntaLogic.validatePregunta(pregunta);
    }

    public List<Resena> getResena() throws Exception {
        return resenaLogic.getResena();
    }

    public void saveResena(Resena entity) throws Exception {
        resenaLogic.saveResena(entity);
    }

    public void deleteResena(Resena entity) throws Exception {
        resenaLogic.deleteResena(entity);
    }

    public void updateResena(Resena entity) throws Exception {
        resenaLogic.updateResena(entity);
    }

    public Resena getResena(Long resId) throws Exception {
        Resena resena = null;

        try {
            resena = resenaLogic.getResena(resId);
        } catch (Exception e) {
            throw e;
        }

        return resena;
    }

    public List<Resena> findByCriteriaInResena(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception {
        return resenaLogic.findByCriteria(variables, variablesBetween,
            variablesBetweenDates);
    }

    public List<Resena> findPageResena(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception {
        return resenaLogic.findPageResena(sortColumnName, sortAscending,
            startRow, maxResults);
    }

    public Long findTotalNumberResena() throws Exception {
        return resenaLogic.findTotalNumberResena();
    }

    public List<ResenaDTO> getDataResena() throws Exception {
        return resenaLogic.getDataResena();
    }

    public void validateResena(Resena resena) throws Exception {
        resenaLogic.validateResena(resena);
    }

    public List<Respuesta> getRespuesta() throws Exception {
        return respuestaLogic.getRespuesta();
    }

    public void saveRespuesta(Respuesta entity) throws Exception {
        respuestaLogic.saveRespuesta(entity);
    }

    public void deleteRespuesta(Respuesta entity) throws Exception {
        respuestaLogic.deleteRespuesta(entity);
    }

    public void updateRespuesta(Respuesta entity) throws Exception {
        respuestaLogic.updateRespuesta(entity);
    }

    public Respuesta getRespuesta(Long respId) throws Exception {
        Respuesta respuesta = null;

        try {
            respuesta = respuestaLogic.getRespuesta(respId);
        } catch (Exception e) {
            throw e;
        }

        return respuesta;
    }

    public List<Respuesta> findByCriteriaInRespuesta(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception {
        return respuestaLogic.findByCriteria(variables, variablesBetween,
            variablesBetweenDates);
    }

    public List<Respuesta> findPageRespuesta(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception {
        return respuestaLogic.findPageRespuesta(sortColumnName, sortAscending,
            startRow, maxResults);
    }

    public Long findTotalNumberRespuesta() throws Exception {
        return respuestaLogic.findTotalNumberRespuesta();
    }

    public List<RespuestaDTO> getDataRespuesta() throws Exception {
        return respuestaLogic.getDataRespuesta();
    }

    public void validateRespuesta(Respuesta respuesta)
        throws Exception {
        respuestaLogic.validateRespuesta(respuesta);
    }

    public List<Respuestaxusuario> getRespuestaxusuario()
        throws Exception {
        return respuestaxusuarioLogic.getRespuestaxusuario();
    }

    public void saveRespuestaxusuario(Respuestaxusuario entity)
        throws Exception {
        respuestaxusuarioLogic.saveRespuestaxusuario(entity);
    }

    public void deleteRespuestaxusuario(Respuestaxusuario entity)
        throws Exception {
        respuestaxusuarioLogic.deleteRespuestaxusuario(entity);
    }

    public void updateRespuestaxusuario(Respuestaxusuario entity)
        throws Exception {
        respuestaxusuarioLogic.updateRespuestaxusuario(entity);
    }

    public Respuestaxusuario getRespuestaxusuario(Long respusuId)
        throws Exception {
        Respuestaxusuario respuestaxusuario = null;

        try {
            respuestaxusuario = respuestaxusuarioLogic.getRespuestaxusuario(respusuId);
        } catch (Exception e) {
            throw e;
        }

        return respuestaxusuario;
    }

    public List<Respuestaxusuario> findByCriteriaInRespuestaxusuario(
        Object[] variables, Object[] variablesBetween,
        Object[] variablesBetweenDates) throws Exception {
        return respuestaxusuarioLogic.findByCriteria(variables,
            variablesBetween, variablesBetweenDates);
    }

    public List<Respuestaxusuario> findPageRespuestaxusuario(
        String sortColumnName, boolean sortAscending, int startRow,
        int maxResults) throws Exception {
        return respuestaxusuarioLogic.findPageRespuestaxusuario(sortColumnName,
            sortAscending, startRow, maxResults);
    }

    public Long findTotalNumberRespuestaxusuario() throws Exception {
        return respuestaxusuarioLogic.findTotalNumberRespuestaxusuario();
    }

    public List<RespuestaxusuarioDTO> getDataRespuestaxusuario()
        throws Exception {
        return respuestaxusuarioLogic.getDataRespuestaxusuario();
    }

    public void validateRespuestaxusuario(Respuestaxusuario respuestaxusuario)
        throws Exception {
        respuestaxusuarioLogic.validateRespuestaxusuario(respuestaxusuario);
    }

    public List<TipoUsuario> getTipoUsuario() throws Exception {
        return tipoUsuarioLogic.getTipoUsuario();
    }

    public void saveTipoUsuario(TipoUsuario entity) throws Exception {
        tipoUsuarioLogic.saveTipoUsuario(entity);
    }

    public void deleteTipoUsuario(TipoUsuario entity) throws Exception {
        tipoUsuarioLogic.deleteTipoUsuario(entity);
    }

    public void updateTipoUsuario(TipoUsuario entity) throws Exception {
        tipoUsuarioLogic.updateTipoUsuario(entity);
    }

    public TipoUsuario getTipoUsuario(Long tiusId) throws Exception {
        TipoUsuario tipoUsuario = null;

        try {
            tipoUsuario = tipoUsuarioLogic.getTipoUsuario(tiusId);
        } catch (Exception e) {
            throw e;
        }

        return tipoUsuario;
    }

    public List<TipoUsuario> findByCriteriaInTipoUsuario(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception {
        return tipoUsuarioLogic.findByCriteria(variables, variablesBetween,
            variablesBetweenDates);
    }

    public List<TipoUsuario> findPageTipoUsuario(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception {
        return tipoUsuarioLogic.findPageTipoUsuario(sortColumnName,
            sortAscending, startRow, maxResults);
    }

    public Long findTotalNumberTipoUsuario() throws Exception {
        return tipoUsuarioLogic.findTotalNumberTipoUsuario();
    }

    public List<TipoUsuarioDTO> getDataTipoUsuario() throws Exception {
        return tipoUsuarioLogic.getDataTipoUsuario();
    }

    public void validateTipoUsuario(TipoUsuario tipoUsuario)
        throws Exception {
        tipoUsuarioLogic.validateTipoUsuario(tipoUsuario);
    }

    public List<Usuario> getUsuario() throws Exception {
        return usuarioLogic.getUsuario();
    }

    public void saveUsuario(Usuario entity) throws Exception {
        usuarioLogic.saveUsuario(entity);
    }

    public void deleteUsuario(Usuario entity) throws Exception {
        usuarioLogic.deleteUsuario(entity);
    }

    public void updateUsuario(Usuario entity) throws Exception {
        usuarioLogic.updateUsuario(entity);
    }

    public Usuario getUsuario(Long usuId) throws Exception {
        Usuario usuario = null;

        try {
            usuario = usuarioLogic.getUsuario(usuId);
        } catch (Exception e) {
            throw e;
        }

        return usuario;
    }

    public List<Usuario> findByCriteriaInUsuario(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception {
        return usuarioLogic.findByCriteria(variables, variablesBetween,
            variablesBetweenDates);
    }

    public List<Usuario> findPageUsuario(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception {
        return usuarioLogic.findPageUsuario(sortColumnName, sortAscending,
            startRow, maxResults);
    }

    public Long findTotalNumberUsuario() throws Exception {
        return usuarioLogic.findTotalNumberUsuario();
    }

    public List<UsuarioDTO> getDataUsuario() throws Exception {
        return usuarioLogic.getDataUsuario();
    }

    public void validateUsuario(Usuario usuario) throws Exception {
        usuarioLogic.validateUsuario(usuario);
    }
}

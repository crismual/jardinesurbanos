package co.edu.usbcali.jardines.presentation.backingBeans;

import co.edu.usbcali.jardines.exceptions.*;
import co.edu.usbcali.jardines.model.*;
import co.edu.usbcali.jardines.model.dto.RespuestaxusuarioDTO;
import co.edu.usbcali.jardines.presentation.businessDelegate.*;
import co.edu.usbcali.jardines.utilities.*;

import org.primefaces.component.calendar.*;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.inputtext.InputText;

import org.primefaces.event.RowEditEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

import java.sql.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;


/**
 * @author Zathura Code Generator http://zathuracode.org
 * www.zathuracode.org
 *
 */
@ManagedBean
@ViewScoped
public class RespuestaxusuarioView implements Serializable {
    private static final long serialVersionUID = 1L;
    private static final Logger log = LoggerFactory.getLogger(RespuestaxusuarioView.class);
    private InputText txtDescripcion;
    private InputText txtDescripcionPregunta;
    private InputText txtEstado;
    private InputText txtUsuarioCreador;
    private InputText txtUsuarioModificador;
    private InputText txtJarId_Jardin;
    private InputText txtRespId_Respuesta;
    private InputText txtUsuId_Usuario;
    private InputText txtRespusuId;
    private Calendar txtFechaCreacion;
    private Calendar txtFechaModificacion;
    private CommandButton btnSave;
    private CommandButton btnModify;
    private CommandButton btnDelete;
    private CommandButton btnClear;
    private List<RespuestaxusuarioDTO> data;
    private RespuestaxusuarioDTO selectedRespuestaxusuario;
    private Respuestaxusuario entity;
    private boolean showDialog;
    @ManagedProperty(value = "#{BusinessDelegatorView}")
    private IBusinessDelegatorView businessDelegatorView;

    public RespuestaxusuarioView() {
        super();
    }

    public String action_new() {
        action_clear();
        selectedRespuestaxusuario = null;
        setShowDialog(true);

        return "";
    }

    public String action_clear() {
        entity = null;
        selectedRespuestaxusuario = null;

        if (txtDescripcion != null) {
            txtDescripcion.setValue(null);
            txtDescripcion.setDisabled(true);
        }

        if (txtDescripcionPregunta != null) {
            txtDescripcionPregunta.setValue(null);
            txtDescripcionPregunta.setDisabled(true);
        }

        if (txtEstado != null) {
            txtEstado.setValue(null);
            txtEstado.setDisabled(true);
        }

        if (txtUsuarioCreador != null) {
            txtUsuarioCreador.setValue(null);
            txtUsuarioCreador.setDisabled(true);
        }

        if (txtUsuarioModificador != null) {
            txtUsuarioModificador.setValue(null);
            txtUsuarioModificador.setDisabled(true);
        }

        if (txtJarId_Jardin != null) {
            txtJarId_Jardin.setValue(null);
            txtJarId_Jardin.setDisabled(true);
        }

        if (txtRespId_Respuesta != null) {
            txtRespId_Respuesta.setValue(null);
            txtRespId_Respuesta.setDisabled(true);
        }

        if (txtUsuId_Usuario != null) {
            txtUsuId_Usuario.setValue(null);
            txtUsuId_Usuario.setDisabled(true);
        }

        if (txtFechaCreacion != null) {
            txtFechaCreacion.setValue(null);
            txtFechaCreacion.setDisabled(true);
        }

        if (txtFechaModificacion != null) {
            txtFechaModificacion.setValue(null);
            txtFechaModificacion.setDisabled(true);
        }

        if (txtRespusuId != null) {
            txtRespusuId.setValue(null);
            txtRespusuId.setDisabled(false);
        }

        if (btnSave != null) {
            btnSave.setDisabled(true);
        }

        if (btnDelete != null) {
            btnDelete.setDisabled(true);
        }

        return "";
    }

    public void listener_txtFechaCreacion() {
        Date inputDate = (Date) txtFechaCreacion.getValue();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        FacesContext.getCurrentInstance()
                    .addMessage("",
            new FacesMessage("Selected Date " + dateFormat.format(inputDate)));
    }

    public void listener_txtFechaModificacion() {
        Date inputDate = (Date) txtFechaModificacion.getValue();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        FacesContext.getCurrentInstance()
                    .addMessage("",
            new FacesMessage("Selected Date " + dateFormat.format(inputDate)));
    }

    public void listener_txtId() {
        try {
            Long respusuId = FacesUtils.checkLong(txtRespusuId);
            entity = (respusuId != null)
                ? businessDelegatorView.getRespuestaxusuario(respusuId) : null;
        } catch (Exception e) {
            entity = null;
        }

        if (entity == null) {
            txtDescripcion.setDisabled(false);
            txtDescripcionPregunta.setDisabled(false);
            txtEstado.setDisabled(false);
            txtUsuarioCreador.setDisabled(false);
            txtUsuarioModificador.setDisabled(false);
            txtJarId_Jardin.setDisabled(false);
            txtRespId_Respuesta.setDisabled(false);
            txtUsuId_Usuario.setDisabled(false);
            txtFechaCreacion.setDisabled(false);
            txtFechaModificacion.setDisabled(false);
            txtRespusuId.setDisabled(false);
            btnSave.setDisabled(false);
        } else {
            txtDescripcion.setValue(entity.getDescripcion());
            txtDescripcion.setDisabled(false);
            txtDescripcionPregunta.setValue(entity.getDescripcionPregunta());
            txtDescripcionPregunta.setDisabled(false);
            txtEstado.setValue(entity.getEstado());
            txtEstado.setDisabled(false);
            txtFechaCreacion.setValue(entity.getFechaCreacion());
            txtFechaCreacion.setDisabled(false);
            txtFechaModificacion.setValue(entity.getFechaModificacion());
            txtFechaModificacion.setDisabled(false);
            txtUsuarioCreador.setValue(entity.getUsuarioCreador());
            txtUsuarioCreador.setDisabled(false);
            txtUsuarioModificador.setValue(entity.getUsuarioModificador());
            txtUsuarioModificador.setDisabled(false);
            txtJarId_Jardin.setValue(entity.getJardin().getJarId());
            txtJarId_Jardin.setDisabled(false);
            txtRespId_Respuesta.setValue(entity.getRespuesta().getRespId());
            txtRespId_Respuesta.setDisabled(false);
            txtUsuId_Usuario.setValue(entity.getUsuario().getUsuId());
            txtUsuId_Usuario.setDisabled(false);
            txtRespusuId.setValue(entity.getRespusuId());
            txtRespusuId.setDisabled(true);
            btnSave.setDisabled(false);

            if (btnDelete != null) {
                btnDelete.setDisabled(false);
            }
        }
    }

    public String action_edit(ActionEvent evt) {
        selectedRespuestaxusuario = (RespuestaxusuarioDTO) (evt.getComponent()
                                                               .getAttributes()
                                                               .get("selectedRespuestaxusuario"));
        txtDescripcion.setValue(selectedRespuestaxusuario.getDescripcion());
        txtDescripcion.setDisabled(false);
        txtDescripcionPregunta.setValue(selectedRespuestaxusuario.getDescripcionPregunta());
        txtDescripcionPregunta.setDisabled(false);
        txtEstado.setValue(selectedRespuestaxusuario.getEstado());
        txtEstado.setDisabled(false);
        txtFechaCreacion.setValue(selectedRespuestaxusuario.getFechaCreacion());
        txtFechaCreacion.setDisabled(false);
        txtFechaModificacion.setValue(selectedRespuestaxusuario.getFechaModificacion());
        txtFechaModificacion.setDisabled(false);
        txtUsuarioCreador.setValue(selectedRespuestaxusuario.getUsuarioCreador());
        txtUsuarioCreador.setDisabled(false);
        txtUsuarioModificador.setValue(selectedRespuestaxusuario.getUsuarioModificador());
        txtUsuarioModificador.setDisabled(false);
        txtJarId_Jardin.setValue(selectedRespuestaxusuario.getJarId_Jardin());
        txtJarId_Jardin.setDisabled(false);
        txtRespId_Respuesta.setValue(selectedRespuestaxusuario.getRespId_Respuesta());
        txtRespId_Respuesta.setDisabled(false);
        txtUsuId_Usuario.setValue(selectedRespuestaxusuario.getUsuId_Usuario());
        txtUsuId_Usuario.setDisabled(false);
        txtRespusuId.setValue(selectedRespuestaxusuario.getRespusuId());
        txtRespusuId.setDisabled(true);
        btnSave.setDisabled(false);
        setShowDialog(true);

        return "";
    }

    public String action_save() {
        try {
            if ((selectedRespuestaxusuario == null) && (entity == null)) {
                action_create();
            } else {
                action_modify();
            }

            data = null;
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_create() {
        try {
            entity = new Respuestaxusuario();

            Long respusuId = FacesUtils.checkLong(txtRespusuId);

            entity.setDescripcion(FacesUtils.checkString(txtDescripcion));
            entity.setDescripcionPregunta(FacesUtils.checkString(
                    txtDescripcionPregunta));
            entity.setEstado(FacesUtils.checkString(txtEstado));
            entity.setFechaCreacion(FacesUtils.checkDate(txtFechaCreacion));
            entity.setFechaModificacion(FacesUtils.checkDate(
                    txtFechaModificacion));
            entity.setRespusuId(respusuId);
            entity.setUsuarioCreador(FacesUtils.checkString(txtUsuarioCreador));
            entity.setUsuarioModificador(FacesUtils.checkString(
                    txtUsuarioModificador));
            entity.setJardin((FacesUtils.checkLong(txtJarId_Jardin) != null)
                ? businessDelegatorView.getJardin(FacesUtils.checkLong(
                        txtJarId_Jardin)) : null);
            entity.setRespuesta((FacesUtils.checkLong(txtRespId_Respuesta) != null)
                ? businessDelegatorView.getRespuesta(FacesUtils.checkLong(
                        txtRespId_Respuesta)) : null);
            entity.setUsuario((FacesUtils.checkLong(txtUsuId_Usuario) != null)
                ? businessDelegatorView.getUsuario(FacesUtils.checkLong(
                        txtUsuId_Usuario)) : null);
            businessDelegatorView.saveRespuestaxusuario(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYSAVED);
            action_clear();
        } catch (Exception e) {
            entity = null;
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_modify() {
        try {
            if (entity == null) {
                Long respusuId = new Long(selectedRespuestaxusuario.getRespusuId());
                entity = businessDelegatorView.getRespuestaxusuario(respusuId);
            }

            entity.setDescripcion(FacesUtils.checkString(txtDescripcion));
            entity.setDescripcionPregunta(FacesUtils.checkString(
                    txtDescripcionPregunta));
            entity.setEstado(FacesUtils.checkString(txtEstado));
            entity.setFechaCreacion(FacesUtils.checkDate(txtFechaCreacion));
            entity.setFechaModificacion(FacesUtils.checkDate(
                    txtFechaModificacion));
            entity.setUsuarioCreador(FacesUtils.checkString(txtUsuarioCreador));
            entity.setUsuarioModificador(FacesUtils.checkString(
                    txtUsuarioModificador));
            entity.setJardin((FacesUtils.checkLong(txtJarId_Jardin) != null)
                ? businessDelegatorView.getJardin(FacesUtils.checkLong(
                        txtJarId_Jardin)) : null);
            entity.setRespuesta((FacesUtils.checkLong(txtRespId_Respuesta) != null)
                ? businessDelegatorView.getRespuesta(FacesUtils.checkLong(
                        txtRespId_Respuesta)) : null);
            entity.setUsuario((FacesUtils.checkLong(txtUsuId_Usuario) != null)
                ? businessDelegatorView.getUsuario(FacesUtils.checkLong(
                        txtUsuId_Usuario)) : null);
            businessDelegatorView.updateRespuestaxusuario(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYMODIFIED);
        } catch (Exception e) {
            data = null;
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_delete_datatable(ActionEvent evt) {
        try {
            selectedRespuestaxusuario = (RespuestaxusuarioDTO) (evt.getComponent()
                                                                   .getAttributes()
                                                                   .get("selectedRespuestaxusuario"));

            Long respusuId = new Long(selectedRespuestaxusuario.getRespusuId());
            entity = businessDelegatorView.getRespuestaxusuario(respusuId);
            action_delete();
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_delete_master() {
        try {
            Long respusuId = FacesUtils.checkLong(txtRespusuId);
            entity = businessDelegatorView.getRespuestaxusuario(respusuId);
            action_delete();
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public void action_delete() throws Exception {
        try {
            businessDelegatorView.deleteRespuestaxusuario(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYDELETED);
            action_clear();
            data = null;
        } catch (Exception e) {
            throw e;
        }
    }

    public String action_closeDialog() {
        setShowDialog(false);
        action_clear();

        return "";
    }

    public String action_modifyWitDTO(String descripcion,
        String descripcionPregunta, String estado, Date fechaCreacion,
        Date fechaModificacion, Long respusuId, String usuarioCreador,
        String usuarioModificador, Long jarId_Jardin, Long respId_Respuesta,
        Long usuId_Usuario) throws Exception {
        try {
            entity.setDescripcion(FacesUtils.checkString(descripcion));
            entity.setDescripcionPregunta(FacesUtils.checkString(
                    descripcionPregunta));
            entity.setEstado(FacesUtils.checkString(estado));
            entity.setFechaCreacion(FacesUtils.checkDate(fechaCreacion));
            entity.setFechaModificacion(FacesUtils.checkDate(fechaModificacion));
            entity.setUsuarioCreador(FacesUtils.checkString(usuarioCreador));
            entity.setUsuarioModificador(FacesUtils.checkString(
                    usuarioModificador));
            businessDelegatorView.updateRespuestaxusuario(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYMODIFIED);
        } catch (Exception e) {
            //renderManager.getOnDemandRenderer("RespuestaxusuarioView").requestRender();
            FacesUtils.addErrorMessage(e.getMessage());
            throw e;
        }

        return "";
    }

    public InputText getTxtDescripcion() {
        return txtDescripcion;
    }

    public void setTxtDescripcion(InputText txtDescripcion) {
        this.txtDescripcion = txtDescripcion;
    }

    public InputText getTxtDescripcionPregunta() {
        return txtDescripcionPregunta;
    }

    public void setTxtDescripcionPregunta(InputText txtDescripcionPregunta) {
        this.txtDescripcionPregunta = txtDescripcionPregunta;
    }

    public InputText getTxtEstado() {
        return txtEstado;
    }

    public void setTxtEstado(InputText txtEstado) {
        this.txtEstado = txtEstado;
    }

    public InputText getTxtUsuarioCreador() {
        return txtUsuarioCreador;
    }

    public void setTxtUsuarioCreador(InputText txtUsuarioCreador) {
        this.txtUsuarioCreador = txtUsuarioCreador;
    }

    public InputText getTxtUsuarioModificador() {
        return txtUsuarioModificador;
    }

    public void setTxtUsuarioModificador(InputText txtUsuarioModificador) {
        this.txtUsuarioModificador = txtUsuarioModificador;
    }

    public InputText getTxtJarId_Jardin() {
        return txtJarId_Jardin;
    }

    public void setTxtJarId_Jardin(InputText txtJarId_Jardin) {
        this.txtJarId_Jardin = txtJarId_Jardin;
    }

    public InputText getTxtRespId_Respuesta() {
        return txtRespId_Respuesta;
    }

    public void setTxtRespId_Respuesta(InputText txtRespId_Respuesta) {
        this.txtRespId_Respuesta = txtRespId_Respuesta;
    }

    public InputText getTxtUsuId_Usuario() {
        return txtUsuId_Usuario;
    }

    public void setTxtUsuId_Usuario(InputText txtUsuId_Usuario) {
        this.txtUsuId_Usuario = txtUsuId_Usuario;
    }

    public Calendar getTxtFechaCreacion() {
        return txtFechaCreacion;
    }

    public void setTxtFechaCreacion(Calendar txtFechaCreacion) {
        this.txtFechaCreacion = txtFechaCreacion;
    }

    public Calendar getTxtFechaModificacion() {
        return txtFechaModificacion;
    }

    public void setTxtFechaModificacion(Calendar txtFechaModificacion) {
        this.txtFechaModificacion = txtFechaModificacion;
    }

    public InputText getTxtRespusuId() {
        return txtRespusuId;
    }

    public void setTxtRespusuId(InputText txtRespusuId) {
        this.txtRespusuId = txtRespusuId;
    }

    public List<RespuestaxusuarioDTO> getData() {
        try {
            if (data == null) {
                data = businessDelegatorView.getDataRespuestaxusuario();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public void setData(List<RespuestaxusuarioDTO> respuestaxusuarioDTO) {
        this.data = respuestaxusuarioDTO;
    }

    public RespuestaxusuarioDTO getSelectedRespuestaxusuario() {
        return selectedRespuestaxusuario;
    }

    public void setSelectedRespuestaxusuario(
        RespuestaxusuarioDTO respuestaxusuario) {
        this.selectedRespuestaxusuario = respuestaxusuario;
    }

    public CommandButton getBtnSave() {
        return btnSave;
    }

    public void setBtnSave(CommandButton btnSave) {
        this.btnSave = btnSave;
    }

    public CommandButton getBtnModify() {
        return btnModify;
    }

    public void setBtnModify(CommandButton btnModify) {
        this.btnModify = btnModify;
    }

    public CommandButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(CommandButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public CommandButton getBtnClear() {
        return btnClear;
    }

    public void setBtnClear(CommandButton btnClear) {
        this.btnClear = btnClear;
    }

    public TimeZone getTimeZone() {
        return java.util.TimeZone.getDefault();
    }

    public IBusinessDelegatorView getBusinessDelegatorView() {
        return businessDelegatorView;
    }

    public void setBusinessDelegatorView(
        IBusinessDelegatorView businessDelegatorView) {
        this.businessDelegatorView = businessDelegatorView;
    }

    public boolean isShowDialog() {
        return showDialog;
    }

    public void setShowDialog(boolean showDialog) {
        this.showDialog = showDialog;
    }
}

package co.edu.usbcali.jardines.presentation.backingBeans;

import co.edu.usbcali.jardines.exceptions.*;
import co.edu.usbcali.jardines.model.*;
import co.edu.usbcali.jardines.model.dto.JardinDTO;
import co.edu.usbcali.jardines.presentation.businessDelegate.*;
import co.edu.usbcali.jardines.utilities.*;

import org.primefaces.component.calendar.*;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.inputtext.InputText;

import org.primefaces.event.RowEditEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

import java.sql.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;


/**
 * @author Zathura Code Generator http://zathuracode.org
 * www.zathuracode.org
 *
 */
@ManagedBean
@ViewScoped
public class JardinView implements Serializable {
    private static final long serialVersionUID = 1L;
    private static final Logger log = LoggerFactory.getLogger(JardinView.class);
    private InputText txtAltitud;
    private InputText txtBarrio;
    private InputText txtDescripcion;
    private InputText txtDireccion;
    private InputText txtEstado;
    private InputText txtLatitud;
    private InputText txtNombreJardin;
    private InputText txtUsuarioCreador;
    private InputText txtUsuarioModificador;
    private InputText txtZona;
    private InputText txtUsuId_Usuario;
    private InputText txtJarId;
    private Calendar txtFechaCreacion;
    private Calendar txtFechaModificacion;
    private CommandButton btnSave;
    private CommandButton btnModify;
    private CommandButton btnDelete;
    private CommandButton btnClear;
    private List<JardinDTO> data;
    private JardinDTO selectedJardin;
    private Jardin entity;
    private boolean showDialog;
    @ManagedProperty(value = "#{BusinessDelegatorView}")
    private IBusinessDelegatorView businessDelegatorView;

    public JardinView() {
        super();
    }

    public String action_new() {
        action_clear();
        selectedJardin = null;
        setShowDialog(true);

        return "";
    }

    public String action_clear() {
        entity = null;
        selectedJardin = null;

        if (txtAltitud != null) {
            txtAltitud.setValue(null);
            txtAltitud.setDisabled(true);
        }

        if (txtBarrio != null) {
            txtBarrio.setValue(null);
            txtBarrio.setDisabled(true);
        }

        if (txtDescripcion != null) {
            txtDescripcion.setValue(null);
            txtDescripcion.setDisabled(true);
        }

        if (txtDireccion != null) {
            txtDireccion.setValue(null);
            txtDireccion.setDisabled(true);
        }

        if (txtEstado != null) {
            txtEstado.setValue(null);
            txtEstado.setDisabled(true);
        }

        if (txtLatitud != null) {
            txtLatitud.setValue(null);
            txtLatitud.setDisabled(true);
        }

        if (txtNombreJardin != null) {
            txtNombreJardin.setValue(null);
            txtNombreJardin.setDisabled(true);
        }

        if (txtUsuarioCreador != null) {
            txtUsuarioCreador.setValue(null);
            txtUsuarioCreador.setDisabled(true);
        }

        if (txtUsuarioModificador != null) {
            txtUsuarioModificador.setValue(null);
            txtUsuarioModificador.setDisabled(true);
        }

        if (txtZona != null) {
            txtZona.setValue(null);
            txtZona.setDisabled(true);
        }

        if (txtUsuId_Usuario != null) {
            txtUsuId_Usuario.setValue(null);
            txtUsuId_Usuario.setDisabled(true);
        }

        if (txtFechaCreacion != null) {
            txtFechaCreacion.setValue(null);
            txtFechaCreacion.setDisabled(true);
        }

        if (txtFechaModificacion != null) {
            txtFechaModificacion.setValue(null);
            txtFechaModificacion.setDisabled(true);
        }

        if (txtJarId != null) {
            txtJarId.setValue(null);
            txtJarId.setDisabled(false);
        }

        if (btnSave != null) {
            btnSave.setDisabled(true);
        }

        if (btnDelete != null) {
            btnDelete.setDisabled(true);
        }

        return "";
    }

    public void listener_txtFechaCreacion() {
        Date inputDate = (Date) txtFechaCreacion.getValue();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        FacesContext.getCurrentInstance()
                    .addMessage("",
            new FacesMessage("Selected Date " + dateFormat.format(inputDate)));
    }

    public void listener_txtFechaModificacion() {
        Date inputDate = (Date) txtFechaModificacion.getValue();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        FacesContext.getCurrentInstance()
                    .addMessage("",
            new FacesMessage("Selected Date " + dateFormat.format(inputDate)));
    }

    public void listener_txtId() {
        try {
            Long jarId = FacesUtils.checkLong(txtJarId);
            entity = (jarId != null) ? businessDelegatorView.getJardin(jarId)
                                     : null;
        } catch (Exception e) {
            entity = null;
        }

        if (entity == null) {
            txtAltitud.setDisabled(false);
            txtBarrio.setDisabled(false);
            txtDescripcion.setDisabled(false);
            txtDireccion.setDisabled(false);
            txtEstado.setDisabled(false);
            txtLatitud.setDisabled(false);
            txtNombreJardin.setDisabled(false);
            txtUsuarioCreador.setDisabled(false);
            txtUsuarioModificador.setDisabled(false);
            txtZona.setDisabled(false);
            txtUsuId_Usuario.setDisabled(false);
            txtFechaCreacion.setDisabled(false);
            txtFechaModificacion.setDisabled(false);
            txtJarId.setDisabled(false);
            btnSave.setDisabled(false);
        } else {
            txtAltitud.setValue(entity.getAltitud());
            txtAltitud.setDisabled(false);
            txtBarrio.setValue(entity.getBarrio());
            txtBarrio.setDisabled(false);
            txtDescripcion.setValue(entity.getDescripcion());
            txtDescripcion.setDisabled(false);
            txtDireccion.setValue(entity.getDireccion());
            txtDireccion.setDisabled(false);
            txtEstado.setValue(entity.getEstado());
            txtEstado.setDisabled(false);
            txtFechaCreacion.setValue(entity.getFechaCreacion());
            txtFechaCreacion.setDisabled(false);
            txtFechaModificacion.setValue(entity.getFechaModificacion());
            txtFechaModificacion.setDisabled(false);
            txtLatitud.setValue(entity.getLatitud());
            txtLatitud.setDisabled(false);
            txtNombreJardin.setValue(entity.getNombreJardin());
            txtNombreJardin.setDisabled(false);
            txtUsuarioCreador.setValue(entity.getUsuarioCreador());
            txtUsuarioCreador.setDisabled(false);
            txtUsuarioModificador.setValue(entity.getUsuarioModificador());
            txtUsuarioModificador.setDisabled(false);
            txtZona.setValue(entity.getZona());
            txtZona.setDisabled(false);
            txtUsuId_Usuario.setValue(entity.getUsuario().getUsuId());
            txtUsuId_Usuario.setDisabled(false);
            txtJarId.setValue(entity.getJarId());
            txtJarId.setDisabled(true);
            btnSave.setDisabled(false);

            if (btnDelete != null) {
                btnDelete.setDisabled(false);
            }
        }
    }

    public String action_edit(ActionEvent evt) {
        selectedJardin = (JardinDTO) (evt.getComponent().getAttributes()
                                         .get("selectedJardin"));
        txtAltitud.setValue(selectedJardin.getAltitud());
        txtAltitud.setDisabled(false);
        txtBarrio.setValue(selectedJardin.getBarrio());
        txtBarrio.setDisabled(false);
        txtDescripcion.setValue(selectedJardin.getDescripcion());
        txtDescripcion.setDisabled(false);
        txtDireccion.setValue(selectedJardin.getDireccion());
        txtDireccion.setDisabled(false);
        txtEstado.setValue(selectedJardin.getEstado());
        txtEstado.setDisabled(false);
        txtFechaCreacion.setValue(selectedJardin.getFechaCreacion());
        txtFechaCreacion.setDisabled(false);
        txtFechaModificacion.setValue(selectedJardin.getFechaModificacion());
        txtFechaModificacion.setDisabled(false);
        txtLatitud.setValue(selectedJardin.getLatitud());
        txtLatitud.setDisabled(false);
        txtNombreJardin.setValue(selectedJardin.getNombreJardin());
        txtNombreJardin.setDisabled(false);
        txtUsuarioCreador.setValue(selectedJardin.getUsuarioCreador());
        txtUsuarioCreador.setDisabled(false);
        txtUsuarioModificador.setValue(selectedJardin.getUsuarioModificador());
        txtUsuarioModificador.setDisabled(false);
        txtZona.setValue(selectedJardin.getZona());
        txtZona.setDisabled(false);
        txtUsuId_Usuario.setValue(selectedJardin.getUsuId_Usuario());
        txtUsuId_Usuario.setDisabled(false);
        txtJarId.setValue(selectedJardin.getJarId());
        txtJarId.setDisabled(true);
        btnSave.setDisabled(false);
        setShowDialog(true);

        return "";
    }

    public String action_save() {
        try {
            if ((selectedJardin == null) && (entity == null)) {
                action_create();
            } else {
                action_modify();
            }

            data = null;
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_create() {
        try {
            entity = new Jardin();

            Long jarId = FacesUtils.checkLong(txtJarId);

            entity.setAltitud(FacesUtils.checkString(txtAltitud));
            entity.setBarrio(FacesUtils.checkString(txtBarrio));
            entity.setDescripcion(FacesUtils.checkString(txtDescripcion));
            entity.setDireccion(FacesUtils.checkString(txtDireccion));
            entity.setEstado(FacesUtils.checkString(txtEstado));
            entity.setFechaCreacion(FacesUtils.checkDate(txtFechaCreacion));
            entity.setFechaModificacion(FacesUtils.checkDate(
                    txtFechaModificacion));
            entity.setJarId(jarId);
            entity.setLatitud(FacesUtils.checkString(txtLatitud));
            entity.setNombreJardin(FacesUtils.checkString(txtNombreJardin));
            entity.setUsuarioCreador(FacesUtils.checkString(txtUsuarioCreador));
            entity.setUsuarioModificador(FacesUtils.checkString(
                    txtUsuarioModificador));
            entity.setZona(FacesUtils.checkString(txtZona));
            entity.setUsuario((FacesUtils.checkLong(txtUsuId_Usuario) != null)
                ? businessDelegatorView.getUsuario(FacesUtils.checkLong(
                        txtUsuId_Usuario)) : null);
            businessDelegatorView.saveJardin(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYSAVED);
            action_clear();
        } catch (Exception e) {
            entity = null;
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_modify() {
        try {
            if (entity == null) {
                Long jarId = new Long(selectedJardin.getJarId());
                entity = businessDelegatorView.getJardin(jarId);
            }

            entity.setAltitud(FacesUtils.checkString(txtAltitud));
            entity.setBarrio(FacesUtils.checkString(txtBarrio));
            entity.setDescripcion(FacesUtils.checkString(txtDescripcion));
            entity.setDireccion(FacesUtils.checkString(txtDireccion));
            entity.setEstado(FacesUtils.checkString(txtEstado));
            entity.setFechaCreacion(FacesUtils.checkDate(txtFechaCreacion));
            entity.setFechaModificacion(FacesUtils.checkDate(
                    txtFechaModificacion));
            entity.setLatitud(FacesUtils.checkString(txtLatitud));
            entity.setNombreJardin(FacesUtils.checkString(txtNombreJardin));
            entity.setUsuarioCreador(FacesUtils.checkString(txtUsuarioCreador));
            entity.setUsuarioModificador(FacesUtils.checkString(
                    txtUsuarioModificador));
            entity.setZona(FacesUtils.checkString(txtZona));
            entity.setUsuario((FacesUtils.checkLong(txtUsuId_Usuario) != null)
                ? businessDelegatorView.getUsuario(FacesUtils.checkLong(
                        txtUsuId_Usuario)) : null);
            businessDelegatorView.updateJardin(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYMODIFIED);
        } catch (Exception e) {
            data = null;
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_delete_datatable(ActionEvent evt) {
        try {
            selectedJardin = (JardinDTO) (evt.getComponent().getAttributes()
                                             .get("selectedJardin"));

            Long jarId = new Long(selectedJardin.getJarId());
            entity = businessDelegatorView.getJardin(jarId);
            action_delete();
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_delete_master() {
        try {
            Long jarId = FacesUtils.checkLong(txtJarId);
            entity = businessDelegatorView.getJardin(jarId);
            action_delete();
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public void action_delete() throws Exception {
        try {
            businessDelegatorView.deleteJardin(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYDELETED);
            action_clear();
            data = null;
        } catch (Exception e) {
            throw e;
        }
    }

    public String action_closeDialog() {
        setShowDialog(false);
        action_clear();

        return "";
    }

    public String action_modifyWitDTO(String altitud, String barrio,
        String descripcion, String direccion, String estado,
        Date fechaCreacion, Date fechaModificacion, Long jarId, String latitud,
        String nombreJardin, String usuarioCreador, String usuarioModificador,
        String zona, Long usuId_Usuario) throws Exception {
        try {
            entity.setAltitud(FacesUtils.checkString(altitud));
            entity.setBarrio(FacesUtils.checkString(barrio));
            entity.setDescripcion(FacesUtils.checkString(descripcion));
            entity.setDireccion(FacesUtils.checkString(direccion));
            entity.setEstado(FacesUtils.checkString(estado));
            entity.setFechaCreacion(FacesUtils.checkDate(fechaCreacion));
            entity.setFechaModificacion(FacesUtils.checkDate(fechaModificacion));
            entity.setLatitud(FacesUtils.checkString(latitud));
            entity.setNombreJardin(FacesUtils.checkString(nombreJardin));
            entity.setUsuarioCreador(FacesUtils.checkString(usuarioCreador));
            entity.setUsuarioModificador(FacesUtils.checkString(
                    usuarioModificador));
            entity.setZona(FacesUtils.checkString(zona));
            businessDelegatorView.updateJardin(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYMODIFIED);
        } catch (Exception e) {
            //renderManager.getOnDemandRenderer("JardinView").requestRender();
            FacesUtils.addErrorMessage(e.getMessage());
            throw e;
        }

        return "";
    }

    public InputText getTxtAltitud() {
        return txtAltitud;
    }

    public void setTxtAltitud(InputText txtAltitud) {
        this.txtAltitud = txtAltitud;
    }

    public InputText getTxtBarrio() {
        return txtBarrio;
    }

    public void setTxtBarrio(InputText txtBarrio) {
        this.txtBarrio = txtBarrio;
    }

    public InputText getTxtDescripcion() {
        return txtDescripcion;
    }

    public void setTxtDescripcion(InputText txtDescripcion) {
        this.txtDescripcion = txtDescripcion;
    }

    public InputText getTxtDireccion() {
        return txtDireccion;
    }

    public void setTxtDireccion(InputText txtDireccion) {
        this.txtDireccion = txtDireccion;
    }

    public InputText getTxtEstado() {
        return txtEstado;
    }

    public void setTxtEstado(InputText txtEstado) {
        this.txtEstado = txtEstado;
    }

    public InputText getTxtLatitud() {
        return txtLatitud;
    }

    public void setTxtLatitud(InputText txtLatitud) {
        this.txtLatitud = txtLatitud;
    }

    public InputText getTxtNombreJardin() {
        return txtNombreJardin;
    }

    public void setTxtNombreJardin(InputText txtNombreJardin) {
        this.txtNombreJardin = txtNombreJardin;
    }

    public InputText getTxtUsuarioCreador() {
        return txtUsuarioCreador;
    }

    public void setTxtUsuarioCreador(InputText txtUsuarioCreador) {
        this.txtUsuarioCreador = txtUsuarioCreador;
    }

    public InputText getTxtUsuarioModificador() {
        return txtUsuarioModificador;
    }

    public void setTxtUsuarioModificador(InputText txtUsuarioModificador) {
        this.txtUsuarioModificador = txtUsuarioModificador;
    }

    public InputText getTxtZona() {
        return txtZona;
    }

    public void setTxtZona(InputText txtZona) {
        this.txtZona = txtZona;
    }

    public InputText getTxtUsuId_Usuario() {
        return txtUsuId_Usuario;
    }

    public void setTxtUsuId_Usuario(InputText txtUsuId_Usuario) {
        this.txtUsuId_Usuario = txtUsuId_Usuario;
    }

    public Calendar getTxtFechaCreacion() {
        return txtFechaCreacion;
    }

    public void setTxtFechaCreacion(Calendar txtFechaCreacion) {
        this.txtFechaCreacion = txtFechaCreacion;
    }

    public Calendar getTxtFechaModificacion() {
        return txtFechaModificacion;
    }

    public void setTxtFechaModificacion(Calendar txtFechaModificacion) {
        this.txtFechaModificacion = txtFechaModificacion;
    }

    public InputText getTxtJarId() {
        return txtJarId;
    }

    public void setTxtJarId(InputText txtJarId) {
        this.txtJarId = txtJarId;
    }

    public List<JardinDTO> getData() {
        try {
            if (data == null) {
                data = businessDelegatorView.getDataJardin();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public void setData(List<JardinDTO> jardinDTO) {
        this.data = jardinDTO;
    }

    public JardinDTO getSelectedJardin() {
        return selectedJardin;
    }

    public void setSelectedJardin(JardinDTO jardin) {
        this.selectedJardin = jardin;
    }

    public CommandButton getBtnSave() {
        return btnSave;
    }

    public void setBtnSave(CommandButton btnSave) {
        this.btnSave = btnSave;
    }

    public CommandButton getBtnModify() {
        return btnModify;
    }

    public void setBtnModify(CommandButton btnModify) {
        this.btnModify = btnModify;
    }

    public CommandButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(CommandButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public CommandButton getBtnClear() {
        return btnClear;
    }

    public void setBtnClear(CommandButton btnClear) {
        this.btnClear = btnClear;
    }

    public TimeZone getTimeZone() {
        return java.util.TimeZone.getDefault();
    }

    public IBusinessDelegatorView getBusinessDelegatorView() {
        return businessDelegatorView;
    }

    public void setBusinessDelegatorView(
        IBusinessDelegatorView businessDelegatorView) {
        this.businessDelegatorView = businessDelegatorView;
    }

    public boolean isShowDialog() {
        return showDialog;
    }

    public void setShowDialog(boolean showDialog) {
        this.showDialog = showDialog;
    }
}

package co.edu.usbcali.jardines.presentation.backingBeans;

import co.edu.usbcali.jardines.exceptions.*;
import co.edu.usbcali.jardines.model.*;
import co.edu.usbcali.jardines.model.dto.JardinEspecieDTO;
import co.edu.usbcali.jardines.presentation.businessDelegate.*;
import co.edu.usbcali.jardines.utilities.*;

import org.primefaces.component.calendar.*;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.inputtext.InputText;

import org.primefaces.event.RowEditEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

import java.sql.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;


/**
 * @author Zathura Code Generator http://zathuracode.org
 * www.zathuracode.org
 *
 */
@ManagedBean
@ViewScoped
public class JardinEspecieView implements Serializable {
    private static final long serialVersionUID = 1L;
    private static final Logger log = LoggerFactory.getLogger(JardinEspecieView.class);
    private InputText txtDescripcion;
    private InputText txtEstado;
    private InputText txtUsuarioCreador;
    private InputText txtUsuarioModificador;
    private InputText txtEspeId_Especie;
    private InputText txtJarId_Jardin;
    private InputText txtJarespId;
    private Calendar txtFechaCreacion;
    private Calendar txtFechaModificacion;
    private CommandButton btnSave;
    private CommandButton btnModify;
    private CommandButton btnDelete;
    private CommandButton btnClear;
    private List<JardinEspecieDTO> data;
    private JardinEspecieDTO selectedJardinEspecie;
    private JardinEspecie entity;
    private boolean showDialog;
    @ManagedProperty(value = "#{BusinessDelegatorView}")
    private IBusinessDelegatorView businessDelegatorView;

    public JardinEspecieView() {
        super();
    }

    public String action_new() {
        action_clear();
        selectedJardinEspecie = null;
        setShowDialog(true);

        return "";
    }

    public String action_clear() {
        entity = null;
        selectedJardinEspecie = null;

        if (txtDescripcion != null) {
            txtDescripcion.setValue(null);
            txtDescripcion.setDisabled(true);
        }

        if (txtEstado != null) {
            txtEstado.setValue(null);
            txtEstado.setDisabled(true);
        }

        if (txtUsuarioCreador != null) {
            txtUsuarioCreador.setValue(null);
            txtUsuarioCreador.setDisabled(true);
        }

        if (txtUsuarioModificador != null) {
            txtUsuarioModificador.setValue(null);
            txtUsuarioModificador.setDisabled(true);
        }

        if (txtEspeId_Especie != null) {
            txtEspeId_Especie.setValue(null);
            txtEspeId_Especie.setDisabled(true);
        }

        if (txtJarId_Jardin != null) {
            txtJarId_Jardin.setValue(null);
            txtJarId_Jardin.setDisabled(true);
        }

        if (txtFechaCreacion != null) {
            txtFechaCreacion.setValue(null);
            txtFechaCreacion.setDisabled(true);
        }

        if (txtFechaModificacion != null) {
            txtFechaModificacion.setValue(null);
            txtFechaModificacion.setDisabled(true);
        }

        if (txtJarespId != null) {
            txtJarespId.setValue(null);
            txtJarespId.setDisabled(false);
        }

        if (btnSave != null) {
            btnSave.setDisabled(true);
        }

        if (btnDelete != null) {
            btnDelete.setDisabled(true);
        }

        return "";
    }

    public void listener_txtFechaCreacion() {
        Date inputDate = (Date) txtFechaCreacion.getValue();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        FacesContext.getCurrentInstance()
                    .addMessage("",
            new FacesMessage("Selected Date " + dateFormat.format(inputDate)));
    }

    public void listener_txtFechaModificacion() {
        Date inputDate = (Date) txtFechaModificacion.getValue();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        FacesContext.getCurrentInstance()
                    .addMessage("",
            new FacesMessage("Selected Date " + dateFormat.format(inputDate)));
    }

    public void listener_txtId() {
        try {
            Long jarespId = FacesUtils.checkLong(txtJarespId);
            entity = (jarespId != null)
                ? businessDelegatorView.getJardinEspecie(jarespId) : null;
        } catch (Exception e) {
            entity = null;
        }

        if (entity == null) {
            txtDescripcion.setDisabled(false);
            txtEstado.setDisabled(false);
            txtUsuarioCreador.setDisabled(false);
            txtUsuarioModificador.setDisabled(false);
            txtEspeId_Especie.setDisabled(false);
            txtJarId_Jardin.setDisabled(false);
            txtFechaCreacion.setDisabled(false);
            txtFechaModificacion.setDisabled(false);
            txtJarespId.setDisabled(false);
            btnSave.setDisabled(false);
        } else {
            txtDescripcion.setValue(entity.getDescripcion());
            txtDescripcion.setDisabled(false);
            txtEstado.setValue(entity.getEstado());
            txtEstado.setDisabled(false);
            txtFechaCreacion.setValue(entity.getFechaCreacion());
            txtFechaCreacion.setDisabled(false);
            txtFechaModificacion.setValue(entity.getFechaModificacion());
            txtFechaModificacion.setDisabled(false);
            txtUsuarioCreador.setValue(entity.getUsuarioCreador());
            txtUsuarioCreador.setDisabled(false);
            txtUsuarioModificador.setValue(entity.getUsuarioModificador());
            txtUsuarioModificador.setDisabled(false);
            txtEspeId_Especie.setValue(entity.getEspecie().getEspeId());
            txtEspeId_Especie.setDisabled(false);
            txtJarId_Jardin.setValue(entity.getJardin().getJarId());
            txtJarId_Jardin.setDisabled(false);
            txtJarespId.setValue(entity.getJarespId());
            txtJarespId.setDisabled(true);
            btnSave.setDisabled(false);

            if (btnDelete != null) {
                btnDelete.setDisabled(false);
            }
        }
    }

    public String action_edit(ActionEvent evt) {
        selectedJardinEspecie = (JardinEspecieDTO) (evt.getComponent()
                                                       .getAttributes()
                                                       .get("selectedJardinEspecie"));
        txtDescripcion.setValue(selectedJardinEspecie.getDescripcion());
        txtDescripcion.setDisabled(false);
        txtEstado.setValue(selectedJardinEspecie.getEstado());
        txtEstado.setDisabled(false);
        txtFechaCreacion.setValue(selectedJardinEspecie.getFechaCreacion());
        txtFechaCreacion.setDisabled(false);
        txtFechaModificacion.setValue(selectedJardinEspecie.getFechaModificacion());
        txtFechaModificacion.setDisabled(false);
        txtUsuarioCreador.setValue(selectedJardinEspecie.getUsuarioCreador());
        txtUsuarioCreador.setDisabled(false);
        txtUsuarioModificador.setValue(selectedJardinEspecie.getUsuarioModificador());
        txtUsuarioModificador.setDisabled(false);
        txtEspeId_Especie.setValue(selectedJardinEspecie.getEspeId_Especie());
        txtEspeId_Especie.setDisabled(false);
        txtJarId_Jardin.setValue(selectedJardinEspecie.getJarId_Jardin());
        txtJarId_Jardin.setDisabled(false);
        txtJarespId.setValue(selectedJardinEspecie.getJarespId());
        txtJarespId.setDisabled(true);
        btnSave.setDisabled(false);
        setShowDialog(true);

        return "";
    }

    public String action_save() {
        try {
            if ((selectedJardinEspecie == null) && (entity == null)) {
                action_create();
            } else {
                action_modify();
            }

            data = null;
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_create() {
        try {
            entity = new JardinEspecie();

            Long jarespId = FacesUtils.checkLong(txtJarespId);

            entity.setDescripcion(FacesUtils.checkString(txtDescripcion));
            entity.setEstado(FacesUtils.checkString(txtEstado));
            entity.setFechaCreacion(FacesUtils.checkDate(txtFechaCreacion));
            entity.setFechaModificacion(FacesUtils.checkDate(
                    txtFechaModificacion));
            entity.setJarespId(jarespId);
            entity.setUsuarioCreador(FacesUtils.checkString(txtUsuarioCreador));
            entity.setUsuarioModificador(FacesUtils.checkString(
                    txtUsuarioModificador));
            entity.setEspecie((FacesUtils.checkLong(txtEspeId_Especie) != null)
                ? businessDelegatorView.getEspecie(FacesUtils.checkLong(
                        txtEspeId_Especie)) : null);
            entity.setJardin((FacesUtils.checkLong(txtJarId_Jardin) != null)
                ? businessDelegatorView.getJardin(FacesUtils.checkLong(
                        txtJarId_Jardin)) : null);
            businessDelegatorView.saveJardinEspecie(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYSAVED);
            action_clear();
        } catch (Exception e) {
            entity = null;
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_modify() {
        try {
            if (entity == null) {
                Long jarespId = new Long(selectedJardinEspecie.getJarespId());
                entity = businessDelegatorView.getJardinEspecie(jarespId);
            }

            entity.setDescripcion(FacesUtils.checkString(txtDescripcion));
            entity.setEstado(FacesUtils.checkString(txtEstado));
            entity.setFechaCreacion(FacesUtils.checkDate(txtFechaCreacion));
            entity.setFechaModificacion(FacesUtils.checkDate(
                    txtFechaModificacion));
            entity.setUsuarioCreador(FacesUtils.checkString(txtUsuarioCreador));
            entity.setUsuarioModificador(FacesUtils.checkString(
                    txtUsuarioModificador));
            entity.setEspecie((FacesUtils.checkLong(txtEspeId_Especie) != null)
                ? businessDelegatorView.getEspecie(FacesUtils.checkLong(
                        txtEspeId_Especie)) : null);
            entity.setJardin((FacesUtils.checkLong(txtJarId_Jardin) != null)
                ? businessDelegatorView.getJardin(FacesUtils.checkLong(
                        txtJarId_Jardin)) : null);
            businessDelegatorView.updateJardinEspecie(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYMODIFIED);
        } catch (Exception e) {
            data = null;
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_delete_datatable(ActionEvent evt) {
        try {
            selectedJardinEspecie = (JardinEspecieDTO) (evt.getComponent()
                                                           .getAttributes()
                                                           .get("selectedJardinEspecie"));

            Long jarespId = new Long(selectedJardinEspecie.getJarespId());
            entity = businessDelegatorView.getJardinEspecie(jarespId);
            action_delete();
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_delete_master() {
        try {
            Long jarespId = FacesUtils.checkLong(txtJarespId);
            entity = businessDelegatorView.getJardinEspecie(jarespId);
            action_delete();
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public void action_delete() throws Exception {
        try {
            businessDelegatorView.deleteJardinEspecie(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYDELETED);
            action_clear();
            data = null;
        } catch (Exception e) {
            throw e;
        }
    }

    public String action_closeDialog() {
        setShowDialog(false);
        action_clear();

        return "";
    }

    public String action_modifyWitDTO(String descripcion, String estado,
        Date fechaCreacion, Date fechaModificacion, Long jarespId,
        String usuarioCreador, String usuarioModificador, Long espeId_Especie,
        Long jarId_Jardin) throws Exception {
        try {
            entity.setDescripcion(FacesUtils.checkString(descripcion));
            entity.setEstado(FacesUtils.checkString(estado));
            entity.setFechaCreacion(FacesUtils.checkDate(fechaCreacion));
            entity.setFechaModificacion(FacesUtils.checkDate(fechaModificacion));
            entity.setUsuarioCreador(FacesUtils.checkString(usuarioCreador));
            entity.setUsuarioModificador(FacesUtils.checkString(
                    usuarioModificador));
            businessDelegatorView.updateJardinEspecie(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYMODIFIED);
        } catch (Exception e) {
            //renderManager.getOnDemandRenderer("JardinEspecieView").requestRender();
            FacesUtils.addErrorMessage(e.getMessage());
            throw e;
        }

        return "";
    }

    public InputText getTxtDescripcion() {
        return txtDescripcion;
    }

    public void setTxtDescripcion(InputText txtDescripcion) {
        this.txtDescripcion = txtDescripcion;
    }

    public InputText getTxtEstado() {
        return txtEstado;
    }

    public void setTxtEstado(InputText txtEstado) {
        this.txtEstado = txtEstado;
    }

    public InputText getTxtUsuarioCreador() {
        return txtUsuarioCreador;
    }

    public void setTxtUsuarioCreador(InputText txtUsuarioCreador) {
        this.txtUsuarioCreador = txtUsuarioCreador;
    }

    public InputText getTxtUsuarioModificador() {
        return txtUsuarioModificador;
    }

    public void setTxtUsuarioModificador(InputText txtUsuarioModificador) {
        this.txtUsuarioModificador = txtUsuarioModificador;
    }

    public InputText getTxtEspeId_Especie() {
        return txtEspeId_Especie;
    }

    public void setTxtEspeId_Especie(InputText txtEspeId_Especie) {
        this.txtEspeId_Especie = txtEspeId_Especie;
    }

    public InputText getTxtJarId_Jardin() {
        return txtJarId_Jardin;
    }

    public void setTxtJarId_Jardin(InputText txtJarId_Jardin) {
        this.txtJarId_Jardin = txtJarId_Jardin;
    }

    public Calendar getTxtFechaCreacion() {
        return txtFechaCreacion;
    }

    public void setTxtFechaCreacion(Calendar txtFechaCreacion) {
        this.txtFechaCreacion = txtFechaCreacion;
    }

    public Calendar getTxtFechaModificacion() {
        return txtFechaModificacion;
    }

    public void setTxtFechaModificacion(Calendar txtFechaModificacion) {
        this.txtFechaModificacion = txtFechaModificacion;
    }

    public InputText getTxtJarespId() {
        return txtJarespId;
    }

    public void setTxtJarespId(InputText txtJarespId) {
        this.txtJarespId = txtJarespId;
    }

    public List<JardinEspecieDTO> getData() {
        try {
            if (data == null) {
                data = businessDelegatorView.getDataJardinEspecie();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public void setData(List<JardinEspecieDTO> jardinEspecieDTO) {
        this.data = jardinEspecieDTO;
    }

    public JardinEspecieDTO getSelectedJardinEspecie() {
        return selectedJardinEspecie;
    }

    public void setSelectedJardinEspecie(JardinEspecieDTO jardinEspecie) {
        this.selectedJardinEspecie = jardinEspecie;
    }

    public CommandButton getBtnSave() {
        return btnSave;
    }

    public void setBtnSave(CommandButton btnSave) {
        this.btnSave = btnSave;
    }

    public CommandButton getBtnModify() {
        return btnModify;
    }

    public void setBtnModify(CommandButton btnModify) {
        this.btnModify = btnModify;
    }

    public CommandButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(CommandButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public CommandButton getBtnClear() {
        return btnClear;
    }

    public void setBtnClear(CommandButton btnClear) {
        this.btnClear = btnClear;
    }

    public TimeZone getTimeZone() {
        return java.util.TimeZone.getDefault();
    }

    public IBusinessDelegatorView getBusinessDelegatorView() {
        return businessDelegatorView;
    }

    public void setBusinessDelegatorView(
        IBusinessDelegatorView businessDelegatorView) {
        this.businessDelegatorView = businessDelegatorView;
    }

    public boolean isShowDialog() {
        return showDialog;
    }

    public void setShowDialog(boolean showDialog) {
        this.showDialog = showDialog;
    }
}

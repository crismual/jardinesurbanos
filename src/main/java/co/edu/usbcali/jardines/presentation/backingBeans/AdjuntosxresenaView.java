package co.edu.usbcali.jardines.presentation.backingBeans;

import co.edu.usbcali.jardines.exceptions.*;
import co.edu.usbcali.jardines.model.*;
import co.edu.usbcali.jardines.model.dto.AdjuntosxresenaDTO;
import co.edu.usbcali.jardines.presentation.businessDelegate.*;
import co.edu.usbcali.jardines.utilities.*;

import org.primefaces.component.calendar.*;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.inputtext.InputText;

import org.primefaces.event.RowEditEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

import java.sql.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;


/**
 * @author Zathura Code Generator http://zathuracode.org
 * www.zathuracode.org
 *
 */
@ManagedBean
@ViewScoped
public class AdjuntosxresenaView implements Serializable {
    private static final long serialVersionUID = 1L;
    private static final Logger log = LoggerFactory.getLogger(AdjuntosxresenaView.class);
    private InputText txtDescripcion;
    private InputText txtEstado;
    private InputText txtUrlFotoRes;
    private InputText txtUsuarioCreador;
    private InputText txtUsuarioModificador;
    private InputText txtResId_Resena;
    private InputText txtAdresId;
    private Calendar txtFechaCreacion;
    private Calendar txtFechaModificacion;
    private CommandButton btnSave;
    private CommandButton btnModify;
    private CommandButton btnDelete;
    private CommandButton btnClear;
    private List<AdjuntosxresenaDTO> data;
    private AdjuntosxresenaDTO selectedAdjuntosxresena;
    private Adjuntosxresena entity;
    private boolean showDialog;
    @ManagedProperty(value = "#{BusinessDelegatorView}")
    private IBusinessDelegatorView businessDelegatorView;

    public AdjuntosxresenaView() {
        super();
    }

    public String action_new() {
        action_clear();
        selectedAdjuntosxresena = null;
        setShowDialog(true);

        return "";
    }

    public String action_clear() {
        entity = null;
        selectedAdjuntosxresena = null;

        if (txtDescripcion != null) {
            txtDescripcion.setValue(null);
            txtDescripcion.setDisabled(true);
        }

        if (txtEstado != null) {
            txtEstado.setValue(null);
            txtEstado.setDisabled(true);
        }

        if (txtUrlFotoRes != null) {
            txtUrlFotoRes.setValue(null);
            txtUrlFotoRes.setDisabled(true);
        }

        if (txtUsuarioCreador != null) {
            txtUsuarioCreador.setValue(null);
            txtUsuarioCreador.setDisabled(true);
        }

        if (txtUsuarioModificador != null) {
            txtUsuarioModificador.setValue(null);
            txtUsuarioModificador.setDisabled(true);
        }

        if (txtResId_Resena != null) {
            txtResId_Resena.setValue(null);
            txtResId_Resena.setDisabled(true);
        }

        if (txtFechaCreacion != null) {
            txtFechaCreacion.setValue(null);
            txtFechaCreacion.setDisabled(true);
        }

        if (txtFechaModificacion != null) {
            txtFechaModificacion.setValue(null);
            txtFechaModificacion.setDisabled(true);
        }

        if (txtAdresId != null) {
            txtAdresId.setValue(null);
            txtAdresId.setDisabled(false);
        }

        if (btnSave != null) {
            btnSave.setDisabled(true);
        }

        if (btnDelete != null) {
            btnDelete.setDisabled(true);
        }

        return "";
    }

    public void listener_txtFechaCreacion() {
        Date inputDate = (Date) txtFechaCreacion.getValue();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        FacesContext.getCurrentInstance()
                    .addMessage("",
            new FacesMessage("Selected Date " + dateFormat.format(inputDate)));
    }

    public void listener_txtFechaModificacion() {
        Date inputDate = (Date) txtFechaModificacion.getValue();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        FacesContext.getCurrentInstance()
                    .addMessage("",
            new FacesMessage("Selected Date " + dateFormat.format(inputDate)));
    }

    public void listener_txtId() {
        try {
            Long adresId = FacesUtils.checkLong(txtAdresId);
            entity = (adresId != null)
                ? businessDelegatorView.getAdjuntosxresena(adresId) : null;
        } catch (Exception e) {
            entity = null;
        }

        if (entity == null) {
            txtDescripcion.setDisabled(false);
            txtEstado.setDisabled(false);
            txtUrlFotoRes.setDisabled(false);
            txtUsuarioCreador.setDisabled(false);
            txtUsuarioModificador.setDisabled(false);
            txtResId_Resena.setDisabled(false);
            txtFechaCreacion.setDisabled(false);
            txtFechaModificacion.setDisabled(false);
            txtAdresId.setDisabled(false);
            btnSave.setDisabled(false);
        } else {
            txtDescripcion.setValue(entity.getDescripcion());
            txtDescripcion.setDisabled(false);
            txtEstado.setValue(entity.getEstado());
            txtEstado.setDisabled(false);
            txtFechaCreacion.setValue(entity.getFechaCreacion());
            txtFechaCreacion.setDisabled(false);
            txtFechaModificacion.setValue(entity.getFechaModificacion());
            txtFechaModificacion.setDisabled(false);
            txtUrlFotoRes.setValue(entity.getUrlFotoRes());
            txtUrlFotoRes.setDisabled(false);
            txtUsuarioCreador.setValue(entity.getUsuarioCreador());
            txtUsuarioCreador.setDisabled(false);
            txtUsuarioModificador.setValue(entity.getUsuarioModificador());
            txtUsuarioModificador.setDisabled(false);
            txtResId_Resena.setValue(entity.getResena().getResId());
            txtResId_Resena.setDisabled(false);
            txtAdresId.setValue(entity.getAdresId());
            txtAdresId.setDisabled(true);
            btnSave.setDisabled(false);

            if (btnDelete != null) {
                btnDelete.setDisabled(false);
            }
        }
    }

    public String action_edit(ActionEvent evt) {
        selectedAdjuntosxresena = (AdjuntosxresenaDTO) (evt.getComponent()
                                                           .getAttributes()
                                                           .get("selectedAdjuntosxresena"));
        txtDescripcion.setValue(selectedAdjuntosxresena.getDescripcion());
        txtDescripcion.setDisabled(false);
        txtEstado.setValue(selectedAdjuntosxresena.getEstado());
        txtEstado.setDisabled(false);
        txtFechaCreacion.setValue(selectedAdjuntosxresena.getFechaCreacion());
        txtFechaCreacion.setDisabled(false);
        txtFechaModificacion.setValue(selectedAdjuntosxresena.getFechaModificacion());
        txtFechaModificacion.setDisabled(false);
        txtUrlFotoRes.setValue(selectedAdjuntosxresena.getUrlFotoRes());
        txtUrlFotoRes.setDisabled(false);
        txtUsuarioCreador.setValue(selectedAdjuntosxresena.getUsuarioCreador());
        txtUsuarioCreador.setDisabled(false);
        txtUsuarioModificador.setValue(selectedAdjuntosxresena.getUsuarioModificador());
        txtUsuarioModificador.setDisabled(false);
        txtResId_Resena.setValue(selectedAdjuntosxresena.getResId_Resena());
        txtResId_Resena.setDisabled(false);
        txtAdresId.setValue(selectedAdjuntosxresena.getAdresId());
        txtAdresId.setDisabled(true);
        btnSave.setDisabled(false);
        setShowDialog(true);

        return "";
    }

    public String action_save() {
        try {
            if ((selectedAdjuntosxresena == null) && (entity == null)) {
                action_create();
            } else {
                action_modify();
            }

            data = null;
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_create() {
        try {
            entity = new Adjuntosxresena();

            Long adresId = FacesUtils.checkLong(txtAdresId);

            entity.setAdresId(adresId);
            entity.setDescripcion(FacesUtils.checkString(txtDescripcion));
            entity.setEstado(FacesUtils.checkString(txtEstado));
            entity.setFechaCreacion(FacesUtils.checkDate(txtFechaCreacion));
            entity.setFechaModificacion(FacesUtils.checkDate(
                    txtFechaModificacion));
            entity.setUrlFotoRes(FacesUtils.checkString(txtUrlFotoRes));
            entity.setUsuarioCreador(FacesUtils.checkString(txtUsuarioCreador));
            entity.setUsuarioModificador(FacesUtils.checkString(
                    txtUsuarioModificador));
            entity.setResena((FacesUtils.checkLong(txtResId_Resena) != null)
                ? businessDelegatorView.getResena(FacesUtils.checkLong(
                        txtResId_Resena)) : null);
            businessDelegatorView.saveAdjuntosxresena(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYSAVED);
            action_clear();
        } catch (Exception e) {
            entity = null;
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_modify() {
        try {
            if (entity == null) {
                Long adresId = new Long(selectedAdjuntosxresena.getAdresId());
                entity = businessDelegatorView.getAdjuntosxresena(adresId);
            }

            entity.setDescripcion(FacesUtils.checkString(txtDescripcion));
            entity.setEstado(FacesUtils.checkString(txtEstado));
            entity.setFechaCreacion(FacesUtils.checkDate(txtFechaCreacion));
            entity.setFechaModificacion(FacesUtils.checkDate(
                    txtFechaModificacion));
            entity.setUrlFotoRes(FacesUtils.checkString(txtUrlFotoRes));
            entity.setUsuarioCreador(FacesUtils.checkString(txtUsuarioCreador));
            entity.setUsuarioModificador(FacesUtils.checkString(
                    txtUsuarioModificador));
            entity.setResena((FacesUtils.checkLong(txtResId_Resena) != null)
                ? businessDelegatorView.getResena(FacesUtils.checkLong(
                        txtResId_Resena)) : null);
            businessDelegatorView.updateAdjuntosxresena(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYMODIFIED);
        } catch (Exception e) {
            data = null;
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_delete_datatable(ActionEvent evt) {
        try {
            selectedAdjuntosxresena = (AdjuntosxresenaDTO) (evt.getComponent()
                                                               .getAttributes()
                                                               .get("selectedAdjuntosxresena"));

            Long adresId = new Long(selectedAdjuntosxresena.getAdresId());
            entity = businessDelegatorView.getAdjuntosxresena(adresId);
            action_delete();
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_delete_master() {
        try {
            Long adresId = FacesUtils.checkLong(txtAdresId);
            entity = businessDelegatorView.getAdjuntosxresena(adresId);
            action_delete();
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public void action_delete() throws Exception {
        try {
            businessDelegatorView.deleteAdjuntosxresena(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYDELETED);
            action_clear();
            data = null;
        } catch (Exception e) {
            throw e;
        }
    }

    public String action_closeDialog() {
        setShowDialog(false);
        action_clear();

        return "";
    }

    public String action_modifyWitDTO(Long adresId, String descripcion,
        String estado, Date fechaCreacion, Date fechaModificacion,
        String urlFotoRes, String usuarioCreador, String usuarioModificador,
        Long resId_Resena) throws Exception {
        try {
            entity.setDescripcion(FacesUtils.checkString(descripcion));
            entity.setEstado(FacesUtils.checkString(estado));
            entity.setFechaCreacion(FacesUtils.checkDate(fechaCreacion));
            entity.setFechaModificacion(FacesUtils.checkDate(fechaModificacion));
            entity.setUrlFotoRes(FacesUtils.checkString(urlFotoRes));
            entity.setUsuarioCreador(FacesUtils.checkString(usuarioCreador));
            entity.setUsuarioModificador(FacesUtils.checkString(
                    usuarioModificador));
            businessDelegatorView.updateAdjuntosxresena(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYMODIFIED);
        } catch (Exception e) {
            //renderManager.getOnDemandRenderer("AdjuntosxresenaView").requestRender();
            FacesUtils.addErrorMessage(e.getMessage());
            throw e;
        }

        return "";
    }

    public InputText getTxtDescripcion() {
        return txtDescripcion;
    }

    public void setTxtDescripcion(InputText txtDescripcion) {
        this.txtDescripcion = txtDescripcion;
    }

    public InputText getTxtEstado() {
        return txtEstado;
    }

    public void setTxtEstado(InputText txtEstado) {
        this.txtEstado = txtEstado;
    }

    public InputText getTxtUrlFotoRes() {
        return txtUrlFotoRes;
    }

    public void setTxtUrlFotoRes(InputText txtUrlFotoRes) {
        this.txtUrlFotoRes = txtUrlFotoRes;
    }

    public InputText getTxtUsuarioCreador() {
        return txtUsuarioCreador;
    }

    public void setTxtUsuarioCreador(InputText txtUsuarioCreador) {
        this.txtUsuarioCreador = txtUsuarioCreador;
    }

    public InputText getTxtUsuarioModificador() {
        return txtUsuarioModificador;
    }

    public void setTxtUsuarioModificador(InputText txtUsuarioModificador) {
        this.txtUsuarioModificador = txtUsuarioModificador;
    }

    public InputText getTxtResId_Resena() {
        return txtResId_Resena;
    }

    public void setTxtResId_Resena(InputText txtResId_Resena) {
        this.txtResId_Resena = txtResId_Resena;
    }

    public Calendar getTxtFechaCreacion() {
        return txtFechaCreacion;
    }

    public void setTxtFechaCreacion(Calendar txtFechaCreacion) {
        this.txtFechaCreacion = txtFechaCreacion;
    }

    public Calendar getTxtFechaModificacion() {
        return txtFechaModificacion;
    }

    public void setTxtFechaModificacion(Calendar txtFechaModificacion) {
        this.txtFechaModificacion = txtFechaModificacion;
    }

    public InputText getTxtAdresId() {
        return txtAdresId;
    }

    public void setTxtAdresId(InputText txtAdresId) {
        this.txtAdresId = txtAdresId;
    }

    public List<AdjuntosxresenaDTO> getData() {
        try {
            if (data == null) {
                data = businessDelegatorView.getDataAdjuntosxresena();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public void setData(List<AdjuntosxresenaDTO> adjuntosxresenaDTO) {
        this.data = adjuntosxresenaDTO;
    }

    public AdjuntosxresenaDTO getSelectedAdjuntosxresena() {
        return selectedAdjuntosxresena;
    }

    public void setSelectedAdjuntosxresena(AdjuntosxresenaDTO adjuntosxresena) {
        this.selectedAdjuntosxresena = adjuntosxresena;
    }

    public CommandButton getBtnSave() {
        return btnSave;
    }

    public void setBtnSave(CommandButton btnSave) {
        this.btnSave = btnSave;
    }

    public CommandButton getBtnModify() {
        return btnModify;
    }

    public void setBtnModify(CommandButton btnModify) {
        this.btnModify = btnModify;
    }

    public CommandButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(CommandButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public CommandButton getBtnClear() {
        return btnClear;
    }

    public void setBtnClear(CommandButton btnClear) {
        this.btnClear = btnClear;
    }

    public TimeZone getTimeZone() {
        return java.util.TimeZone.getDefault();
    }

    public IBusinessDelegatorView getBusinessDelegatorView() {
        return businessDelegatorView;
    }

    public void setBusinessDelegatorView(
        IBusinessDelegatorView businessDelegatorView) {
        this.businessDelegatorView = businessDelegatorView;
    }

    public boolean isShowDialog() {
        return showDialog;
    }

    public void setShowDialog(boolean showDialog) {
        this.showDialog = showDialog;
    }
}

package co.edu.usbcali.jardines.presentation.backingBeans;

import co.edu.usbcali.jardines.exceptions.*;
import co.edu.usbcali.jardines.model.*;
import co.edu.usbcali.jardines.model.dto.GaleriaDTO;
import co.edu.usbcali.jardines.presentation.businessDelegate.*;
import co.edu.usbcali.jardines.utilities.*;

import org.primefaces.component.calendar.*;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.inputtext.InputText;

import org.primefaces.event.RowEditEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

import java.sql.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;


/**
 * @author Zathura Code Generator http://zathuracode.org
 * www.zathuracode.org
 *
 */
@ManagedBean
@ViewScoped
public class GaleriaView implements Serializable {
    private static final long serialVersionUID = 1L;
    private static final Logger log = LoggerFactory.getLogger(GaleriaView.class);
    private InputText txtJarId_Jardin;
    private InputText txtUsuId_Usuario;
    private InputText txtGalId;
    private InputText txtUsuarioCreador;
    private InputText txtUsuarioModificador;
    private InputText txtEstado;
    private InputText txtDescripcion;
    private InputText txtUrlFotoGaleria;
    private InputText txtJardinJarId;
    private InputText txtUsuarioUsuId;
    private Calendar txtFechaCreacion;
    private Calendar txtFechaModificacion;
    private CommandButton btnSave;
    private CommandButton btnModify;
    private CommandButton btnDelete;
    private CommandButton btnClear;
    private List<GaleriaDTO> data;
    private GaleriaDTO selectedGaleria;
    private Galeria entity;
    private boolean showDialog;
    @ManagedProperty(value = "#{BusinessDelegatorView}")
    private IBusinessDelegatorView businessDelegatorView;

    public GaleriaView() {
        super();
    }

    public String action_new() {
        action_clear();
        selectedGaleria = null;
        setShowDialog(true);

        return "";
    }

    public String action_clear() {
        entity = null;
        selectedGaleria = null;

        if (txtJarId_Jardin != null) {
            txtJarId_Jardin.setValue(null);
            txtJarId_Jardin.setDisabled(true);
        }

        if (txtUsuId_Usuario != null) {
            txtUsuId_Usuario.setValue(null);
            txtUsuId_Usuario.setDisabled(true);
        }

        if (txtGalId != null) {
            txtGalId.setValue(null);
            txtGalId.setDisabled(false);
        }

        if (txtUsuarioCreador != null) {
            txtUsuarioCreador.setValue(null);
            txtUsuarioCreador.setDisabled(false);
        }

        if (txtUsuarioModificador != null) {
            txtUsuarioModificador.setValue(null);
            txtUsuarioModificador.setDisabled(false);
        }

        if (txtEstado != null) {
            txtEstado.setValue(null);
            txtEstado.setDisabled(false);
        }

        if (txtDescripcion != null) {
            txtDescripcion.setValue(null);
            txtDescripcion.setDisabled(false);
        }

        if (txtUrlFotoGaleria != null) {
            txtUrlFotoGaleria.setValue(null);
            txtUrlFotoGaleria.setDisabled(false);
        }

        if (txtJardinJarId != null) {
            txtJardinJarId.setValue(null);
            txtJardinJarId.setDisabled(false);
        }

        if (txtUsuarioUsuId != null) {
            txtUsuarioUsuId.setValue(null);
            txtUsuarioUsuId.setDisabled(false);
        }

        if (txtFechaCreacion != null) {
            txtFechaCreacion.setValue(null);
            txtFechaCreacion.setDisabled(false);
        }

        if (txtFechaModificacion != null) {
            txtFechaModificacion.setValue(null);
            txtFechaModificacion.setDisabled(false);
        }

        if (btnSave != null) {
            btnSave.setDisabled(true);
        }

        if (btnDelete != null) {
            btnDelete.setDisabled(true);
        }

        return "";
    }

    public void listener_txtFechaCreacion() {
        Date inputDate = (Date) txtFechaCreacion.getValue();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        FacesContext.getCurrentInstance()
                    .addMessage("",
            new FacesMessage("Selected Date " + dateFormat.format(inputDate)));
    }

    public void listener_txtFechaModificacion() {
        Date inputDate = (Date) txtFechaModificacion.getValue();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        FacesContext.getCurrentInstance()
                    .addMessage("",
            new FacesMessage("Selected Date " + dateFormat.format(inputDate)));
    }

    public void listener_txtId() {
        try {
            GaleriaId id = new GaleriaId();
            id.setGalId((((txtGalId.getValue()) == null) ||
                (txtGalId.getValue()).equals("")) ? null
                                                  : FacesUtils.checkLong(
                    txtGalId));
            id.setFechaCreacion((((txtFechaCreacion.getValue()) == null) ||
                (txtFechaCreacion.getValue()).equals("")) ? null
                                                          : FacesUtils.checkDate(
                    txtFechaCreacion.getValue()));
            id.setFechaModificacion((((txtFechaModificacion.getValue()) == null) ||
                (txtFechaModificacion.getValue()).equals("")) ? null
                                                              : FacesUtils.checkDate(
                    txtFechaModificacion.getValue()));
            id.setUsuarioCreador((((txtUsuarioCreador.getValue()) == null) ||
                (txtUsuarioCreador.getValue()).equals("")) ? null
                                                           : FacesUtils.checkString(
                    txtUsuarioCreador));
            id.setUsuarioModificador((((txtUsuarioModificador.getValue()) == null) ||
                (txtUsuarioModificador.getValue()).equals("")) ? null
                                                               : FacesUtils.checkString(
                    txtUsuarioModificador));
            id.setEstado((((txtEstado.getValue()) == null) ||
                (txtEstado.getValue()).equals("")) ? null
                                                   : FacesUtils.checkString(
                    txtEstado));
            id.setDescripcion((((txtDescripcion.getValue()) == null) ||
                (txtDescripcion.getValue()).equals("")) ? null
                                                        : FacesUtils.checkString(
                    txtDescripcion));
            id.setUrlFotoGaleria((((txtUrlFotoGaleria.getValue()) == null) ||
                (txtUrlFotoGaleria.getValue()).equals("")) ? null
                                                           : FacesUtils.checkString(
                    txtUrlFotoGaleria));
            id.setJardinJarId((((txtJardinJarId.getValue()) == null) ||
                (txtJardinJarId.getValue()).equals("")) ? null
                                                        : FacesUtils.checkLong(
                    txtJardinJarId));
            id.setUsuarioUsuId((((txtUsuarioUsuId.getValue()) == null) ||
                (txtUsuarioUsuId.getValue()).equals("")) ? null
                                                         : FacesUtils.checkLong(
                    txtUsuarioUsuId));
            entity = (id != null) ? businessDelegatorView.getGaleria(id) : null;
        } catch (Exception e) {
            entity = null;
        }

        if (entity == null) {
            txtJarId_Jardin.setDisabled(false);
            txtUsuId_Usuario.setDisabled(false);
            txtGalId.setDisabled(false);
            txtUsuarioCreador.setDisabled(false);
            txtUsuarioModificador.setDisabled(false);
            txtEstado.setDisabled(false);
            txtDescripcion.setDisabled(false);
            txtUrlFotoGaleria.setDisabled(false);
            txtJardinJarId.setDisabled(false);
            txtUsuarioUsuId.setDisabled(false);
            txtFechaCreacion.setDisabled(false);
            txtFechaModificacion.setDisabled(false);
            btnSave.setDisabled(false);
        } else {
            txtJarId_Jardin.setValue(entity.getJardin().getJarId());
            txtJarId_Jardin.setDisabled(false);
            txtUsuId_Usuario.setValue(entity.getUsuario().getUsuId());
            txtUsuId_Usuario.setDisabled(false);
            txtGalId.setValue(entity.getId().getGalId());
            txtGalId.setDisabled(true);
            txtFechaCreacion.setValue(entity.getId().getFechaCreacion());
            txtFechaCreacion.setDisabled(true);
            txtFechaModificacion.setValue(entity.getId().getFechaModificacion());
            txtFechaModificacion.setDisabled(true);
            txtUsuarioCreador.setValue(entity.getId().getUsuarioCreador());
            txtUsuarioCreador.setDisabled(true);
            txtUsuarioModificador.setValue(entity.getId().getUsuarioModificador());
            txtUsuarioModificador.setDisabled(true);
            txtEstado.setValue(entity.getId().getEstado());
            txtEstado.setDisabled(true);
            txtDescripcion.setValue(entity.getId().getDescripcion());
            txtDescripcion.setDisabled(true);
            txtUrlFotoGaleria.setValue(entity.getId().getUrlFotoGaleria());
            txtUrlFotoGaleria.setDisabled(true);
            txtJardinJarId.setValue(entity.getId().getJardinJarId());
            txtJardinJarId.setDisabled(true);
            txtUsuarioUsuId.setValue(entity.getId().getUsuarioUsuId());
            txtUsuarioUsuId.setDisabled(true);
            btnSave.setDisabled(false);

            if (btnDelete != null) {
                btnDelete.setDisabled(false);
            }
        }
    }

    public String action_edit(ActionEvent evt) {
        selectedGaleria = (GaleriaDTO) (evt.getComponent().getAttributes()
                                           .get("selectedGaleria"));
        txtJarId_Jardin.setValue(selectedGaleria.getJarId_Jardin());
        txtJarId_Jardin.setDisabled(false);
        txtUsuId_Usuario.setValue(selectedGaleria.getUsuId_Usuario());
        txtUsuId_Usuario.setDisabled(false);
        txtGalId.setValue(selectedGaleria.getGalId());
        txtGalId.setDisabled(true);
        txtFechaCreacion.setValue(selectedGaleria.getFechaCreacion());
        txtFechaCreacion.setDisabled(true);
        txtFechaModificacion.setValue(selectedGaleria.getFechaModificacion());
        txtFechaModificacion.setDisabled(true);
        txtUsuarioCreador.setValue(selectedGaleria.getUsuarioCreador());
        txtUsuarioCreador.setDisabled(true);
        txtUsuarioModificador.setValue(selectedGaleria.getUsuarioModificador());
        txtUsuarioModificador.setDisabled(true);
        txtEstado.setValue(selectedGaleria.getEstado());
        txtEstado.setDisabled(true);
        txtDescripcion.setValue(selectedGaleria.getDescripcion());
        txtDescripcion.setDisabled(true);
        txtUrlFotoGaleria.setValue(selectedGaleria.getUrlFotoGaleria());
        txtUrlFotoGaleria.setDisabled(true);
        txtJardinJarId.setValue(selectedGaleria.getJardinJarId());
        txtJardinJarId.setDisabled(true);
        txtUsuarioUsuId.setValue(selectedGaleria.getUsuarioUsuId());
        txtUsuarioUsuId.setDisabled(true);
        btnSave.setDisabled(false);
        setShowDialog(true);

        return "";
    }

    public String action_save() {
        try {
            if ((selectedGaleria == null) && (entity == null)) {
                action_create();
            } else {
                action_modify();
            }

            data = null;
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_create() {
        try {
            entity = new Galeria();

            GaleriaId id = new GaleriaId();
            id.setGalId((((txtGalId.getValue()) == null) ||
                (txtGalId.getValue()).equals("")) ? null
                                                  : FacesUtils.checkLong(
                    txtGalId));
            id.setFechaCreacion((((txtFechaCreacion.getValue()) == null) ||
                (txtFechaCreacion.getValue()).equals("")) ? null
                                                          : FacesUtils.checkDate(
                    txtFechaCreacion.getValue()));
            id.setFechaModificacion((((txtFechaModificacion.getValue()) == null) ||
                (txtFechaModificacion.getValue()).equals("")) ? null
                                                              : FacesUtils.checkDate(
                    txtFechaModificacion.getValue()));
            id.setUsuarioCreador((((txtUsuarioCreador.getValue()) == null) ||
                (txtUsuarioCreador.getValue()).equals("")) ? null
                                                           : FacesUtils.checkString(
                    txtUsuarioCreador));
            id.setUsuarioModificador((((txtUsuarioModificador.getValue()) == null) ||
                (txtUsuarioModificador.getValue()).equals("")) ? null
                                                               : FacesUtils.checkString(
                    txtUsuarioModificador));
            id.setEstado((((txtEstado.getValue()) == null) ||
                (txtEstado.getValue()).equals("")) ? null
                                                   : FacesUtils.checkString(
                    txtEstado));
            id.setDescripcion((((txtDescripcion.getValue()) == null) ||
                (txtDescripcion.getValue()).equals("")) ? null
                                                        : FacesUtils.checkString(
                    txtDescripcion));
            id.setUrlFotoGaleria((((txtUrlFotoGaleria.getValue()) == null) ||
                (txtUrlFotoGaleria.getValue()).equals("")) ? null
                                                           : FacesUtils.checkString(
                    txtUrlFotoGaleria));
            id.setJardinJarId((((txtJardinJarId.getValue()) == null) ||
                (txtJardinJarId.getValue()).equals("")) ? null
                                                        : FacesUtils.checkLong(
                    txtJardinJarId));
            id.setUsuarioUsuId((((txtUsuarioUsuId.getValue()) == null) ||
                (txtUsuarioUsuId.getValue()).equals("")) ? null
                                                         : FacesUtils.checkLong(
                    txtUsuarioUsuId));

            entity.setId(id);
            entity.setJardin(businessDelegatorView.getJardin(
                    entity.getId().getJardinJarId()));
            entity.setUsuario(businessDelegatorView.getUsuario(
                    entity.getId().getUsuarioUsuId()));
            businessDelegatorView.saveGaleria(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYSAVED);
            action_clear();
        } catch (Exception e) {
            entity = null;
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_modify() {
        try {
            if (entity == null) {
                GaleriaId id = new GaleriaId();
                id.setGalId(selectedGaleria.getGalId());
                id.setFechaCreacion(selectedGaleria.getFechaCreacion());
                id.setFechaModificacion(selectedGaleria.getFechaModificacion());
                id.setUsuarioCreador(selectedGaleria.getUsuarioCreador());
                id.setUsuarioModificador(selectedGaleria.getUsuarioModificador());
                id.setEstado(selectedGaleria.getEstado());
                id.setDescripcion(selectedGaleria.getDescripcion());
                id.setUrlFotoGaleria(selectedGaleria.getUrlFotoGaleria());
                id.setJardinJarId(selectedGaleria.getJardinJarId());
                id.setUsuarioUsuId(selectedGaleria.getUsuarioUsuId());
                entity = businessDelegatorView.getGaleria(id);
            }

            businessDelegatorView.updateGaleria(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYMODIFIED);
        } catch (Exception e) {
            data = null;
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_delete_datatable(ActionEvent evt) {
        try {
            selectedGaleria = (GaleriaDTO) (evt.getComponent().getAttributes()
                                               .get("selectedGaleria"));

            GaleriaId id = new GaleriaId();
            id.setGalId(selectedGaleria.getGalId());
            id.setFechaCreacion(selectedGaleria.getFechaCreacion());
            id.setFechaModificacion(selectedGaleria.getFechaModificacion());
            id.setUsuarioCreador(selectedGaleria.getUsuarioCreador());
            id.setUsuarioModificador(selectedGaleria.getUsuarioModificador());
            id.setEstado(selectedGaleria.getEstado());
            id.setDescripcion(selectedGaleria.getDescripcion());
            id.setUrlFotoGaleria(selectedGaleria.getUrlFotoGaleria());
            id.setJardinJarId(selectedGaleria.getJardinJarId());
            id.setUsuarioUsuId(selectedGaleria.getUsuarioUsuId());
            entity = businessDelegatorView.getGaleria(id);
            action_delete();
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_delete_master() {
        try {
            GaleriaId id = new GaleriaId();
            id.setGalId((((txtGalId.getValue()) == null) ||
                (txtGalId.getValue()).equals("")) ? null
                                                  : FacesUtils.checkLong(
                    txtGalId));
            id.setFechaCreacion((((txtFechaCreacion.getValue()) == null) ||
                (txtFechaCreacion.getValue()).equals("")) ? null
                                                          : FacesUtils.checkDate(
                    txtFechaCreacion.getValue()));
            id.setFechaModificacion((((txtFechaModificacion.getValue()) == null) ||
                (txtFechaModificacion.getValue()).equals("")) ? null
                                                              : FacesUtils.checkDate(
                    txtFechaModificacion.getValue()));
            id.setUsuarioCreador((((txtUsuarioCreador.getValue()) == null) ||
                (txtUsuarioCreador.getValue()).equals("")) ? null
                                                           : FacesUtils.checkString(
                    txtUsuarioCreador));
            id.setUsuarioModificador((((txtUsuarioModificador.getValue()) == null) ||
                (txtUsuarioModificador.getValue()).equals("")) ? null
                                                               : FacesUtils.checkString(
                    txtUsuarioModificador));
            id.setEstado((((txtEstado.getValue()) == null) ||
                (txtEstado.getValue()).equals("")) ? null
                                                   : FacesUtils.checkString(
                    txtEstado));
            id.setDescripcion((((txtDescripcion.getValue()) == null) ||
                (txtDescripcion.getValue()).equals("")) ? null
                                                        : FacesUtils.checkString(
                    txtDescripcion));
            id.setUrlFotoGaleria((((txtUrlFotoGaleria.getValue()) == null) ||
                (txtUrlFotoGaleria.getValue()).equals("")) ? null
                                                           : FacesUtils.checkString(
                    txtUrlFotoGaleria));
            id.setJardinJarId((((txtJardinJarId.getValue()) == null) ||
                (txtJardinJarId.getValue()).equals("")) ? null
                                                        : FacesUtils.checkLong(
                    txtJardinJarId));
            id.setUsuarioUsuId((((txtUsuarioUsuId.getValue()) == null) ||
                (txtUsuarioUsuId.getValue()).equals("")) ? null
                                                         : FacesUtils.checkLong(
                    txtUsuarioUsuId));
            entity = businessDelegatorView.getGaleria(id);
            action_delete();
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public void action_delete() throws Exception {
        try {
            businessDelegatorView.deleteGaleria(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYDELETED);
            action_clear();
            data = null;
        } catch (Exception e) {
            throw e;
        }
    }

    public String action_closeDialog() {
        setShowDialog(false);
        action_clear();

        return "";
    }

    public String action_modifyWitDTO(Long galId, Date fechaCreacion,
        Date fechaModificacion, String usuarioCreador,
        String usuarioModificador, String estado, String descripcion,
        String urlFotoGaleria, Long jardinJarId, Long usuarioUsuId,
        Long jarId_Jardin, Long usuId_Usuario) throws Exception {
        try {
            businessDelegatorView.updateGaleria(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYMODIFIED);
        } catch (Exception e) {
            //renderManager.getOnDemandRenderer("GaleriaView").requestRender();
            FacesUtils.addErrorMessage(e.getMessage());
            throw e;
        }

        return "";
    }

    public InputText getTxtJarId_Jardin() {
        return txtJarId_Jardin;
    }

    public void setTxtJarId_Jardin(InputText txtJarId_Jardin) {
        this.txtJarId_Jardin = txtJarId_Jardin;
    }

    public InputText getTxtUsuId_Usuario() {
        return txtUsuId_Usuario;
    }

    public void setTxtUsuId_Usuario(InputText txtUsuId_Usuario) {
        this.txtUsuId_Usuario = txtUsuId_Usuario;
    }

    public InputText getTxtGalId() {
        return txtGalId;
    }

    public void setTxtGalId(InputText txtGalId) {
        this.txtGalId = txtGalId;
    }

    public InputText getTxtUsuarioCreador() {
        return txtUsuarioCreador;
    }

    public void setTxtUsuarioCreador(InputText txtUsuarioCreador) {
        this.txtUsuarioCreador = txtUsuarioCreador;
    }

    public InputText getTxtUsuarioModificador() {
        return txtUsuarioModificador;
    }

    public void setTxtUsuarioModificador(InputText txtUsuarioModificador) {
        this.txtUsuarioModificador = txtUsuarioModificador;
    }

    public InputText getTxtEstado() {
        return txtEstado;
    }

    public void setTxtEstado(InputText txtEstado) {
        this.txtEstado = txtEstado;
    }

    public InputText getTxtDescripcion() {
        return txtDescripcion;
    }

    public void setTxtDescripcion(InputText txtDescripcion) {
        this.txtDescripcion = txtDescripcion;
    }

    public InputText getTxtUrlFotoGaleria() {
        return txtUrlFotoGaleria;
    }

    public void setTxtUrlFotoGaleria(InputText txtUrlFotoGaleria) {
        this.txtUrlFotoGaleria = txtUrlFotoGaleria;
    }

    public InputText getTxtJardinJarId() {
        return txtJardinJarId;
    }

    public void setTxtJardinJarId(InputText txtJardinJarId) {
        this.txtJardinJarId = txtJardinJarId;
    }

    public InputText getTxtUsuarioUsuId() {
        return txtUsuarioUsuId;
    }

    public void setTxtUsuarioUsuId(InputText txtUsuarioUsuId) {
        this.txtUsuarioUsuId = txtUsuarioUsuId;
    }

    public Calendar getTxtFechaCreacion() {
        return txtFechaCreacion;
    }

    public void setTxtFechaCreacion(Calendar txtFechaCreacion) {
        this.txtFechaCreacion = txtFechaCreacion;
    }

    public Calendar getTxtFechaModificacion() {
        return txtFechaModificacion;
    }

    public void setTxtFechaModificacion(Calendar txtFechaModificacion) {
        this.txtFechaModificacion = txtFechaModificacion;
    }

    public List<GaleriaDTO> getData() {
        try {
            if (data == null) {
                data = businessDelegatorView.getDataGaleria();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public void setData(List<GaleriaDTO> galeriaDTO) {
        this.data = galeriaDTO;
    }

    public GaleriaDTO getSelectedGaleria() {
        return selectedGaleria;
    }

    public void setSelectedGaleria(GaleriaDTO galeria) {
        this.selectedGaleria = galeria;
    }

    public CommandButton getBtnSave() {
        return btnSave;
    }

    public void setBtnSave(CommandButton btnSave) {
        this.btnSave = btnSave;
    }

    public CommandButton getBtnModify() {
        return btnModify;
    }

    public void setBtnModify(CommandButton btnModify) {
        this.btnModify = btnModify;
    }

    public CommandButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(CommandButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public CommandButton getBtnClear() {
        return btnClear;
    }

    public void setBtnClear(CommandButton btnClear) {
        this.btnClear = btnClear;
    }

    public TimeZone getTimeZone() {
        return java.util.TimeZone.getDefault();
    }

    public IBusinessDelegatorView getBusinessDelegatorView() {
        return businessDelegatorView;
    }

    public void setBusinessDelegatorView(
        IBusinessDelegatorView businessDelegatorView) {
        this.businessDelegatorView = businessDelegatorView;
    }

    public boolean isShowDialog() {
        return showDialog;
    }

    public void setShowDialog(boolean showDialog) {
        this.showDialog = showDialog;
    }
}

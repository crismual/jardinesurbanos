package co.edu.usbcali.jardines.presentation.backingBeans;

import co.edu.usbcali.jardines.exceptions.*;
import co.edu.usbcali.jardines.model.*;
import co.edu.usbcali.jardines.model.dto.ResenaDTO;
import co.edu.usbcali.jardines.presentation.businessDelegate.*;
import co.edu.usbcali.jardines.utilities.*;

import org.primefaces.component.calendar.*;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.inputtext.InputText;

import org.primefaces.event.RowEditEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

import java.sql.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;


/**
 * @author Zathura Code Generator http://zathuracode.org
 * www.zathuracode.org
 *
 */
@ManagedBean
@ViewScoped
public class ResenaView implements Serializable {
    private static final long serialVersionUID = 1L;
    private static final Logger log = LoggerFactory.getLogger(ResenaView.class);
    private InputText txtDescripcion;
    private InputText txtEstado;
    private InputText txtEstadoJardin;
    private InputText txtUsuarioCreador;
    private InputText txtUsuarioModificador;
    private InputText txtJarId_Jardin;
    private InputText txtUsuId_Usuario;
    private InputText txtResId;
    private Calendar txtFechaCreacion;
    private Calendar txtFechaModificacion;
    private CommandButton btnSave;
    private CommandButton btnModify;
    private CommandButton btnDelete;
    private CommandButton btnClear;
    private List<ResenaDTO> data;
    private ResenaDTO selectedResena;
    private Resena entity;
    private boolean showDialog;
    @ManagedProperty(value = "#{BusinessDelegatorView}")
    private IBusinessDelegatorView businessDelegatorView;

    public ResenaView() {
        super();
    }

    public String action_new() {
        action_clear();
        selectedResena = null;
        setShowDialog(true);

        return "";
    }

    public String action_clear() {
        entity = null;
        selectedResena = null;

        if (txtDescripcion != null) {
            txtDescripcion.setValue(null);
            txtDescripcion.setDisabled(true);
        }

        if (txtEstado != null) {
            txtEstado.setValue(null);
            txtEstado.setDisabled(true);
        }

        if (txtEstadoJardin != null) {
            txtEstadoJardin.setValue(null);
            txtEstadoJardin.setDisabled(true);
        }

        if (txtUsuarioCreador != null) {
            txtUsuarioCreador.setValue(null);
            txtUsuarioCreador.setDisabled(true);
        }

        if (txtUsuarioModificador != null) {
            txtUsuarioModificador.setValue(null);
            txtUsuarioModificador.setDisabled(true);
        }

        if (txtJarId_Jardin != null) {
            txtJarId_Jardin.setValue(null);
            txtJarId_Jardin.setDisabled(true);
        }

        if (txtUsuId_Usuario != null) {
            txtUsuId_Usuario.setValue(null);
            txtUsuId_Usuario.setDisabled(true);
        }

        if (txtFechaCreacion != null) {
            txtFechaCreacion.setValue(null);
            txtFechaCreacion.setDisabled(true);
        }

        if (txtFechaModificacion != null) {
            txtFechaModificacion.setValue(null);
            txtFechaModificacion.setDisabled(true);
        }

        if (txtResId != null) {
            txtResId.setValue(null);
            txtResId.setDisabled(false);
        }

        if (btnSave != null) {
            btnSave.setDisabled(true);
        }

        if (btnDelete != null) {
            btnDelete.setDisabled(true);
        }

        return "";
    }

    public void listener_txtFechaCreacion() {
        Date inputDate = (Date) txtFechaCreacion.getValue();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        FacesContext.getCurrentInstance()
                    .addMessage("",
            new FacesMessage("Selected Date " + dateFormat.format(inputDate)));
    }

    public void listener_txtFechaModificacion() {
        Date inputDate = (Date) txtFechaModificacion.getValue();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        FacesContext.getCurrentInstance()
                    .addMessage("",
            new FacesMessage("Selected Date " + dateFormat.format(inputDate)));
    }

    public void listener_txtId() {
        try {
            Long resId = FacesUtils.checkLong(txtResId);
            entity = (resId != null) ? businessDelegatorView.getResena(resId)
                                     : null;
        } catch (Exception e) {
            entity = null;
        }

        if (entity == null) {
            txtDescripcion.setDisabled(false);
            txtEstado.setDisabled(false);
            txtEstadoJardin.setDisabled(false);
            txtUsuarioCreador.setDisabled(false);
            txtUsuarioModificador.setDisabled(false);
            txtJarId_Jardin.setDisabled(false);
            txtUsuId_Usuario.setDisabled(false);
            txtFechaCreacion.setDisabled(false);
            txtFechaModificacion.setDisabled(false);
            txtResId.setDisabled(false);
            btnSave.setDisabled(false);
        } else {
            txtDescripcion.setValue(entity.getDescripcion());
            txtDescripcion.setDisabled(false);
            txtEstado.setValue(entity.getEstado());
            txtEstado.setDisabled(false);
            txtEstadoJardin.setValue(entity.getEstadoJardin());
            txtEstadoJardin.setDisabled(false);
            txtFechaCreacion.setValue(entity.getFechaCreacion());
            txtFechaCreacion.setDisabled(false);
            txtFechaModificacion.setValue(entity.getFechaModificacion());
            txtFechaModificacion.setDisabled(false);
            txtUsuarioCreador.setValue(entity.getUsuarioCreador());
            txtUsuarioCreador.setDisabled(false);
            txtUsuarioModificador.setValue(entity.getUsuarioModificador());
            txtUsuarioModificador.setDisabled(false);
            txtJarId_Jardin.setValue(entity.getJardin().getJarId());
            txtJarId_Jardin.setDisabled(false);
            txtUsuId_Usuario.setValue(entity.getUsuario().getUsuId());
            txtUsuId_Usuario.setDisabled(false);
            txtResId.setValue(entity.getResId());
            txtResId.setDisabled(true);
            btnSave.setDisabled(false);

            if (btnDelete != null) {
                btnDelete.setDisabled(false);
            }
        }
    }

    public String action_edit(ActionEvent evt) {
        selectedResena = (ResenaDTO) (evt.getComponent().getAttributes()
                                         .get("selectedResena"));
        txtDescripcion.setValue(selectedResena.getDescripcion());
        txtDescripcion.setDisabled(false);
        txtEstado.setValue(selectedResena.getEstado());
        txtEstado.setDisabled(false);
        txtEstadoJardin.setValue(selectedResena.getEstadoJardin());
        txtEstadoJardin.setDisabled(false);
        txtFechaCreacion.setValue(selectedResena.getFechaCreacion());
        txtFechaCreacion.setDisabled(false);
        txtFechaModificacion.setValue(selectedResena.getFechaModificacion());
        txtFechaModificacion.setDisabled(false);
        txtUsuarioCreador.setValue(selectedResena.getUsuarioCreador());
        txtUsuarioCreador.setDisabled(false);
        txtUsuarioModificador.setValue(selectedResena.getUsuarioModificador());
        txtUsuarioModificador.setDisabled(false);
        txtJarId_Jardin.setValue(selectedResena.getJarId_Jardin());
        txtJarId_Jardin.setDisabled(false);
        txtUsuId_Usuario.setValue(selectedResena.getUsuId_Usuario());
        txtUsuId_Usuario.setDisabled(false);
        txtResId.setValue(selectedResena.getResId());
        txtResId.setDisabled(true);
        btnSave.setDisabled(false);
        setShowDialog(true);

        return "";
    }

    public String action_save() {
        try {
            if ((selectedResena == null) && (entity == null)) {
                action_create();
            } else {
                action_modify();
            }

            data = null;
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_create() {
        try {
            entity = new Resena();

            Long resId = FacesUtils.checkLong(txtResId);

            entity.setDescripcion(FacesUtils.checkString(txtDescripcion));
            entity.setEstado(FacesUtils.checkString(txtEstado));
            entity.setEstadoJardin(FacesUtils.checkString(txtEstadoJardin));
            entity.setFechaCreacion(FacesUtils.checkDate(txtFechaCreacion));
            entity.setFechaModificacion(FacesUtils.checkDate(
                    txtFechaModificacion));
            entity.setResId(resId);
            entity.setUsuarioCreador(FacesUtils.checkString(txtUsuarioCreador));
            entity.setUsuarioModificador(FacesUtils.checkString(
                    txtUsuarioModificador));
            entity.setJardin((FacesUtils.checkLong(txtJarId_Jardin) != null)
                ? businessDelegatorView.getJardin(FacesUtils.checkLong(
                        txtJarId_Jardin)) : null);
            entity.setUsuario((FacesUtils.checkLong(txtUsuId_Usuario) != null)
                ? businessDelegatorView.getUsuario(FacesUtils.checkLong(
                        txtUsuId_Usuario)) : null);
            businessDelegatorView.saveResena(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYSAVED);
            action_clear();
        } catch (Exception e) {
            entity = null;
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_modify() {
        try {
            if (entity == null) {
                Long resId = new Long(selectedResena.getResId());
                entity = businessDelegatorView.getResena(resId);
            }

            entity.setDescripcion(FacesUtils.checkString(txtDescripcion));
            entity.setEstado(FacesUtils.checkString(txtEstado));
            entity.setEstadoJardin(FacesUtils.checkString(txtEstadoJardin));
            entity.setFechaCreacion(FacesUtils.checkDate(txtFechaCreacion));
            entity.setFechaModificacion(FacesUtils.checkDate(
                    txtFechaModificacion));
            entity.setUsuarioCreador(FacesUtils.checkString(txtUsuarioCreador));
            entity.setUsuarioModificador(FacesUtils.checkString(
                    txtUsuarioModificador));
            entity.setJardin((FacesUtils.checkLong(txtJarId_Jardin) != null)
                ? businessDelegatorView.getJardin(FacesUtils.checkLong(
                        txtJarId_Jardin)) : null);
            entity.setUsuario((FacesUtils.checkLong(txtUsuId_Usuario) != null)
                ? businessDelegatorView.getUsuario(FacesUtils.checkLong(
                        txtUsuId_Usuario)) : null);
            businessDelegatorView.updateResena(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYMODIFIED);
        } catch (Exception e) {
            data = null;
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_delete_datatable(ActionEvent evt) {
        try {
            selectedResena = (ResenaDTO) (evt.getComponent().getAttributes()
                                             .get("selectedResena"));

            Long resId = new Long(selectedResena.getResId());
            entity = businessDelegatorView.getResena(resId);
            action_delete();
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_delete_master() {
        try {
            Long resId = FacesUtils.checkLong(txtResId);
            entity = businessDelegatorView.getResena(resId);
            action_delete();
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public void action_delete() throws Exception {
        try {
            businessDelegatorView.deleteResena(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYDELETED);
            action_clear();
            data = null;
        } catch (Exception e) {
            throw e;
        }
    }

    public String action_closeDialog() {
        setShowDialog(false);
        action_clear();

        return "";
    }

    public String action_modifyWitDTO(String descripcion, String estado,
        String estadoJardin, Date fechaCreacion, Date fechaModificacion,
        Long resId, String usuarioCreador, String usuarioModificador,
        Long jarId_Jardin, Long usuId_Usuario) throws Exception {
        try {
            entity.setDescripcion(FacesUtils.checkString(descripcion));
            entity.setEstado(FacesUtils.checkString(estado));
            entity.setEstadoJardin(FacesUtils.checkString(estadoJardin));
            entity.setFechaCreacion(FacesUtils.checkDate(fechaCreacion));
            entity.setFechaModificacion(FacesUtils.checkDate(fechaModificacion));
            entity.setUsuarioCreador(FacesUtils.checkString(usuarioCreador));
            entity.setUsuarioModificador(FacesUtils.checkString(
                    usuarioModificador));
            businessDelegatorView.updateResena(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYMODIFIED);
        } catch (Exception e) {
            //renderManager.getOnDemandRenderer("ResenaView").requestRender();
            FacesUtils.addErrorMessage(e.getMessage());
            throw e;
        }

        return "";
    }

    public InputText getTxtDescripcion() {
        return txtDescripcion;
    }

    public void setTxtDescripcion(InputText txtDescripcion) {
        this.txtDescripcion = txtDescripcion;
    }

    public InputText getTxtEstado() {
        return txtEstado;
    }

    public void setTxtEstado(InputText txtEstado) {
        this.txtEstado = txtEstado;
    }

    public InputText getTxtEstadoJardin() {
        return txtEstadoJardin;
    }

    public void setTxtEstadoJardin(InputText txtEstadoJardin) {
        this.txtEstadoJardin = txtEstadoJardin;
    }

    public InputText getTxtUsuarioCreador() {
        return txtUsuarioCreador;
    }

    public void setTxtUsuarioCreador(InputText txtUsuarioCreador) {
        this.txtUsuarioCreador = txtUsuarioCreador;
    }

    public InputText getTxtUsuarioModificador() {
        return txtUsuarioModificador;
    }

    public void setTxtUsuarioModificador(InputText txtUsuarioModificador) {
        this.txtUsuarioModificador = txtUsuarioModificador;
    }

    public InputText getTxtJarId_Jardin() {
        return txtJarId_Jardin;
    }

    public void setTxtJarId_Jardin(InputText txtJarId_Jardin) {
        this.txtJarId_Jardin = txtJarId_Jardin;
    }

    public InputText getTxtUsuId_Usuario() {
        return txtUsuId_Usuario;
    }

    public void setTxtUsuId_Usuario(InputText txtUsuId_Usuario) {
        this.txtUsuId_Usuario = txtUsuId_Usuario;
    }

    public Calendar getTxtFechaCreacion() {
        return txtFechaCreacion;
    }

    public void setTxtFechaCreacion(Calendar txtFechaCreacion) {
        this.txtFechaCreacion = txtFechaCreacion;
    }

    public Calendar getTxtFechaModificacion() {
        return txtFechaModificacion;
    }

    public void setTxtFechaModificacion(Calendar txtFechaModificacion) {
        this.txtFechaModificacion = txtFechaModificacion;
    }

    public InputText getTxtResId() {
        return txtResId;
    }

    public void setTxtResId(InputText txtResId) {
        this.txtResId = txtResId;
    }

    public List<ResenaDTO> getData() {
        try {
            if (data == null) {
                data = businessDelegatorView.getDataResena();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public void setData(List<ResenaDTO> resenaDTO) {
        this.data = resenaDTO;
    }

    public ResenaDTO getSelectedResena() {
        return selectedResena;
    }

    public void setSelectedResena(ResenaDTO resena) {
        this.selectedResena = resena;
    }

    public CommandButton getBtnSave() {
        return btnSave;
    }

    public void setBtnSave(CommandButton btnSave) {
        this.btnSave = btnSave;
    }

    public CommandButton getBtnModify() {
        return btnModify;
    }

    public void setBtnModify(CommandButton btnModify) {
        this.btnModify = btnModify;
    }

    public CommandButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(CommandButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public CommandButton getBtnClear() {
        return btnClear;
    }

    public void setBtnClear(CommandButton btnClear) {
        this.btnClear = btnClear;
    }

    public TimeZone getTimeZone() {
        return java.util.TimeZone.getDefault();
    }

    public IBusinessDelegatorView getBusinessDelegatorView() {
        return businessDelegatorView;
    }

    public void setBusinessDelegatorView(
        IBusinessDelegatorView businessDelegatorView) {
        this.businessDelegatorView = businessDelegatorView;
    }

    public boolean isShowDialog() {
        return showDialog;
    }

    public void setShowDialog(boolean showDialog) {
        this.showDialog = showDialog;
    }
}

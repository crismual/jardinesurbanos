package co.edu.usbcali.jardines.presentation.backingBeans;

import co.edu.usbcali.jardines.exceptions.*;
import co.edu.usbcali.jardines.model.*;
import co.edu.usbcali.jardines.model.dto.EspecieDTO;
import co.edu.usbcali.jardines.presentation.businessDelegate.*;
import co.edu.usbcali.jardines.utilities.*;

import org.primefaces.component.calendar.*;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.inputtext.InputText;

import org.primefaces.event.RowEditEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

import java.sql.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;


/**
 * @author Zathura Code Generator http://zathuracode.org
 * www.zathuracode.org
 *
 */
@ManagedBean
@ViewScoped
public class EspecieView implements Serializable {
    private static final long serialVersionUID = 1L;
    private static final Logger log = LoggerFactory.getLogger(EspecieView.class);
    private InputText txtAltitud;
    private InputText txtCuidados;
    private InputText txtDescripcion;
    private InputText txtEstado;
    private InputText txtFlor;
    private InputText txtFruto;
    private InputText txtNombreCientifico;
    private InputText txtNombreComun;
    private InputText txtPlagas;
    private InputText txtRaiz;
    private InputText txtRiego;
    private InputText txtSilueta;
    private InputText txtSol;
    private InputText txtSuelo;
    private InputText txtUsuarioCreador;
    private InputText txtUsuarioModificador;
    private InputText txtUsuId_Usuario;
    private InputText txtEspeId;
    private Calendar txtFechaCreacion;
    private Calendar txtFechaModificacion;
    private CommandButton btnSave;
    private CommandButton btnModify;
    private CommandButton btnDelete;
    private CommandButton btnClear;
    private List<EspecieDTO> data;
    private EspecieDTO selectedEspecie;
    private Especie entity;
    private boolean showDialog;
    @ManagedProperty(value = "#{BusinessDelegatorView}")
    private IBusinessDelegatorView businessDelegatorView;

    public EspecieView() {
        super();
    }

    public String action_new() {
        action_clear();
        selectedEspecie = null;
        setShowDialog(true);

        return "";
    }

    public String action_clear() {
        entity = null;
        selectedEspecie = null;

        if (txtAltitud != null) {
            txtAltitud.setValue(null);
            txtAltitud.setDisabled(true);
        }

        if (txtCuidados != null) {
            txtCuidados.setValue(null);
            txtCuidados.setDisabled(true);
        }

        if (txtDescripcion != null) {
            txtDescripcion.setValue(null);
            txtDescripcion.setDisabled(true);
        }

        if (txtEstado != null) {
            txtEstado.setValue(null);
            txtEstado.setDisabled(true);
        }

        if (txtFlor != null) {
            txtFlor.setValue(null);
            txtFlor.setDisabled(true);
        }

        if (txtFruto != null) {
            txtFruto.setValue(null);
            txtFruto.setDisabled(true);
        }

        if (txtNombreCientifico != null) {
            txtNombreCientifico.setValue(null);
            txtNombreCientifico.setDisabled(true);
        }

        if (txtNombreComun != null) {
            txtNombreComun.setValue(null);
            txtNombreComun.setDisabled(true);
        }

        if (txtPlagas != null) {
            txtPlagas.setValue(null);
            txtPlagas.setDisabled(true);
        }

        if (txtRaiz != null) {
            txtRaiz.setValue(null);
            txtRaiz.setDisabled(true);
        }

        if (txtRiego != null) {
            txtRiego.setValue(null);
            txtRiego.setDisabled(true);
        }

        if (txtSilueta != null) {
            txtSilueta.setValue(null);
            txtSilueta.setDisabled(true);
        }

        if (txtSol != null) {
            txtSol.setValue(null);
            txtSol.setDisabled(true);
        }

        if (txtSuelo != null) {
            txtSuelo.setValue(null);
            txtSuelo.setDisabled(true);
        }

        if (txtUsuarioCreador != null) {
            txtUsuarioCreador.setValue(null);
            txtUsuarioCreador.setDisabled(true);
        }

        if (txtUsuarioModificador != null) {
            txtUsuarioModificador.setValue(null);
            txtUsuarioModificador.setDisabled(true);
        }

        if (txtUsuId_Usuario != null) {
            txtUsuId_Usuario.setValue(null);
            txtUsuId_Usuario.setDisabled(true);
        }

        if (txtFechaCreacion != null) {
            txtFechaCreacion.setValue(null);
            txtFechaCreacion.setDisabled(true);
        }

        if (txtFechaModificacion != null) {
            txtFechaModificacion.setValue(null);
            txtFechaModificacion.setDisabled(true);
        }

        if (txtEspeId != null) {
            txtEspeId.setValue(null);
            txtEspeId.setDisabled(false);
        }

        if (btnSave != null) {
            btnSave.setDisabled(true);
        }

        if (btnDelete != null) {
            btnDelete.setDisabled(true);
        }

        return "";
    }

    public void listener_txtFechaCreacion() {
        Date inputDate = (Date) txtFechaCreacion.getValue();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        FacesContext.getCurrentInstance()
                    .addMessage("",
            new FacesMessage("Selected Date " + dateFormat.format(inputDate)));
    }

    public void listener_txtFechaModificacion() {
        Date inputDate = (Date) txtFechaModificacion.getValue();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        FacesContext.getCurrentInstance()
                    .addMessage("",
            new FacesMessage("Selected Date " + dateFormat.format(inputDate)));
    }

    public void listener_txtId() {
        try {
            Long espeId = FacesUtils.checkLong(txtEspeId);
            entity = (espeId != null)
                ? businessDelegatorView.getEspecie(espeId) : null;
        } catch (Exception e) {
            entity = null;
        }

        if (entity == null) {
            txtAltitud.setDisabled(false);
            txtCuidados.setDisabled(false);
            txtDescripcion.setDisabled(false);
            txtEstado.setDisabled(false);
            txtFlor.setDisabled(false);
            txtFruto.setDisabled(false);
            txtNombreCientifico.setDisabled(false);
            txtNombreComun.setDisabled(false);
            txtPlagas.setDisabled(false);
            txtRaiz.setDisabled(false);
            txtRiego.setDisabled(false);
            txtSilueta.setDisabled(false);
            txtSol.setDisabled(false);
            txtSuelo.setDisabled(false);
            txtUsuarioCreador.setDisabled(false);
            txtUsuarioModificador.setDisabled(false);
            txtUsuId_Usuario.setDisabled(false);
            txtFechaCreacion.setDisabled(false);
            txtFechaModificacion.setDisabled(false);
            txtEspeId.setDisabled(false);
            btnSave.setDisabled(false);
        } else {
            txtAltitud.setValue(entity.getAltitud());
            txtAltitud.setDisabled(false);
            txtCuidados.setValue(entity.getCuidados());
            txtCuidados.setDisabled(false);
            txtDescripcion.setValue(entity.getDescripcion());
            txtDescripcion.setDisabled(false);
            txtEstado.setValue(entity.getEstado());
            txtEstado.setDisabled(false);
            txtFechaCreacion.setValue(entity.getFechaCreacion());
            txtFechaCreacion.setDisabled(false);
            txtFechaModificacion.setValue(entity.getFechaModificacion());
            txtFechaModificacion.setDisabled(false);
            txtFlor.setValue(entity.getFlor());
            txtFlor.setDisabled(false);
            txtFruto.setValue(entity.getFruto());
            txtFruto.setDisabled(false);
            txtNombreCientifico.setValue(entity.getNombreCientifico());
            txtNombreCientifico.setDisabled(false);
            txtNombreComun.setValue(entity.getNombreComun());
            txtNombreComun.setDisabled(false);
            txtPlagas.setValue(entity.getPlagas());
            txtPlagas.setDisabled(false);
            txtRaiz.setValue(entity.getRaiz());
            txtRaiz.setDisabled(false);
            txtRiego.setValue(entity.getRiego());
            txtRiego.setDisabled(false);
            txtSilueta.setValue(entity.getSilueta());
            txtSilueta.setDisabled(false);
            txtSol.setValue(entity.getSol());
            txtSol.setDisabled(false);
            txtSuelo.setValue(entity.getSuelo());
            txtSuelo.setDisabled(false);
            txtUsuarioCreador.setValue(entity.getUsuarioCreador());
            txtUsuarioCreador.setDisabled(false);
            txtUsuarioModificador.setValue(entity.getUsuarioModificador());
            txtUsuarioModificador.setDisabled(false);
            txtUsuId_Usuario.setValue(entity.getUsuario().getUsuId());
            txtUsuId_Usuario.setDisabled(false);
            txtEspeId.setValue(entity.getEspeId());
            txtEspeId.setDisabled(true);
            btnSave.setDisabled(false);

            if (btnDelete != null) {
                btnDelete.setDisabled(false);
            }
        }
    }

    public String action_edit(ActionEvent evt) {
        selectedEspecie = (EspecieDTO) (evt.getComponent().getAttributes()
                                           .get("selectedEspecie"));
        txtAltitud.setValue(selectedEspecie.getAltitud());
        txtAltitud.setDisabled(false);
        txtCuidados.setValue(selectedEspecie.getCuidados());
        txtCuidados.setDisabled(false);
        txtDescripcion.setValue(selectedEspecie.getDescripcion());
        txtDescripcion.setDisabled(false);
        txtEstado.setValue(selectedEspecie.getEstado());
        txtEstado.setDisabled(false);
        txtFechaCreacion.setValue(selectedEspecie.getFechaCreacion());
        txtFechaCreacion.setDisabled(false);
        txtFechaModificacion.setValue(selectedEspecie.getFechaModificacion());
        txtFechaModificacion.setDisabled(false);
        txtFlor.setValue(selectedEspecie.getFlor());
        txtFlor.setDisabled(false);
        txtFruto.setValue(selectedEspecie.getFruto());
        txtFruto.setDisabled(false);
        txtNombreCientifico.setValue(selectedEspecie.getNombreCientifico());
        txtNombreCientifico.setDisabled(false);
        txtNombreComun.setValue(selectedEspecie.getNombreComun());
        txtNombreComun.setDisabled(false);
        txtPlagas.setValue(selectedEspecie.getPlagas());
        txtPlagas.setDisabled(false);
        txtRaiz.setValue(selectedEspecie.getRaiz());
        txtRaiz.setDisabled(false);
        txtRiego.setValue(selectedEspecie.getRiego());
        txtRiego.setDisabled(false);
        txtSilueta.setValue(selectedEspecie.getSilueta());
        txtSilueta.setDisabled(false);
        txtSol.setValue(selectedEspecie.getSol());
        txtSol.setDisabled(false);
        txtSuelo.setValue(selectedEspecie.getSuelo());
        txtSuelo.setDisabled(false);
        txtUsuarioCreador.setValue(selectedEspecie.getUsuarioCreador());
        txtUsuarioCreador.setDisabled(false);
        txtUsuarioModificador.setValue(selectedEspecie.getUsuarioModificador());
        txtUsuarioModificador.setDisabled(false);
        txtUsuId_Usuario.setValue(selectedEspecie.getUsuId_Usuario());
        txtUsuId_Usuario.setDisabled(false);
        txtEspeId.setValue(selectedEspecie.getEspeId());
        txtEspeId.setDisabled(true);
        btnSave.setDisabled(false);
        setShowDialog(true);

        return "";
    }

    public String action_save() {
        try {
            if ((selectedEspecie == null) && (entity == null)) {
                action_create();
            } else {
                action_modify();
            }

            data = null;
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_create() {
        try {
            entity = new Especie();

            Long espeId = FacesUtils.checkLong(txtEspeId);

            entity.setAltitud(FacesUtils.checkString(txtAltitud));
            entity.setCuidados(FacesUtils.checkString(txtCuidados));
            entity.setDescripcion(FacesUtils.checkString(txtDescripcion));
            entity.setEspeId(espeId);
            entity.setEstado(FacesUtils.checkString(txtEstado));
            entity.setFechaCreacion(FacesUtils.checkDate(txtFechaCreacion));
            entity.setFechaModificacion(FacesUtils.checkDate(
                    txtFechaModificacion));
            entity.setFlor(FacesUtils.checkString(txtFlor));
            entity.setFruto(FacesUtils.checkString(txtFruto));
            entity.setNombreCientifico(FacesUtils.checkString(
                    txtNombreCientifico));
            entity.setNombreComun(FacesUtils.checkString(txtNombreComun));
            entity.setPlagas(FacesUtils.checkString(txtPlagas));
            entity.setRaiz(FacesUtils.checkString(txtRaiz));
            entity.setRiego(FacesUtils.checkString(txtRiego));
            entity.setSilueta(FacesUtils.checkString(txtSilueta));
            entity.setSol(FacesUtils.checkString(txtSol));
            entity.setSuelo(FacesUtils.checkString(txtSuelo));
            entity.setUsuarioCreador(FacesUtils.checkString(txtUsuarioCreador));
            entity.setUsuarioModificador(FacesUtils.checkString(
                    txtUsuarioModificador));
            entity.setUsuario((FacesUtils.checkLong(txtUsuId_Usuario) != null)
                ? businessDelegatorView.getUsuario(FacesUtils.checkLong(
                        txtUsuId_Usuario)) : null);
            businessDelegatorView.saveEspecie(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYSAVED);
            action_clear();
        } catch (Exception e) {
            entity = null;
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_modify() {
        try {
            if (entity == null) {
                Long espeId = new Long(selectedEspecie.getEspeId());
                entity = businessDelegatorView.getEspecie(espeId);
            }

            entity.setAltitud(FacesUtils.checkString(txtAltitud));
            entity.setCuidados(FacesUtils.checkString(txtCuidados));
            entity.setDescripcion(FacesUtils.checkString(txtDescripcion));
            entity.setEstado(FacesUtils.checkString(txtEstado));
            entity.setFechaCreacion(FacesUtils.checkDate(txtFechaCreacion));
            entity.setFechaModificacion(FacesUtils.checkDate(
                    txtFechaModificacion));
            entity.setFlor(FacesUtils.checkString(txtFlor));
            entity.setFruto(FacesUtils.checkString(txtFruto));
            entity.setNombreCientifico(FacesUtils.checkString(
                    txtNombreCientifico));
            entity.setNombreComun(FacesUtils.checkString(txtNombreComun));
            entity.setPlagas(FacesUtils.checkString(txtPlagas));
            entity.setRaiz(FacesUtils.checkString(txtRaiz));
            entity.setRiego(FacesUtils.checkString(txtRiego));
            entity.setSilueta(FacesUtils.checkString(txtSilueta));
            entity.setSol(FacesUtils.checkString(txtSol));
            entity.setSuelo(FacesUtils.checkString(txtSuelo));
            entity.setUsuarioCreador(FacesUtils.checkString(txtUsuarioCreador));
            entity.setUsuarioModificador(FacesUtils.checkString(
                    txtUsuarioModificador));
            entity.setUsuario((FacesUtils.checkLong(txtUsuId_Usuario) != null)
                ? businessDelegatorView.getUsuario(FacesUtils.checkLong(
                        txtUsuId_Usuario)) : null);
            businessDelegatorView.updateEspecie(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYMODIFIED);
        } catch (Exception e) {
            data = null;
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_delete_datatable(ActionEvent evt) {
        try {
            selectedEspecie = (EspecieDTO) (evt.getComponent().getAttributes()
                                               .get("selectedEspecie"));

            Long espeId = new Long(selectedEspecie.getEspeId());
            entity = businessDelegatorView.getEspecie(espeId);
            action_delete();
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_delete_master() {
        try {
            Long espeId = FacesUtils.checkLong(txtEspeId);
            entity = businessDelegatorView.getEspecie(espeId);
            action_delete();
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public void action_delete() throws Exception {
        try {
            businessDelegatorView.deleteEspecie(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYDELETED);
            action_clear();
            data = null;
        } catch (Exception e) {
            throw e;
        }
    }

    public String action_closeDialog() {
        setShowDialog(false);
        action_clear();

        return "";
    }

    public String action_modifyWitDTO(String altitud, String cuidados,
        String descripcion, Long espeId, String estado, Date fechaCreacion,
        Date fechaModificacion, String flor, String fruto,
        String nombreCientifico, String nombreComun, String plagas,
        String raiz, String riego, String silueta, String sol, String suelo,
        String usuarioCreador, String usuarioModificador, Long usuId_Usuario)
        throws Exception {
        try {
            entity.setAltitud(FacesUtils.checkString(altitud));
            entity.setCuidados(FacesUtils.checkString(cuidados));
            entity.setDescripcion(FacesUtils.checkString(descripcion));
            entity.setEstado(FacesUtils.checkString(estado));
            entity.setFechaCreacion(FacesUtils.checkDate(fechaCreacion));
            entity.setFechaModificacion(FacesUtils.checkDate(fechaModificacion));
            entity.setFlor(FacesUtils.checkString(flor));
            entity.setFruto(FacesUtils.checkString(fruto));
            entity.setNombreCientifico(FacesUtils.checkString(nombreCientifico));
            entity.setNombreComun(FacesUtils.checkString(nombreComun));
            entity.setPlagas(FacesUtils.checkString(plagas));
            entity.setRaiz(FacesUtils.checkString(raiz));
            entity.setRiego(FacesUtils.checkString(riego));
            entity.setSilueta(FacesUtils.checkString(silueta));
            entity.setSol(FacesUtils.checkString(sol));
            entity.setSuelo(FacesUtils.checkString(suelo));
            entity.setUsuarioCreador(FacesUtils.checkString(usuarioCreador));
            entity.setUsuarioModificador(FacesUtils.checkString(
                    usuarioModificador));
            businessDelegatorView.updateEspecie(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYMODIFIED);
        } catch (Exception e) {
            //renderManager.getOnDemandRenderer("EspecieView").requestRender();
            FacesUtils.addErrorMessage(e.getMessage());
            throw e;
        }

        return "";
    }

    public InputText getTxtAltitud() {
        return txtAltitud;
    }

    public void setTxtAltitud(InputText txtAltitud) {
        this.txtAltitud = txtAltitud;
    }

    public InputText getTxtCuidados() {
        return txtCuidados;
    }

    public void setTxtCuidados(InputText txtCuidados) {
        this.txtCuidados = txtCuidados;
    }

    public InputText getTxtDescripcion() {
        return txtDescripcion;
    }

    public void setTxtDescripcion(InputText txtDescripcion) {
        this.txtDescripcion = txtDescripcion;
    }

    public InputText getTxtEstado() {
        return txtEstado;
    }

    public void setTxtEstado(InputText txtEstado) {
        this.txtEstado = txtEstado;
    }

    public InputText getTxtFlor() {
        return txtFlor;
    }

    public void setTxtFlor(InputText txtFlor) {
        this.txtFlor = txtFlor;
    }

    public InputText getTxtFruto() {
        return txtFruto;
    }

    public void setTxtFruto(InputText txtFruto) {
        this.txtFruto = txtFruto;
    }

    public InputText getTxtNombreCientifico() {
        return txtNombreCientifico;
    }

    public void setTxtNombreCientifico(InputText txtNombreCientifico) {
        this.txtNombreCientifico = txtNombreCientifico;
    }

    public InputText getTxtNombreComun() {
        return txtNombreComun;
    }

    public void setTxtNombreComun(InputText txtNombreComun) {
        this.txtNombreComun = txtNombreComun;
    }

    public InputText getTxtPlagas() {
        return txtPlagas;
    }

    public void setTxtPlagas(InputText txtPlagas) {
        this.txtPlagas = txtPlagas;
    }

    public InputText getTxtRaiz() {
        return txtRaiz;
    }

    public void setTxtRaiz(InputText txtRaiz) {
        this.txtRaiz = txtRaiz;
    }

    public InputText getTxtRiego() {
        return txtRiego;
    }

    public void setTxtRiego(InputText txtRiego) {
        this.txtRiego = txtRiego;
    }

    public InputText getTxtSilueta() {
        return txtSilueta;
    }

    public void setTxtSilueta(InputText txtSilueta) {
        this.txtSilueta = txtSilueta;
    }

    public InputText getTxtSol() {
        return txtSol;
    }

    public void setTxtSol(InputText txtSol) {
        this.txtSol = txtSol;
    }

    public InputText getTxtSuelo() {
        return txtSuelo;
    }

    public void setTxtSuelo(InputText txtSuelo) {
        this.txtSuelo = txtSuelo;
    }

    public InputText getTxtUsuarioCreador() {
        return txtUsuarioCreador;
    }

    public void setTxtUsuarioCreador(InputText txtUsuarioCreador) {
        this.txtUsuarioCreador = txtUsuarioCreador;
    }

    public InputText getTxtUsuarioModificador() {
        return txtUsuarioModificador;
    }

    public void setTxtUsuarioModificador(InputText txtUsuarioModificador) {
        this.txtUsuarioModificador = txtUsuarioModificador;
    }

    public InputText getTxtUsuId_Usuario() {
        return txtUsuId_Usuario;
    }

    public void setTxtUsuId_Usuario(InputText txtUsuId_Usuario) {
        this.txtUsuId_Usuario = txtUsuId_Usuario;
    }

    public Calendar getTxtFechaCreacion() {
        return txtFechaCreacion;
    }

    public void setTxtFechaCreacion(Calendar txtFechaCreacion) {
        this.txtFechaCreacion = txtFechaCreacion;
    }

    public Calendar getTxtFechaModificacion() {
        return txtFechaModificacion;
    }

    public void setTxtFechaModificacion(Calendar txtFechaModificacion) {
        this.txtFechaModificacion = txtFechaModificacion;
    }

    public InputText getTxtEspeId() {
        return txtEspeId;
    }

    public void setTxtEspeId(InputText txtEspeId) {
        this.txtEspeId = txtEspeId;
    }

    public List<EspecieDTO> getData() {
        try {
            if (data == null) {
                data = businessDelegatorView.getDataEspecie();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public void setData(List<EspecieDTO> especieDTO) {
        this.data = especieDTO;
    }

    public EspecieDTO getSelectedEspecie() {
        return selectedEspecie;
    }

    public void setSelectedEspecie(EspecieDTO especie) {
        this.selectedEspecie = especie;
    }

    public CommandButton getBtnSave() {
        return btnSave;
    }

    public void setBtnSave(CommandButton btnSave) {
        this.btnSave = btnSave;
    }

    public CommandButton getBtnModify() {
        return btnModify;
    }

    public void setBtnModify(CommandButton btnModify) {
        this.btnModify = btnModify;
    }

    public CommandButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(CommandButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public CommandButton getBtnClear() {
        return btnClear;
    }

    public void setBtnClear(CommandButton btnClear) {
        this.btnClear = btnClear;
    }

    public TimeZone getTimeZone() {
        return java.util.TimeZone.getDefault();
    }

    public IBusinessDelegatorView getBusinessDelegatorView() {
        return businessDelegatorView;
    }

    public void setBusinessDelegatorView(
        IBusinessDelegatorView businessDelegatorView) {
        this.businessDelegatorView = businessDelegatorView;
    }

    public boolean isShowDialog() {
        return showDialog;
    }

    public void setShowDialog(boolean showDialog) {
        this.showDialog = showDialog;
    }
}

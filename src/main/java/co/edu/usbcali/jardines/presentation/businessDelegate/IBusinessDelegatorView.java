package co.edu.usbcali.jardines.presentation.businessDelegate;

import co.edu.usbcali.jardines.model.Adjuntosxresena;
import co.edu.usbcali.jardines.model.Adjuntoxpregunta;
import co.edu.usbcali.jardines.model.Auditoria;
import co.edu.usbcali.jardines.model.Especie;
import co.edu.usbcali.jardines.model.Galeria;
import co.edu.usbcali.jardines.model.GaleriaId;
import co.edu.usbcali.jardines.model.Jardin;
import co.edu.usbcali.jardines.model.JardinEspecie;
import co.edu.usbcali.jardines.model.Pregunta;
import co.edu.usbcali.jardines.model.Resena;
import co.edu.usbcali.jardines.model.Respuesta;
import co.edu.usbcali.jardines.model.Respuestaxusuario;
import co.edu.usbcali.jardines.model.TipoUsuario;
import co.edu.usbcali.jardines.model.Usuario;
import co.edu.usbcali.jardines.model.control.AdjuntosxresenaLogic;
import co.edu.usbcali.jardines.model.control.AdjuntoxpreguntaLogic;
import co.edu.usbcali.jardines.model.control.AuditoriaLogic;
import co.edu.usbcali.jardines.model.control.EspecieLogic;
import co.edu.usbcali.jardines.model.control.GaleriaLogic;
import co.edu.usbcali.jardines.model.control.IAdjuntosxresenaLogic;
import co.edu.usbcali.jardines.model.control.IAdjuntoxpreguntaLogic;
import co.edu.usbcali.jardines.model.control.IAuditoriaLogic;
import co.edu.usbcali.jardines.model.control.IEspecieLogic;
import co.edu.usbcali.jardines.model.control.IGaleriaLogic;
import co.edu.usbcali.jardines.model.control.IJardinEspecieLogic;
import co.edu.usbcali.jardines.model.control.IJardinLogic;
import co.edu.usbcali.jardines.model.control.IPreguntaLogic;
import co.edu.usbcali.jardines.model.control.IResenaLogic;
import co.edu.usbcali.jardines.model.control.IRespuestaLogic;
import co.edu.usbcali.jardines.model.control.IRespuestaxusuarioLogic;
import co.edu.usbcali.jardines.model.control.ITipoUsuarioLogic;
import co.edu.usbcali.jardines.model.control.IUsuarioLogic;
import co.edu.usbcali.jardines.model.control.JardinEspecieLogic;
import co.edu.usbcali.jardines.model.control.JardinLogic;
import co.edu.usbcali.jardines.model.control.PreguntaLogic;
import co.edu.usbcali.jardines.model.control.ResenaLogic;
import co.edu.usbcali.jardines.model.control.RespuestaLogic;
import co.edu.usbcali.jardines.model.control.RespuestaxusuarioLogic;
import co.edu.usbcali.jardines.model.control.TipoUsuarioLogic;
import co.edu.usbcali.jardines.model.control.UsuarioLogic;
import co.edu.usbcali.jardines.model.dto.AdjuntosxresenaDTO;
import co.edu.usbcali.jardines.model.dto.AdjuntoxpreguntaDTO;
import co.edu.usbcali.jardines.model.dto.AuditoriaDTO;
import co.edu.usbcali.jardines.model.dto.EspecieDTO;
import co.edu.usbcali.jardines.model.dto.GaleriaDTO;
import co.edu.usbcali.jardines.model.dto.JardinDTO;
import co.edu.usbcali.jardines.model.dto.JardinEspecieDTO;
import co.edu.usbcali.jardines.model.dto.PreguntaDTO;
import co.edu.usbcali.jardines.model.dto.ResenaDTO;
import co.edu.usbcali.jardines.model.dto.RespuestaDTO;
import co.edu.usbcali.jardines.model.dto.RespuestaxusuarioDTO;
import co.edu.usbcali.jardines.model.dto.TipoUsuarioDTO;
import co.edu.usbcali.jardines.model.dto.UsuarioDTO;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.Scope;

import org.springframework.stereotype.Service;

import java.math.BigDecimal;

import java.util.Date;
import java.util.List;
import java.util.Set;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public interface IBusinessDelegatorView {
    public List<Adjuntosxresena> getAdjuntosxresena() throws Exception;

    public void saveAdjuntosxresena(Adjuntosxresena entity)
        throws Exception;

    public void deleteAdjuntosxresena(Adjuntosxresena entity)
        throws Exception;

    public void updateAdjuntosxresena(Adjuntosxresena entity)
        throws Exception;

    public Adjuntosxresena getAdjuntosxresena(Long adresId)
        throws Exception;

    public List<Adjuntosxresena> findByCriteriaInAdjuntosxresena(
        Object[] variables, Object[] variablesBetween,
        Object[] variablesBetweenDates) throws Exception;

    public List<Adjuntosxresena> findPageAdjuntosxresena(
        String sortColumnName, boolean sortAscending, int startRow,
        int maxResults) throws Exception;

    public Long findTotalNumberAdjuntosxresena() throws Exception;

    public List<AdjuntosxresenaDTO> getDataAdjuntosxresena()
        throws Exception;

    public void validateAdjuntosxresena(Adjuntosxresena adjuntosxresena)
        throws Exception;

    public List<Adjuntoxpregunta> getAdjuntoxpregunta()
        throws Exception;

    public void saveAdjuntoxpregunta(Adjuntoxpregunta entity)
        throws Exception;

    public void deleteAdjuntoxpregunta(Adjuntoxpregunta entity)
        throws Exception;

    public void updateAdjuntoxpregunta(Adjuntoxpregunta entity)
        throws Exception;

    public Adjuntoxpregunta getAdjuntoxpregunta(Long adjpreId)
        throws Exception;

    public List<Adjuntoxpregunta> findByCriteriaInAdjuntoxpregunta(
        Object[] variables, Object[] variablesBetween,
        Object[] variablesBetweenDates) throws Exception;

    public List<Adjuntoxpregunta> findPageAdjuntoxpregunta(
        String sortColumnName, boolean sortAscending, int startRow,
        int maxResults) throws Exception;

    public Long findTotalNumberAdjuntoxpregunta() throws Exception;

    public List<AdjuntoxpreguntaDTO> getDataAdjuntoxpregunta()
        throws Exception;

    public void validateAdjuntoxpregunta(Adjuntoxpregunta adjuntoxpregunta)
        throws Exception;

    public List<Auditoria> getAuditoria() throws Exception;

    public void saveAuditoria(Auditoria entity) throws Exception;

    public void deleteAuditoria(Auditoria entity) throws Exception;

    public void updateAuditoria(Auditoria entity) throws Exception;

    public Auditoria getAuditoria(Long audId) throws Exception;

    public List<Auditoria> findByCriteriaInAuditoria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<Auditoria> findPageAuditoria(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception;

    public Long findTotalNumberAuditoria() throws Exception;

    public List<AuditoriaDTO> getDataAuditoria() throws Exception;

    public void validateAuditoria(Auditoria auditoria)
        throws Exception;

    public List<Especie> getEspecie() throws Exception;

    public void saveEspecie(Especie entity) throws Exception;

    public void deleteEspecie(Especie entity) throws Exception;

    public void updateEspecie(Especie entity) throws Exception;

    public Especie getEspecie(Long espeId) throws Exception;

    public List<Especie> findByCriteriaInEspecie(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<Especie> findPageEspecie(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception;

    public Long findTotalNumberEspecie() throws Exception;

    public List<EspecieDTO> getDataEspecie() throws Exception;

    public void validateEspecie(Especie especie) throws Exception;

    public List<Galeria> getGaleria() throws Exception;

    public void saveGaleria(Galeria entity) throws Exception;

    public void deleteGaleria(Galeria entity) throws Exception;

    public void updateGaleria(Galeria entity) throws Exception;

    public Galeria getGaleria(GaleriaId id) throws Exception;

    public List<Galeria> findByCriteriaInGaleria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<Galeria> findPageGaleria(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception;

    public Long findTotalNumberGaleria() throws Exception;

    public List<GaleriaDTO> getDataGaleria() throws Exception;

    public void validateGaleria(Galeria galeria) throws Exception;

    public List<Jardin> getJardin() throws Exception;

    public void saveJardin(Jardin entity) throws Exception;

    public void deleteJardin(Jardin entity) throws Exception;

    public void updateJardin(Jardin entity) throws Exception;

    public Jardin getJardin(Long jarId) throws Exception;

    public List<Jardin> findByCriteriaInJardin(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<Jardin> findPageJardin(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception;

    public Long findTotalNumberJardin() throws Exception;

    public List<JardinDTO> getDataJardin() throws Exception;

    public void validateJardin(Jardin jardin) throws Exception;

    public List<JardinEspecie> getJardinEspecie() throws Exception;

    public void saveJardinEspecie(JardinEspecie entity)
        throws Exception;

    public void deleteJardinEspecie(JardinEspecie entity)
        throws Exception;

    public void updateJardinEspecie(JardinEspecie entity)
        throws Exception;

    public JardinEspecie getJardinEspecie(Long jarespId)
        throws Exception;

    public List<JardinEspecie> findByCriteriaInJardinEspecie(
        Object[] variables, Object[] variablesBetween,
        Object[] variablesBetweenDates) throws Exception;

    public List<JardinEspecie> findPageJardinEspecie(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception;

    public Long findTotalNumberJardinEspecie() throws Exception;

    public List<JardinEspecieDTO> getDataJardinEspecie()
        throws Exception;

    public void validateJardinEspecie(JardinEspecie jardinEspecie)
        throws Exception;

    public List<Pregunta> getPregunta() throws Exception;

    public void savePregunta(Pregunta entity) throws Exception;

    public void deletePregunta(Pregunta entity) throws Exception;

    public void updatePregunta(Pregunta entity) throws Exception;

    public Pregunta getPregunta(Long preId) throws Exception;

    public List<Pregunta> findByCriteriaInPregunta(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<Pregunta> findPagePregunta(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception;

    public Long findTotalNumberPregunta() throws Exception;

    public List<PreguntaDTO> getDataPregunta() throws Exception;

    public void validatePregunta(Pregunta pregunta) throws Exception;

    public List<Resena> getResena() throws Exception;

    public void saveResena(Resena entity) throws Exception;

    public void deleteResena(Resena entity) throws Exception;

    public void updateResena(Resena entity) throws Exception;

    public Resena getResena(Long resId) throws Exception;

    public List<Resena> findByCriteriaInResena(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<Resena> findPageResena(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception;

    public Long findTotalNumberResena() throws Exception;

    public List<ResenaDTO> getDataResena() throws Exception;

    public void validateResena(Resena resena) throws Exception;

    public List<Respuesta> getRespuesta() throws Exception;

    public void saveRespuesta(Respuesta entity) throws Exception;

    public void deleteRespuesta(Respuesta entity) throws Exception;

    public void updateRespuesta(Respuesta entity) throws Exception;

    public Respuesta getRespuesta(Long respId) throws Exception;

    public List<Respuesta> findByCriteriaInRespuesta(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<Respuesta> findPageRespuesta(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception;

    public Long findTotalNumberRespuesta() throws Exception;

    public List<RespuestaDTO> getDataRespuesta() throws Exception;

    public void validateRespuesta(Respuesta respuesta)
        throws Exception;

    public List<Respuestaxusuario> getRespuestaxusuario()
        throws Exception;

    public void saveRespuestaxusuario(Respuestaxusuario entity)
        throws Exception;

    public void deleteRespuestaxusuario(Respuestaxusuario entity)
        throws Exception;

    public void updateRespuestaxusuario(Respuestaxusuario entity)
        throws Exception;

    public Respuestaxusuario getRespuestaxusuario(Long respusuId)
        throws Exception;

    public List<Respuestaxusuario> findByCriteriaInRespuestaxusuario(
        Object[] variables, Object[] variablesBetween,
        Object[] variablesBetweenDates) throws Exception;

    public List<Respuestaxusuario> findPageRespuestaxusuario(
        String sortColumnName, boolean sortAscending, int startRow,
        int maxResults) throws Exception;

    public Long findTotalNumberRespuestaxusuario() throws Exception;

    public List<RespuestaxusuarioDTO> getDataRespuestaxusuario()
        throws Exception;

    public void validateRespuestaxusuario(Respuestaxusuario respuestaxusuario)
        throws Exception;

    public List<TipoUsuario> getTipoUsuario() throws Exception;

    public void saveTipoUsuario(TipoUsuario entity) throws Exception;

    public void deleteTipoUsuario(TipoUsuario entity) throws Exception;

    public void updateTipoUsuario(TipoUsuario entity) throws Exception;

    public TipoUsuario getTipoUsuario(Long tiusId) throws Exception;

    public List<TipoUsuario> findByCriteriaInTipoUsuario(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<TipoUsuario> findPageTipoUsuario(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception;

    public Long findTotalNumberTipoUsuario() throws Exception;

    public List<TipoUsuarioDTO> getDataTipoUsuario() throws Exception;

    public void validateTipoUsuario(TipoUsuario tipoUsuario)
        throws Exception;

    public List<Usuario> getUsuario() throws Exception;

    public void saveUsuario(Usuario entity) throws Exception;

    public void deleteUsuario(Usuario entity) throws Exception;

    public void updateUsuario(Usuario entity) throws Exception;

    public Usuario getUsuario(Long usuId) throws Exception;

    public List<Usuario> findByCriteriaInUsuario(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<Usuario> findPageUsuario(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception;

    public Long findTotalNumberUsuario() throws Exception;

    public List<UsuarioDTO> getDataUsuario() throws Exception;

    public void validateUsuario(Usuario usuario) throws Exception;
}

package co.edu.usbcali.jardines.presentation.backingBeans;

import co.edu.usbcali.jardines.exceptions.*;
import co.edu.usbcali.jardines.model.*;
import co.edu.usbcali.jardines.model.dto.AdjuntoxpreguntaDTO;
import co.edu.usbcali.jardines.presentation.businessDelegate.*;
import co.edu.usbcali.jardines.utilities.*;

import org.primefaces.component.calendar.*;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.inputtext.InputText;

import org.primefaces.event.RowEditEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

import java.sql.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;


/**
 * @author Zathura Code Generator http://zathuracode.org
 * www.zathuracode.org
 *
 */
@ManagedBean
@ViewScoped
public class AdjuntoxpreguntaView implements Serializable {
    private static final long serialVersionUID = 1L;
    private static final Logger log = LoggerFactory.getLogger(AdjuntoxpreguntaView.class);
    private InputText txtDescripcion;
    private InputText txtEstado;
    private InputText txtUrlFotoPregunta;
    private InputText txtUsuarioCreador;
    private InputText txtUsuarioModificador;
    private InputText txtPreId_Pregunta;
    private InputText txtAdjpreId;
    private Calendar txtFechaCreacion;
    private Calendar txtFechaModificacion;
    private CommandButton btnSave;
    private CommandButton btnModify;
    private CommandButton btnDelete;
    private CommandButton btnClear;
    private List<AdjuntoxpreguntaDTO> data;
    private AdjuntoxpreguntaDTO selectedAdjuntoxpregunta;
    private Adjuntoxpregunta entity;
    private boolean showDialog;
    @ManagedProperty(value = "#{BusinessDelegatorView}")
    private IBusinessDelegatorView businessDelegatorView;

    public AdjuntoxpreguntaView() {
        super();
    }

    public String action_new() {
        action_clear();
        selectedAdjuntoxpregunta = null;
        setShowDialog(true);

        return "";
    }

    public String action_clear() {
        entity = null;
        selectedAdjuntoxpregunta = null;

        if (txtDescripcion != null) {
            txtDescripcion.setValue(null);
            txtDescripcion.setDisabled(true);
        }

        if (txtEstado != null) {
            txtEstado.setValue(null);
            txtEstado.setDisabled(true);
        }

        if (txtUrlFotoPregunta != null) {
            txtUrlFotoPregunta.setValue(null);
            txtUrlFotoPregunta.setDisabled(true);
        }

        if (txtUsuarioCreador != null) {
            txtUsuarioCreador.setValue(null);
            txtUsuarioCreador.setDisabled(true);
        }

        if (txtUsuarioModificador != null) {
            txtUsuarioModificador.setValue(null);
            txtUsuarioModificador.setDisabled(true);
        }

        if (txtPreId_Pregunta != null) {
            txtPreId_Pregunta.setValue(null);
            txtPreId_Pregunta.setDisabled(true);
        }

        if (txtFechaCreacion != null) {
            txtFechaCreacion.setValue(null);
            txtFechaCreacion.setDisabled(true);
        }

        if (txtFechaModificacion != null) {
            txtFechaModificacion.setValue(null);
            txtFechaModificacion.setDisabled(true);
        }

        if (txtAdjpreId != null) {
            txtAdjpreId.setValue(null);
            txtAdjpreId.setDisabled(false);
        }

        if (btnSave != null) {
            btnSave.setDisabled(true);
        }

        if (btnDelete != null) {
            btnDelete.setDisabled(true);
        }

        return "";
    }

    public void listener_txtFechaCreacion() {
        Date inputDate = (Date) txtFechaCreacion.getValue();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        FacesContext.getCurrentInstance()
                    .addMessage("",
            new FacesMessage("Selected Date " + dateFormat.format(inputDate)));
    }

    public void listener_txtFechaModificacion() {
        Date inputDate = (Date) txtFechaModificacion.getValue();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        FacesContext.getCurrentInstance()
                    .addMessage("",
            new FacesMessage("Selected Date " + dateFormat.format(inputDate)));
    }

    public void listener_txtId() {
        try {
            Long adjpreId = FacesUtils.checkLong(txtAdjpreId);
            entity = (adjpreId != null)
                ? businessDelegatorView.getAdjuntoxpregunta(adjpreId) : null;
        } catch (Exception e) {
            entity = null;
        }

        if (entity == null) {
            txtDescripcion.setDisabled(false);
            txtEstado.setDisabled(false);
            txtUrlFotoPregunta.setDisabled(false);
            txtUsuarioCreador.setDisabled(false);
            txtUsuarioModificador.setDisabled(false);
            txtPreId_Pregunta.setDisabled(false);
            txtFechaCreacion.setDisabled(false);
            txtFechaModificacion.setDisabled(false);
            txtAdjpreId.setDisabled(false);
            btnSave.setDisabled(false);
        } else {
            txtDescripcion.setValue(entity.getDescripcion());
            txtDescripcion.setDisabled(false);
            txtEstado.setValue(entity.getEstado());
            txtEstado.setDisabled(false);
            txtFechaCreacion.setValue(entity.getFechaCreacion());
            txtFechaCreacion.setDisabled(false);
            txtFechaModificacion.setValue(entity.getFechaModificacion());
            txtFechaModificacion.setDisabled(false);
            txtUrlFotoPregunta.setValue(entity.getUrlFotoPregunta());
            txtUrlFotoPregunta.setDisabled(false);
            txtUsuarioCreador.setValue(entity.getUsuarioCreador());
            txtUsuarioCreador.setDisabled(false);
            txtUsuarioModificador.setValue(entity.getUsuarioModificador());
            txtUsuarioModificador.setDisabled(false);
            txtPreId_Pregunta.setValue(entity.getPregunta().getPreId());
            txtPreId_Pregunta.setDisabled(false);
            txtAdjpreId.setValue(entity.getAdjpreId());
            txtAdjpreId.setDisabled(true);
            btnSave.setDisabled(false);

            if (btnDelete != null) {
                btnDelete.setDisabled(false);
            }
        }
    }

    public String action_edit(ActionEvent evt) {
        selectedAdjuntoxpregunta = (AdjuntoxpreguntaDTO) (evt.getComponent()
                                                             .getAttributes()
                                                             .get("selectedAdjuntoxpregunta"));
        txtDescripcion.setValue(selectedAdjuntoxpregunta.getDescripcion());
        txtDescripcion.setDisabled(false);
        txtEstado.setValue(selectedAdjuntoxpregunta.getEstado());
        txtEstado.setDisabled(false);
        txtFechaCreacion.setValue(selectedAdjuntoxpregunta.getFechaCreacion());
        txtFechaCreacion.setDisabled(false);
        txtFechaModificacion.setValue(selectedAdjuntoxpregunta.getFechaModificacion());
        txtFechaModificacion.setDisabled(false);
        txtUrlFotoPregunta.setValue(selectedAdjuntoxpregunta.getUrlFotoPregunta());
        txtUrlFotoPregunta.setDisabled(false);
        txtUsuarioCreador.setValue(selectedAdjuntoxpregunta.getUsuarioCreador());
        txtUsuarioCreador.setDisabled(false);
        txtUsuarioModificador.setValue(selectedAdjuntoxpregunta.getUsuarioModificador());
        txtUsuarioModificador.setDisabled(false);
        txtPreId_Pregunta.setValue(selectedAdjuntoxpregunta.getPreId_Pregunta());
        txtPreId_Pregunta.setDisabled(false);
        txtAdjpreId.setValue(selectedAdjuntoxpregunta.getAdjpreId());
        txtAdjpreId.setDisabled(true);
        btnSave.setDisabled(false);
        setShowDialog(true);

        return "";
    }

    public String action_save() {
        try {
            if ((selectedAdjuntoxpregunta == null) && (entity == null)) {
                action_create();
            } else {
                action_modify();
            }

            data = null;
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_create() {
        try {
            entity = new Adjuntoxpregunta();

            Long adjpreId = FacesUtils.checkLong(txtAdjpreId);

            entity.setAdjpreId(adjpreId);
            entity.setDescripcion(FacesUtils.checkString(txtDescripcion));
            entity.setEstado(FacesUtils.checkString(txtEstado));
            entity.setFechaCreacion(FacesUtils.checkDate(txtFechaCreacion));
            entity.setFechaModificacion(FacesUtils.checkDate(
                    txtFechaModificacion));
            entity.setUrlFotoPregunta(FacesUtils.checkString(txtUrlFotoPregunta));
            entity.setUsuarioCreador(FacesUtils.checkString(txtUsuarioCreador));
            entity.setUsuarioModificador(FacesUtils.checkString(
                    txtUsuarioModificador));
            entity.setPregunta((FacesUtils.checkLong(txtPreId_Pregunta) != null)
                ? businessDelegatorView.getPregunta(FacesUtils.checkLong(
                        txtPreId_Pregunta)) : null);
            businessDelegatorView.saveAdjuntoxpregunta(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYSAVED);
            action_clear();
        } catch (Exception e) {
            entity = null;
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_modify() {
        try {
            if (entity == null) {
                Long adjpreId = new Long(selectedAdjuntoxpregunta.getAdjpreId());
                entity = businessDelegatorView.getAdjuntoxpregunta(adjpreId);
            }

            entity.setDescripcion(FacesUtils.checkString(txtDescripcion));
            entity.setEstado(FacesUtils.checkString(txtEstado));
            entity.setFechaCreacion(FacesUtils.checkDate(txtFechaCreacion));
            entity.setFechaModificacion(FacesUtils.checkDate(
                    txtFechaModificacion));
            entity.setUrlFotoPregunta(FacesUtils.checkString(txtUrlFotoPregunta));
            entity.setUsuarioCreador(FacesUtils.checkString(txtUsuarioCreador));
            entity.setUsuarioModificador(FacesUtils.checkString(
                    txtUsuarioModificador));
            entity.setPregunta((FacesUtils.checkLong(txtPreId_Pregunta) != null)
                ? businessDelegatorView.getPregunta(FacesUtils.checkLong(
                        txtPreId_Pregunta)) : null);
            businessDelegatorView.updateAdjuntoxpregunta(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYMODIFIED);
        } catch (Exception e) {
            data = null;
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_delete_datatable(ActionEvent evt) {
        try {
            selectedAdjuntoxpregunta = (AdjuntoxpreguntaDTO) (evt.getComponent()
                                                                 .getAttributes()
                                                                 .get("selectedAdjuntoxpregunta"));

            Long adjpreId = new Long(selectedAdjuntoxpregunta.getAdjpreId());
            entity = businessDelegatorView.getAdjuntoxpregunta(adjpreId);
            action_delete();
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_delete_master() {
        try {
            Long adjpreId = FacesUtils.checkLong(txtAdjpreId);
            entity = businessDelegatorView.getAdjuntoxpregunta(adjpreId);
            action_delete();
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public void action_delete() throws Exception {
        try {
            businessDelegatorView.deleteAdjuntoxpregunta(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYDELETED);
            action_clear();
            data = null;
        } catch (Exception e) {
            throw e;
        }
    }

    public String action_closeDialog() {
        setShowDialog(false);
        action_clear();

        return "";
    }

    public String action_modifyWitDTO(Long adjpreId, String descripcion,
        String estado, Date fechaCreacion, Date fechaModificacion,
        String urlFotoPregunta, String usuarioCreador,
        String usuarioModificador, Long preId_Pregunta)
        throws Exception {
        try {
            entity.setDescripcion(FacesUtils.checkString(descripcion));
            entity.setEstado(FacesUtils.checkString(estado));
            entity.setFechaCreacion(FacesUtils.checkDate(fechaCreacion));
            entity.setFechaModificacion(FacesUtils.checkDate(fechaModificacion));
            entity.setUrlFotoPregunta(FacesUtils.checkString(urlFotoPregunta));
            entity.setUsuarioCreador(FacesUtils.checkString(usuarioCreador));
            entity.setUsuarioModificador(FacesUtils.checkString(
                    usuarioModificador));
            businessDelegatorView.updateAdjuntoxpregunta(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYMODIFIED);
        } catch (Exception e) {
            //renderManager.getOnDemandRenderer("AdjuntoxpreguntaView").requestRender();
            FacesUtils.addErrorMessage(e.getMessage());
            throw e;
        }

        return "";
    }

    public InputText getTxtDescripcion() {
        return txtDescripcion;
    }

    public void setTxtDescripcion(InputText txtDescripcion) {
        this.txtDescripcion = txtDescripcion;
    }

    public InputText getTxtEstado() {
        return txtEstado;
    }

    public void setTxtEstado(InputText txtEstado) {
        this.txtEstado = txtEstado;
    }

    public InputText getTxtUrlFotoPregunta() {
        return txtUrlFotoPregunta;
    }

    public void setTxtUrlFotoPregunta(InputText txtUrlFotoPregunta) {
        this.txtUrlFotoPregunta = txtUrlFotoPregunta;
    }

    public InputText getTxtUsuarioCreador() {
        return txtUsuarioCreador;
    }

    public void setTxtUsuarioCreador(InputText txtUsuarioCreador) {
        this.txtUsuarioCreador = txtUsuarioCreador;
    }

    public InputText getTxtUsuarioModificador() {
        return txtUsuarioModificador;
    }

    public void setTxtUsuarioModificador(InputText txtUsuarioModificador) {
        this.txtUsuarioModificador = txtUsuarioModificador;
    }

    public InputText getTxtPreId_Pregunta() {
        return txtPreId_Pregunta;
    }

    public void setTxtPreId_Pregunta(InputText txtPreId_Pregunta) {
        this.txtPreId_Pregunta = txtPreId_Pregunta;
    }

    public Calendar getTxtFechaCreacion() {
        return txtFechaCreacion;
    }

    public void setTxtFechaCreacion(Calendar txtFechaCreacion) {
        this.txtFechaCreacion = txtFechaCreacion;
    }

    public Calendar getTxtFechaModificacion() {
        return txtFechaModificacion;
    }

    public void setTxtFechaModificacion(Calendar txtFechaModificacion) {
        this.txtFechaModificacion = txtFechaModificacion;
    }

    public InputText getTxtAdjpreId() {
        return txtAdjpreId;
    }

    public void setTxtAdjpreId(InputText txtAdjpreId) {
        this.txtAdjpreId = txtAdjpreId;
    }

    public List<AdjuntoxpreguntaDTO> getData() {
        try {
            if (data == null) {
                data = businessDelegatorView.getDataAdjuntoxpregunta();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public void setData(List<AdjuntoxpreguntaDTO> adjuntoxpreguntaDTO) {
        this.data = adjuntoxpreguntaDTO;
    }

    public AdjuntoxpreguntaDTO getSelectedAdjuntoxpregunta() {
        return selectedAdjuntoxpregunta;
    }

    public void setSelectedAdjuntoxpregunta(
        AdjuntoxpreguntaDTO adjuntoxpregunta) {
        this.selectedAdjuntoxpregunta = adjuntoxpregunta;
    }

    public CommandButton getBtnSave() {
        return btnSave;
    }

    public void setBtnSave(CommandButton btnSave) {
        this.btnSave = btnSave;
    }

    public CommandButton getBtnModify() {
        return btnModify;
    }

    public void setBtnModify(CommandButton btnModify) {
        this.btnModify = btnModify;
    }

    public CommandButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(CommandButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public CommandButton getBtnClear() {
        return btnClear;
    }

    public void setBtnClear(CommandButton btnClear) {
        this.btnClear = btnClear;
    }

    public TimeZone getTimeZone() {
        return java.util.TimeZone.getDefault();
    }

    public IBusinessDelegatorView getBusinessDelegatorView() {
        return businessDelegatorView;
    }

    public void setBusinessDelegatorView(
        IBusinessDelegatorView businessDelegatorView) {
        this.businessDelegatorView = businessDelegatorView;
    }

    public boolean isShowDialog() {
        return showDialog;
    }

    public void setShowDialog(boolean showDialog) {
        this.showDialog = showDialog;
    }
}

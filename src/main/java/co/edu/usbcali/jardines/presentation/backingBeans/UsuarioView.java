package co.edu.usbcali.jardines.presentation.backingBeans;

import co.edu.usbcali.jardines.exceptions.*;
import co.edu.usbcali.jardines.model.*;
import co.edu.usbcali.jardines.model.dto.UsuarioDTO;
import co.edu.usbcali.jardines.presentation.businessDelegate.*;
import co.edu.usbcali.jardines.utilities.*;

import org.primefaces.component.calendar.*;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.inputtext.InputText;

import org.primefaces.event.RowEditEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

import java.sql.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;


/**
 * @author Zathura Code Generator http://zathuracode.org
 * www.zathuracode.org
 *
 */
@ManagedBean
@ViewScoped
public class UsuarioView implements Serializable {
    private static final long serialVersionUID = 1L;
    private static final Logger log = LoggerFactory.getLogger(UsuarioView.class);
    private InputText txtApellidos;
    private InputText txtCelular;
    private InputText txtContrasena;
    private InputText txtCorreo;
    private InputText txtDescripcion;
    private InputText txtEdad;
    private InputText txtEstado;
    private InputText txtNombres;
    private InputText txtSexo;
    private InputText txtUsuarioCreador;
    private InputText txtUsuarioModificador;
    private InputText txtTiusId_TipoUsuario;
    private InputText txtUsuId;
    private Calendar txtFechaCreacion;
    private Calendar txtFechaModificacion;
    private Calendar txtFechaNacimiento;
    private CommandButton btnSave;
    private CommandButton btnModify;
    private CommandButton btnDelete;
    private CommandButton btnClear;
    private List<UsuarioDTO> data;
    private UsuarioDTO selectedUsuario;
    private Usuario entity;
    private boolean showDialog;
    @ManagedProperty(value = "#{BusinessDelegatorView}")
    private IBusinessDelegatorView businessDelegatorView;

    public UsuarioView() {
        super();
    }

    public String action_new() {
        action_clear();
        selectedUsuario = null;
        setShowDialog(true);

        return "";
    }

    public String action_clear() {
        entity = null;
        selectedUsuario = null;

        if (txtApellidos != null) {
            txtApellidos.setValue(null);
            txtApellidos.setDisabled(true);
        }

        if (txtCelular != null) {
            txtCelular.setValue(null);
            txtCelular.setDisabled(true);
        }

        if (txtContrasena != null) {
            txtContrasena.setValue(null);
            txtContrasena.setDisabled(true);
        }

        if (txtCorreo != null) {
            txtCorreo.setValue(null);
            txtCorreo.setDisabled(true);
        }

        if (txtDescripcion != null) {
            txtDescripcion.setValue(null);
            txtDescripcion.setDisabled(true);
        }

        if (txtEdad != null) {
            txtEdad.setValue(null);
            txtEdad.setDisabled(true);
        }

        if (txtEstado != null) {
            txtEstado.setValue(null);
            txtEstado.setDisabled(true);
        }

        if (txtNombres != null) {
            txtNombres.setValue(null);
            txtNombres.setDisabled(true);
        }

        if (txtSexo != null) {
            txtSexo.setValue(null);
            txtSexo.setDisabled(true);
        }

        if (txtUsuarioCreador != null) {
            txtUsuarioCreador.setValue(null);
            txtUsuarioCreador.setDisabled(true);
        }

        if (txtUsuarioModificador != null) {
            txtUsuarioModificador.setValue(null);
            txtUsuarioModificador.setDisabled(true);
        }

        if (txtTiusId_TipoUsuario != null) {
            txtTiusId_TipoUsuario.setValue(null);
            txtTiusId_TipoUsuario.setDisabled(true);
        }

        if (txtFechaCreacion != null) {
            txtFechaCreacion.setValue(null);
            txtFechaCreacion.setDisabled(true);
        }

        if (txtFechaModificacion != null) {
            txtFechaModificacion.setValue(null);
            txtFechaModificacion.setDisabled(true);
        }

        if (txtFechaNacimiento != null) {
            txtFechaNacimiento.setValue(null);
            txtFechaNacimiento.setDisabled(true);
        }

        if (txtUsuId != null) {
            txtUsuId.setValue(null);
            txtUsuId.setDisabled(false);
        }

        if (btnSave != null) {
            btnSave.setDisabled(true);
        }

        if (btnDelete != null) {
            btnDelete.setDisabled(true);
        }

        return "";
    }

    public void listener_txtFechaCreacion() {
        Date inputDate = (Date) txtFechaCreacion.getValue();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        FacesContext.getCurrentInstance()
                    .addMessage("",
            new FacesMessage("Selected Date " + dateFormat.format(inputDate)));
    }

    public void listener_txtFechaModificacion() {
        Date inputDate = (Date) txtFechaModificacion.getValue();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        FacesContext.getCurrentInstance()
                    .addMessage("",
            new FacesMessage("Selected Date " + dateFormat.format(inputDate)));
    }

    public void listener_txtFechaNacimiento() {
        Date inputDate = (Date) txtFechaNacimiento.getValue();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        FacesContext.getCurrentInstance()
                    .addMessage("",
            new FacesMessage("Selected Date " + dateFormat.format(inputDate)));
    }

    public void listener_txtId() {
        try {
            Long usuId = FacesUtils.checkLong(txtUsuId);
            entity = (usuId != null) ? businessDelegatorView.getUsuario(usuId)
                                     : null;
        } catch (Exception e) {
            entity = null;
        }

        if (entity == null) {
            txtApellidos.setDisabled(false);
            txtCelular.setDisabled(false);
            txtContrasena.setDisabled(false);
            txtCorreo.setDisabled(false);
            txtDescripcion.setDisabled(false);
            txtEdad.setDisabled(false);
            txtEstado.setDisabled(false);
            txtNombres.setDisabled(false);
            txtSexo.setDisabled(false);
            txtUsuarioCreador.setDisabled(false);
            txtUsuarioModificador.setDisabled(false);
            txtTiusId_TipoUsuario.setDisabled(false);
            txtFechaCreacion.setDisabled(false);
            txtFechaModificacion.setDisabled(false);
            txtFechaNacimiento.setDisabled(false);
            txtUsuId.setDisabled(false);
            btnSave.setDisabled(false);
        } else {
            txtApellidos.setValue(entity.getApellidos());
            txtApellidos.setDisabled(false);
            txtCelular.setValue(entity.getCelular());
            txtCelular.setDisabled(false);
            txtContrasena.setValue(entity.getContrasena());
            txtContrasena.setDisabled(false);
            txtCorreo.setValue(entity.getCorreo());
            txtCorreo.setDisabled(false);
            txtDescripcion.setValue(entity.getDescripcion());
            txtDescripcion.setDisabled(false);
            txtEdad.setValue(entity.getEdad());
            txtEdad.setDisabled(false);
            txtEstado.setValue(entity.getEstado());
            txtEstado.setDisabled(false);
            txtFechaCreacion.setValue(entity.getFechaCreacion());
            txtFechaCreacion.setDisabled(false);
            txtFechaModificacion.setValue(entity.getFechaModificacion());
            txtFechaModificacion.setDisabled(false);
            txtFechaNacimiento.setValue(entity.getFechaNacimiento());
            txtFechaNacimiento.setDisabled(false);
            txtNombres.setValue(entity.getNombres());
            txtNombres.setDisabled(false);
            txtSexo.setValue(entity.getSexo());
            txtSexo.setDisabled(false);
            txtUsuarioCreador.setValue(entity.getUsuarioCreador());
            txtUsuarioCreador.setDisabled(false);
            txtUsuarioModificador.setValue(entity.getUsuarioModificador());
            txtUsuarioModificador.setDisabled(false);
            txtTiusId_TipoUsuario.setValue(entity.getTipoUsuario().getTiusId());
            txtTiusId_TipoUsuario.setDisabled(false);
            txtUsuId.setValue(entity.getUsuId());
            txtUsuId.setDisabled(true);
            btnSave.setDisabled(false);

            if (btnDelete != null) {
                btnDelete.setDisabled(false);
            }
        }
    }

    public String action_edit(ActionEvent evt) {
        selectedUsuario = (UsuarioDTO) (evt.getComponent().getAttributes()
                                           .get("selectedUsuario"));
        txtApellidos.setValue(selectedUsuario.getApellidos());
        txtApellidos.setDisabled(false);
        txtCelular.setValue(selectedUsuario.getCelular());
        txtCelular.setDisabled(false);
        txtContrasena.setValue(selectedUsuario.getContrasena());
        txtContrasena.setDisabled(false);
        txtCorreo.setValue(selectedUsuario.getCorreo());
        txtCorreo.setDisabled(false);
        txtDescripcion.setValue(selectedUsuario.getDescripcion());
        txtDescripcion.setDisabled(false);
        txtEdad.setValue(selectedUsuario.getEdad());
        txtEdad.setDisabled(false);
        txtEstado.setValue(selectedUsuario.getEstado());
        txtEstado.setDisabled(false);
        txtFechaCreacion.setValue(selectedUsuario.getFechaCreacion());
        txtFechaCreacion.setDisabled(false);
        txtFechaModificacion.setValue(selectedUsuario.getFechaModificacion());
        txtFechaModificacion.setDisabled(false);
        txtFechaNacimiento.setValue(selectedUsuario.getFechaNacimiento());
        txtFechaNacimiento.setDisabled(false);
        txtNombres.setValue(selectedUsuario.getNombres());
        txtNombres.setDisabled(false);
        txtSexo.setValue(selectedUsuario.getSexo());
        txtSexo.setDisabled(false);
        txtUsuarioCreador.setValue(selectedUsuario.getUsuarioCreador());
        txtUsuarioCreador.setDisabled(false);
        txtUsuarioModificador.setValue(selectedUsuario.getUsuarioModificador());
        txtUsuarioModificador.setDisabled(false);
        txtTiusId_TipoUsuario.setValue(selectedUsuario.getTiusId_TipoUsuario());
        txtTiusId_TipoUsuario.setDisabled(false);
        txtUsuId.setValue(selectedUsuario.getUsuId());
        txtUsuId.setDisabled(true);
        btnSave.setDisabled(false);
        setShowDialog(true);

        return "";
    }

    public String action_save() {
        try {
            if ((selectedUsuario == null) && (entity == null)) {
                action_create();
            } else {
                action_modify();
            }

            data = null;
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_create() {
        try {
            entity = new Usuario();

            Long usuId = FacesUtils.checkLong(txtUsuId);

            entity.setApellidos(FacesUtils.checkString(txtApellidos));
            entity.setCelular(FacesUtils.checkString(txtCelular));
            entity.setContrasena(FacesUtils.checkString(txtContrasena));
            entity.setCorreo(FacesUtils.checkString(txtCorreo));
            entity.setDescripcion(FacesUtils.checkString(txtDescripcion));
            entity.setEdad(FacesUtils.checkString(txtEdad));
            entity.setEstado(FacesUtils.checkString(txtEstado));
            entity.setFechaCreacion(FacesUtils.checkDate(txtFechaCreacion));
            entity.setFechaModificacion(FacesUtils.checkDate(
                    txtFechaModificacion));
            entity.setFechaNacimiento(FacesUtils.checkDate(txtFechaNacimiento));
            entity.setNombres(FacesUtils.checkString(txtNombres));
            entity.setSexo(FacesUtils.checkString(txtSexo));
            entity.setUsuId(usuId);
            entity.setUsuarioCreador(FacesUtils.checkString(txtUsuarioCreador));
            entity.setUsuarioModificador(FacesUtils.checkString(
                    txtUsuarioModificador));
            entity.setTipoUsuario((FacesUtils.checkLong(txtTiusId_TipoUsuario) != null)
                ? businessDelegatorView.getTipoUsuario(FacesUtils.checkLong(
                        txtTiusId_TipoUsuario)) : null);
            businessDelegatorView.saveUsuario(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYSAVED);
            action_clear();
        } catch (Exception e) {
            entity = null;
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_modify() {
        try {
            if (entity == null) {
                Long usuId = new Long(selectedUsuario.getUsuId());
                entity = businessDelegatorView.getUsuario(usuId);
            }

            entity.setApellidos(FacesUtils.checkString(txtApellidos));
            entity.setCelular(FacesUtils.checkString(txtCelular));
            entity.setContrasena(FacesUtils.checkString(txtContrasena));
            entity.setCorreo(FacesUtils.checkString(txtCorreo));
            entity.setDescripcion(FacesUtils.checkString(txtDescripcion));
            entity.setEdad(FacesUtils.checkString(txtEdad));
            entity.setEstado(FacesUtils.checkString(txtEstado));
            entity.setFechaCreacion(FacesUtils.checkDate(txtFechaCreacion));
            entity.setFechaModificacion(FacesUtils.checkDate(
                    txtFechaModificacion));
            entity.setFechaNacimiento(FacesUtils.checkDate(txtFechaNacimiento));
            entity.setNombres(FacesUtils.checkString(txtNombres));
            entity.setSexo(FacesUtils.checkString(txtSexo));
            entity.setUsuarioCreador(FacesUtils.checkString(txtUsuarioCreador));
            entity.setUsuarioModificador(FacesUtils.checkString(
                    txtUsuarioModificador));
            entity.setTipoUsuario((FacesUtils.checkLong(txtTiusId_TipoUsuario) != null)
                ? businessDelegatorView.getTipoUsuario(FacesUtils.checkLong(
                        txtTiusId_TipoUsuario)) : null);
            businessDelegatorView.updateUsuario(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYMODIFIED);
        } catch (Exception e) {
            data = null;
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_delete_datatable(ActionEvent evt) {
        try {
            selectedUsuario = (UsuarioDTO) (evt.getComponent().getAttributes()
                                               .get("selectedUsuario"));

            Long usuId = new Long(selectedUsuario.getUsuId());
            entity = businessDelegatorView.getUsuario(usuId);
            action_delete();
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public String action_delete_master() {
        try {
            Long usuId = FacesUtils.checkLong(txtUsuId);
            entity = businessDelegatorView.getUsuario(usuId);
            action_delete();
        } catch (Exception e) {
            FacesUtils.addErrorMessage(e.getMessage());
        }

        return "";
    }

    public void action_delete() throws Exception {
        try {
            businessDelegatorView.deleteUsuario(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYDELETED);
            action_clear();
            data = null;
        } catch (Exception e) {
            throw e;
        }
    }

    public String action_closeDialog() {
        setShowDialog(false);
        action_clear();

        return "";
    }

    public String action_modifyWitDTO(String apellidos, String celular,
        String contrasena, String correo, String descripcion, String edad,
        String estado, Date fechaCreacion, Date fechaModificacion,
        Date fechaNacimiento, String nombres, String sexo, Long usuId,
        String usuarioCreador, String usuarioModificador,
        Long tiusId_TipoUsuario) throws Exception {
        try {
            entity.setApellidos(FacesUtils.checkString(apellidos));
            entity.setCelular(FacesUtils.checkString(celular));
            entity.setContrasena(FacesUtils.checkString(contrasena));
            entity.setCorreo(FacesUtils.checkString(correo));
            entity.setDescripcion(FacesUtils.checkString(descripcion));
            entity.setEdad(FacesUtils.checkString(edad));
            entity.setEstado(FacesUtils.checkString(estado));
            entity.setFechaCreacion(FacesUtils.checkDate(fechaCreacion));
            entity.setFechaModificacion(FacesUtils.checkDate(fechaModificacion));
            entity.setFechaNacimiento(FacesUtils.checkDate(fechaNacimiento));
            entity.setNombres(FacesUtils.checkString(nombres));
            entity.setSexo(FacesUtils.checkString(sexo));
            entity.setUsuarioCreador(FacesUtils.checkString(usuarioCreador));
            entity.setUsuarioModificador(FacesUtils.checkString(
                    usuarioModificador));
            businessDelegatorView.updateUsuario(entity);
            FacesUtils.addInfoMessage(ZMessManager.ENTITY_SUCCESFULLYMODIFIED);
        } catch (Exception e) {
            //renderManager.getOnDemandRenderer("UsuarioView").requestRender();
            FacesUtils.addErrorMessage(e.getMessage());
            throw e;
        }

        return "";
    }

    public InputText getTxtApellidos() {
        return txtApellidos;
    }

    public void setTxtApellidos(InputText txtApellidos) {
        this.txtApellidos = txtApellidos;
    }

    public InputText getTxtCelular() {
        return txtCelular;
    }

    public void setTxtCelular(InputText txtCelular) {
        this.txtCelular = txtCelular;
    }

    public InputText getTxtContrasena() {
        return txtContrasena;
    }

    public void setTxtContrasena(InputText txtContrasena) {
        this.txtContrasena = txtContrasena;
    }

    public InputText getTxtCorreo() {
        return txtCorreo;
    }

    public void setTxtCorreo(InputText txtCorreo) {
        this.txtCorreo = txtCorreo;
    }

    public InputText getTxtDescripcion() {
        return txtDescripcion;
    }

    public void setTxtDescripcion(InputText txtDescripcion) {
        this.txtDescripcion = txtDescripcion;
    }

    public InputText getTxtEdad() {
        return txtEdad;
    }

    public void setTxtEdad(InputText txtEdad) {
        this.txtEdad = txtEdad;
    }

    public InputText getTxtEstado() {
        return txtEstado;
    }

    public void setTxtEstado(InputText txtEstado) {
        this.txtEstado = txtEstado;
    }

    public InputText getTxtNombres() {
        return txtNombres;
    }

    public void setTxtNombres(InputText txtNombres) {
        this.txtNombres = txtNombres;
    }

    public InputText getTxtSexo() {
        return txtSexo;
    }

    public void setTxtSexo(InputText txtSexo) {
        this.txtSexo = txtSexo;
    }

    public InputText getTxtUsuarioCreador() {
        return txtUsuarioCreador;
    }

    public void setTxtUsuarioCreador(InputText txtUsuarioCreador) {
        this.txtUsuarioCreador = txtUsuarioCreador;
    }

    public InputText getTxtUsuarioModificador() {
        return txtUsuarioModificador;
    }

    public void setTxtUsuarioModificador(InputText txtUsuarioModificador) {
        this.txtUsuarioModificador = txtUsuarioModificador;
    }

    public InputText getTxtTiusId_TipoUsuario() {
        return txtTiusId_TipoUsuario;
    }

    public void setTxtTiusId_TipoUsuario(InputText txtTiusId_TipoUsuario) {
        this.txtTiusId_TipoUsuario = txtTiusId_TipoUsuario;
    }

    public Calendar getTxtFechaCreacion() {
        return txtFechaCreacion;
    }

    public void setTxtFechaCreacion(Calendar txtFechaCreacion) {
        this.txtFechaCreacion = txtFechaCreacion;
    }

    public Calendar getTxtFechaModificacion() {
        return txtFechaModificacion;
    }

    public void setTxtFechaModificacion(Calendar txtFechaModificacion) {
        this.txtFechaModificacion = txtFechaModificacion;
    }

    public Calendar getTxtFechaNacimiento() {
        return txtFechaNacimiento;
    }

    public void setTxtFechaNacimiento(Calendar txtFechaNacimiento) {
        this.txtFechaNacimiento = txtFechaNacimiento;
    }

    public InputText getTxtUsuId() {
        return txtUsuId;
    }

    public void setTxtUsuId(InputText txtUsuId) {
        this.txtUsuId = txtUsuId;
    }

    public List<UsuarioDTO> getData() {
        try {
            if (data == null) {
                data = businessDelegatorView.getDataUsuario();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    public void setData(List<UsuarioDTO> usuarioDTO) {
        this.data = usuarioDTO;
    }

    public UsuarioDTO getSelectedUsuario() {
        return selectedUsuario;
    }

    public void setSelectedUsuario(UsuarioDTO usuario) {
        this.selectedUsuario = usuario;
    }

    public CommandButton getBtnSave() {
        return btnSave;
    }

    public void setBtnSave(CommandButton btnSave) {
        this.btnSave = btnSave;
    }

    public CommandButton getBtnModify() {
        return btnModify;
    }

    public void setBtnModify(CommandButton btnModify) {
        this.btnModify = btnModify;
    }

    public CommandButton getBtnDelete() {
        return btnDelete;
    }

    public void setBtnDelete(CommandButton btnDelete) {
        this.btnDelete = btnDelete;
    }

    public CommandButton getBtnClear() {
        return btnClear;
    }

    public void setBtnClear(CommandButton btnClear) {
        this.btnClear = btnClear;
    }

    public TimeZone getTimeZone() {
        return java.util.TimeZone.getDefault();
    }

    public IBusinessDelegatorView getBusinessDelegatorView() {
        return businessDelegatorView;
    }

    public void setBusinessDelegatorView(
        IBusinessDelegatorView businessDelegatorView) {
        this.businessDelegatorView = businessDelegatorView;
    }

    public boolean isShowDialog() {
        return showDialog;
    }

    public void setShowDialog(boolean showDialog) {
        this.showDialog = showDialog;
    }
}

package co.edu.usbcali.jardines.dataaccess.dao;

import co.edu.usbcali.jardines.dataaccess.api.Dao;
import co.edu.usbcali.jardines.model.Jardin;

import java.math.BigDecimal;

import java.util.Date;
import java.util.List;
import java.util.Set;


/**
* Interface for   JardinDAO.
*
*/
public interface IJardinDAO extends Dao<Jardin, Long> {
}

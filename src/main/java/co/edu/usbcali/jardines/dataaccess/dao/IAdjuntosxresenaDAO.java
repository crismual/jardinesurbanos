package co.edu.usbcali.jardines.dataaccess.dao;

import co.edu.usbcali.jardines.dataaccess.api.Dao;
import co.edu.usbcali.jardines.model.Adjuntosxresena;

import java.math.BigDecimal;

import java.util.Date;
import java.util.List;
import java.util.Set;


/**
* Interface for   AdjuntosxresenaDAO.
*
*/
public interface IAdjuntosxresenaDAO extends Dao<Adjuntosxresena, Long> {
}

package co.edu.usbcali.jardines.dataaccess.dao;

import co.edu.usbcali.jardines.dataaccess.api.Dao;
import co.edu.usbcali.jardines.model.Galeria;
import co.edu.usbcali.jardines.model.GaleriaId;

import java.math.BigDecimal;

import java.util.Date;
import java.util.List;
import java.util.Set;


/**
* Interface for   GaleriaDAO.
*
*/
public interface IGaleriaDAO extends Dao<Galeria, GaleriaId> {
}

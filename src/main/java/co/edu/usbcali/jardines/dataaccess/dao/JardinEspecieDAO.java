package co.edu.usbcali.jardines.dataaccess.dao;

import co.edu.usbcali.jardines.dataaccess.api.JpaDaoImpl;
import co.edu.usbcali.jardines.model.JardinEspecie;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;

import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


/**
 * A data access object (DAO) providing persistence and search support for
 * JardinEspecie entities. Transaction control of the save(), update() and
 * delete() operations can directly support Spring container-managed
 * transactions or they can be augmented to handle user-managed Spring
 * transactions. Each of these methods provides additional information for how
 * to configure it for the desired type of transaction control.
 *
 * @see lidis.JardinEspecie
 */
@Scope("singleton")
@Repository("JardinEspecieDAO")
public class JardinEspecieDAO extends JpaDaoImpl<JardinEspecie, Long>
    implements IJardinEspecieDAO {
    private static final Logger log = LoggerFactory.getLogger(JardinEspecieDAO.class);
    @PersistenceContext
    private EntityManager entityManager;

    public static IJardinEspecieDAO getFromApplicationContext(
        ApplicationContext ctx) {
        return (IJardinEspecieDAO) ctx.getBean("JardinEspecieDAO");
    }
}

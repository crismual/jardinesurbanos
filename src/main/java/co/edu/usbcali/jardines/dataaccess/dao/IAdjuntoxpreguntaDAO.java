package co.edu.usbcali.jardines.dataaccess.dao;

import co.edu.usbcali.jardines.dataaccess.api.Dao;
import co.edu.usbcali.jardines.model.Adjuntoxpregunta;

import java.math.BigDecimal;

import java.util.Date;
import java.util.List;
import java.util.Set;


/**
* Interface for   AdjuntoxpreguntaDAO.
*
*/
public interface IAdjuntoxpreguntaDAO extends Dao<Adjuntoxpregunta, Long> {
}

package co.edu.usbcali.jardines.dataaccess.dao;

import co.edu.usbcali.jardines.dataaccess.api.Dao;
import co.edu.usbcali.jardines.model.JardinEspecie;

import java.math.BigDecimal;

import java.util.Date;
import java.util.List;
import java.util.Set;


/**
* Interface for   JardinEspecieDAO.
*
*/
public interface IJardinEspecieDAO extends Dao<JardinEspecie, Long> {
}

package co.edu.usbcali.jardines.dataaccess.dao;

import co.edu.usbcali.jardines.dataaccess.api.JpaDaoImpl;
import co.edu.usbcali.jardines.model.Galeria;
import co.edu.usbcali.jardines.model.GaleriaId;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;

import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


/**
 * A data access object (DAO) providing persistence and search support for
 * Galeria entities. Transaction control of the save(), update() and
 * delete() operations can directly support Spring container-managed
 * transactions or they can be augmented to handle user-managed Spring
 * transactions. Each of these methods provides additional information for how
 * to configure it for the desired type of transaction control.
 *
 * @see lidis.Galeria
 */
@Scope("singleton")
@Repository("GaleriaDAO")
public class GaleriaDAO extends JpaDaoImpl<Galeria, GaleriaId>
    implements IGaleriaDAO {
    private static final Logger log = LoggerFactory.getLogger(GaleriaDAO.class);
    @PersistenceContext
    private EntityManager entityManager;

    public static IGaleriaDAO getFromApplicationContext(ApplicationContext ctx) {
        return (IGaleriaDAO) ctx.getBean("GaleriaDAO");
    }
}

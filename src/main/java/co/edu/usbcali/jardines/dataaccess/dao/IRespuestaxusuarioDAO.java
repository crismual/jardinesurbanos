package co.edu.usbcali.jardines.dataaccess.dao;

import co.edu.usbcali.jardines.dataaccess.api.Dao;
import co.edu.usbcali.jardines.model.Respuestaxusuario;

import java.math.BigDecimal;

import java.util.Date;
import java.util.List;
import java.util.Set;


/**
* Interface for   RespuestaxusuarioDAO.
*
*/
public interface IRespuestaxusuarioDAO extends Dao<Respuestaxusuario, Long> {
}

package co.edu.usbcali.jardines.dto.mapper;

import co.edu.usbcali.jardines.model.*;
import co.edu.usbcali.jardines.model.Galeria;
import co.edu.usbcali.jardines.model.control.*;
import co.edu.usbcali.jardines.model.dto.GaleriaDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.Scope;

import org.springframework.stereotype.Component;

import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
@Component
@Scope("singleton")
public class GaleriaMapper implements IGaleriaMapper {
    private static final Logger log = LoggerFactory.getLogger(GaleriaMapper.class);

    /**
    * Logic injected by Spring that manages Jardin entities
    *
    */
    @Autowired
    IJardinLogic logicJardin1;

    /**
    * Logic injected by Spring that manages Usuario entities
    *
    */
    @Autowired
    IUsuarioLogic logicUsuario2;

    @Transactional(readOnly = true)
    public GaleriaDTO galeriaToGaleriaDTO(Galeria galeria)
        throws Exception {
        try {
            GaleriaDTO galeriaDTO = new GaleriaDTO();

            galeriaDTO.setGalId(galeria.getId().getGalId());
            galeriaDTO.setFechaCreacion(galeria.getId().getFechaCreacion());
            galeriaDTO.setFechaModificacion(galeria.getId()
                                                   .getFechaModificacion());
            galeriaDTO.setUsuarioCreador(galeria.getId().getUsuarioCreador());
            galeriaDTO.setUsuarioModificador(galeria.getId()
                                                    .getUsuarioModificador());
            galeriaDTO.setEstado(galeria.getId().getEstado());
            galeriaDTO.setDescripcion(galeria.getId().getDescripcion());
            galeriaDTO.setUrlFotoGaleria(galeria.getId().getUrlFotoGaleria());
            galeriaDTO.setJardinJarId(galeria.getId().getJardinJarId());
            galeriaDTO.setUsuarioUsuId(galeria.getId().getUsuarioUsuId());
            galeriaDTO.setJarId_Jardin((galeria.getJardin().getJarId() != null)
                ? galeria.getJardin().getJarId() : null);
            galeriaDTO.setUsuId_Usuario((galeria.getUsuario().getUsuId() != null)
                ? galeria.getUsuario().getUsuId() : null);

            return galeriaDTO;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public Galeria galeriaDTOToGaleria(GaleriaDTO galeriaDTO)
        throws Exception {
        try {
            Galeria galeria = new Galeria();

            GaleriaId galeriaId = new GaleriaId();

            if ((galeriaDTO.getGalId() != null) &&
                    (galeriaDTO.getGalId().toString().length() <= 0)) {
                throw new Exception("La llave no puede ser nula");
            }

            galeriaId.setGalId((galeriaDTO.getGalId() != null)
                ? galeriaDTO.getGalId() : null);

            if ((galeriaDTO.getFechaCreacion() != null) &&
                    (galeriaDTO.getFechaCreacion().toString().length() <= 0)) {
                throw new Exception("La llave no puede ser nula");
            }

            galeriaId.setFechaCreacion((galeriaDTO.getFechaCreacion() != null)
                ? galeriaDTO.getFechaCreacion() : null);

            if ((galeriaDTO.getFechaModificacion() != null) &&
                    (galeriaDTO.getFechaModificacion().toString().length() <= 0)) {
                throw new Exception("La llave no puede ser nula");
            }

            galeriaId.setFechaModificacion((galeriaDTO.getFechaModificacion() != null)
                ? galeriaDTO.getFechaModificacion() : null);

            if ((galeriaDTO.getUsuarioCreador() != null) &&
                    (galeriaDTO.getUsuarioCreador().toString().length() <= 0)) {
                throw new Exception("La llave no puede ser nula");
            }

            galeriaId.setUsuarioCreador((galeriaDTO.getUsuarioCreador() != null)
                ? galeriaDTO.getUsuarioCreador() : null);

            if ((galeriaDTO.getUsuarioModificador() != null) &&
                    (galeriaDTO.getUsuarioModificador().toString().length() <= 0)) {
                throw new Exception("La llave no puede ser nula");
            }

            galeriaId.setUsuarioModificador((galeriaDTO.getUsuarioModificador() != null)
                ? galeriaDTO.getUsuarioModificador() : null);

            if ((galeriaDTO.getEstado() != null) &&
                    (galeriaDTO.getEstado().toString().length() <= 0)) {
                throw new Exception("La llave no puede ser nula");
            }

            galeriaId.setEstado((galeriaDTO.getEstado() != null)
                ? galeriaDTO.getEstado() : null);

            if ((galeriaDTO.getDescripcion() != null) &&
                    (galeriaDTO.getDescripcion().toString().length() <= 0)) {
                throw new Exception("La llave no puede ser nula");
            }

            galeriaId.setDescripcion((galeriaDTO.getDescripcion() != null)
                ? galeriaDTO.getDescripcion() : null);

            if ((galeriaDTO.getUrlFotoGaleria() != null) &&
                    (galeriaDTO.getUrlFotoGaleria().toString().length() <= 0)) {
                throw new Exception("La llave no puede ser nula");
            }

            galeriaId.setUrlFotoGaleria((galeriaDTO.getUrlFotoGaleria() != null)
                ? galeriaDTO.getUrlFotoGaleria() : null);

            if ((galeriaDTO.getJardinJarId() != null) &&
                    (galeriaDTO.getJardinJarId().toString().length() <= 0)) {
                throw new Exception("La llave no puede ser nula");
            }

            galeriaId.setJardinJarId((galeriaDTO.getJardinJarId() != null)
                ? galeriaDTO.getJardinJarId() : null);

            if ((galeriaDTO.getUsuarioUsuId() != null) &&
                    (galeriaDTO.getUsuarioUsuId().toString().length() <= 0)) {
                throw new Exception("La llave no puede ser nula");
            }

            galeriaId.setUsuarioUsuId((galeriaDTO.getUsuarioUsuId() != null)
                ? galeriaDTO.getUsuarioUsuId() : null);
            galeria.setId(galeriaId);

            Jardin jardin = logicJardin1.getJardin(galeriaDTO.getJarId_Jardin());

            if (jardin != null) {
                galeria.setJardin(jardin);
            }

            Usuario usuario = logicUsuario2.getUsuario(galeriaDTO.getUsuId_Usuario());

            if (usuario != null) {
                galeria.setUsuario(usuario);
            }

            return galeria;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public List<GaleriaDTO> listGaleriaToListGaleriaDTO(
        List<Galeria> listGaleria) throws Exception {
        try {
            List<GaleriaDTO> galeriaDTOs = new ArrayList<GaleriaDTO>();

            for (Galeria galeria : listGaleria) {
                GaleriaDTO galeriaDTO = galeriaToGaleriaDTO(galeria);

                galeriaDTOs.add(galeriaDTO);
            }

            return galeriaDTOs;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public List<Galeria> listGaleriaDTOToListGaleria(
        List<GaleriaDTO> listGaleriaDTO) throws Exception {
        try {
            List<Galeria> listGaleria = new ArrayList<Galeria>();

            for (GaleriaDTO galeriaDTO : listGaleriaDTO) {
                Galeria galeria = galeriaDTOToGaleria(galeriaDTO);

                listGaleria.add(galeria);
            }

            return listGaleria;
        } catch (Exception e) {
            throw e;
        }
    }
}

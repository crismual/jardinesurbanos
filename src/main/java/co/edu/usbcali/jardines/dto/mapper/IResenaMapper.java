package co.edu.usbcali.jardines.dto.mapper;

import co.edu.usbcali.jardines.model.Resena;
import co.edu.usbcali.jardines.model.dto.ResenaDTO;

import java.util.List;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public interface IResenaMapper {
    public ResenaDTO resenaToResenaDTO(Resena resena) throws Exception;

    public Resena resenaDTOToResena(ResenaDTO resenaDTO)
        throws Exception;

    public List<ResenaDTO> listResenaToListResenaDTO(List<Resena> resenas)
        throws Exception;

    public List<Resena> listResenaDTOToListResena(List<ResenaDTO> resenaDTOs)
        throws Exception;
}

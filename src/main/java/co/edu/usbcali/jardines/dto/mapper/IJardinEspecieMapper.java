package co.edu.usbcali.jardines.dto.mapper;

import co.edu.usbcali.jardines.model.JardinEspecie;
import co.edu.usbcali.jardines.model.dto.JardinEspecieDTO;

import java.util.List;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public interface IJardinEspecieMapper {
    public JardinEspecieDTO jardinEspecieToJardinEspecieDTO(
        JardinEspecie jardinEspecie) throws Exception;

    public JardinEspecie jardinEspecieDTOToJardinEspecie(
        JardinEspecieDTO jardinEspecieDTO) throws Exception;

    public List<JardinEspecieDTO> listJardinEspecieToListJardinEspecieDTO(
        List<JardinEspecie> jardinEspecies) throws Exception;

    public List<JardinEspecie> listJardinEspecieDTOToListJardinEspecie(
        List<JardinEspecieDTO> jardinEspecieDTOs) throws Exception;
}

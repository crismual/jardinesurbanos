package co.edu.usbcali.jardines.dto.mapper;

import co.edu.usbcali.jardines.model.*;
import co.edu.usbcali.jardines.model.Usuario;
import co.edu.usbcali.jardines.model.control.*;
import co.edu.usbcali.jardines.model.dto.UsuarioDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.Scope;

import org.springframework.stereotype.Component;

import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
@Component
@Scope("singleton")
public class UsuarioMapper implements IUsuarioMapper {
    private static final Logger log = LoggerFactory.getLogger(UsuarioMapper.class);

    /**
    * Logic injected by Spring that manages TipoUsuario entities
    *
    */
    @Autowired
    ITipoUsuarioLogic logicTipoUsuario1;

    @Transactional(readOnly = true)
    public UsuarioDTO usuarioToUsuarioDTO(Usuario usuario)
        throws Exception {
        try {
            UsuarioDTO usuarioDTO = new UsuarioDTO();

            usuarioDTO.setUsuId(usuario.getUsuId());
            usuarioDTO.setApellidos((usuario.getApellidos() != null)
                ? usuario.getApellidos() : null);
            usuarioDTO.setCelular((usuario.getCelular() != null)
                ? usuario.getCelular() : null);
            usuarioDTO.setContrasena((usuario.getContrasena() != null)
                ? usuario.getContrasena() : null);
            usuarioDTO.setCorreo((usuario.getCorreo() != null)
                ? usuario.getCorreo() : null);
            usuarioDTO.setDescripcion((usuario.getDescripcion() != null)
                ? usuario.getDescripcion() : null);
            usuarioDTO.setEdad((usuario.getEdad() != null) ? usuario.getEdad()
                                                           : null);
            usuarioDTO.setEstado((usuario.getEstado() != null)
                ? usuario.getEstado() : null);
            usuarioDTO.setFechaCreacion(usuario.getFechaCreacion());
            usuarioDTO.setFechaModificacion(usuario.getFechaModificacion());
            usuarioDTO.setFechaNacimiento(usuario.getFechaNacimiento());
            usuarioDTO.setNombres((usuario.getNombres() != null)
                ? usuario.getNombres() : null);
            usuarioDTO.setSexo((usuario.getSexo() != null) ? usuario.getSexo()
                                                           : null);
            usuarioDTO.setUsuarioCreador((usuario.getUsuarioCreador() != null)
                ? usuario.getUsuarioCreador() : null);
            usuarioDTO.setUsuarioModificador((usuario.getUsuarioModificador() != null)
                ? usuario.getUsuarioModificador() : null);
            usuarioDTO.setTiusId_TipoUsuario((usuario.getTipoUsuario()
                                                     .getTiusId() != null)
                ? usuario.getTipoUsuario().getTiusId() : null);

            return usuarioDTO;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public Usuario usuarioDTOToUsuario(UsuarioDTO usuarioDTO)
        throws Exception {
        try {
            Usuario usuario = new Usuario();

            usuario.setUsuId(usuarioDTO.getUsuId());
            usuario.setApellidos((usuarioDTO.getApellidos() != null)
                ? usuarioDTO.getApellidos() : null);
            usuario.setCelular((usuarioDTO.getCelular() != null)
                ? usuarioDTO.getCelular() : null);
            usuario.setContrasena((usuarioDTO.getContrasena() != null)
                ? usuarioDTO.getContrasena() : null);
            usuario.setCorreo((usuarioDTO.getCorreo() != null)
                ? usuarioDTO.getCorreo() : null);
            usuario.setDescripcion((usuarioDTO.getDescripcion() != null)
                ? usuarioDTO.getDescripcion() : null);
            usuario.setEdad((usuarioDTO.getEdad() != null)
                ? usuarioDTO.getEdad() : null);
            usuario.setEstado((usuarioDTO.getEstado() != null)
                ? usuarioDTO.getEstado() : null);
            usuario.setFechaCreacion(usuarioDTO.getFechaCreacion());
            usuario.setFechaModificacion(usuarioDTO.getFechaModificacion());
            usuario.setFechaNacimiento(usuarioDTO.getFechaNacimiento());
            usuario.setNombres((usuarioDTO.getNombres() != null)
                ? usuarioDTO.getNombres() : null);
            usuario.setSexo((usuarioDTO.getSexo() != null)
                ? usuarioDTO.getSexo() : null);
            usuario.setUsuarioCreador((usuarioDTO.getUsuarioCreador() != null)
                ? usuarioDTO.getUsuarioCreador() : null);
            usuario.setUsuarioModificador((usuarioDTO.getUsuarioModificador() != null)
                ? usuarioDTO.getUsuarioModificador() : null);

            TipoUsuario tipoUsuario = new TipoUsuario();

            if (usuarioDTO.getTiusId_TipoUsuario() != null) {
                tipoUsuario = logicTipoUsuario1.getTipoUsuario(usuarioDTO.getTiusId_TipoUsuario());
            }

            if (tipoUsuario != null) {
                usuario.setTipoUsuario(tipoUsuario);
            }

            return usuario;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public List<UsuarioDTO> listUsuarioToListUsuarioDTO(
        List<Usuario> listUsuario) throws Exception {
        try {
            List<UsuarioDTO> usuarioDTOs = new ArrayList<UsuarioDTO>();

            for (Usuario usuario : listUsuario) {
                UsuarioDTO usuarioDTO = usuarioToUsuarioDTO(usuario);

                usuarioDTOs.add(usuarioDTO);
            }

            return usuarioDTOs;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public List<Usuario> listUsuarioDTOToListUsuario(
        List<UsuarioDTO> listUsuarioDTO) throws Exception {
        try {
            List<Usuario> listUsuario = new ArrayList<Usuario>();

            for (UsuarioDTO usuarioDTO : listUsuarioDTO) {
                Usuario usuario = usuarioDTOToUsuario(usuarioDTO);

                listUsuario.add(usuario);
            }

            return listUsuario;
        } catch (Exception e) {
            throw e;
        }
    }
}

package co.edu.usbcali.jardines.dto.mapper;

import co.edu.usbcali.jardines.model.Especie;
import co.edu.usbcali.jardines.model.dto.EspecieDTO;

import java.util.List;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public interface IEspecieMapper {
    public EspecieDTO especieToEspecieDTO(Especie especie)
        throws Exception;

    public Especie especieDTOToEspecie(EspecieDTO especieDTO)
        throws Exception;

    public List<EspecieDTO> listEspecieToListEspecieDTO(List<Especie> especies)
        throws Exception;

    public List<Especie> listEspecieDTOToListEspecie(
        List<EspecieDTO> especieDTOs) throws Exception;
}

package co.edu.usbcali.jardines.dto.mapper;

import co.edu.usbcali.jardines.model.*;
import co.edu.usbcali.jardines.model.Adjuntosxresena;
import co.edu.usbcali.jardines.model.control.*;
import co.edu.usbcali.jardines.model.dto.AdjuntosxresenaDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.Scope;

import org.springframework.stereotype.Component;

import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
@Component
@Scope("singleton")
public class AdjuntosxresenaMapper implements IAdjuntosxresenaMapper {
    private static final Logger log = LoggerFactory.getLogger(AdjuntosxresenaMapper.class);

    /**
    * Logic injected by Spring that manages Resena entities
    *
    */
    @Autowired
    IResenaLogic logicResena1;

    @Transactional(readOnly = true)
    public AdjuntosxresenaDTO adjuntosxresenaToAdjuntosxresenaDTO(
        Adjuntosxresena adjuntosxresena) throws Exception {
        try {
            AdjuntosxresenaDTO adjuntosxresenaDTO = new AdjuntosxresenaDTO();

            adjuntosxresenaDTO.setAdresId(adjuntosxresena.getAdresId());
            adjuntosxresenaDTO.setDescripcion((adjuntosxresena.getDescripcion() != null)
                ? adjuntosxresena.getDescripcion() : null);
            adjuntosxresenaDTO.setEstado((adjuntosxresena.getEstado() != null)
                ? adjuntosxresena.getEstado() : null);
            adjuntosxresenaDTO.setFechaCreacion(adjuntosxresena.getFechaCreacion());
            adjuntosxresenaDTO.setFechaModificacion(adjuntosxresena.getFechaModificacion());
            adjuntosxresenaDTO.setUrlFotoRes((adjuntosxresena.getUrlFotoRes() != null)
                ? adjuntosxresena.getUrlFotoRes() : null);
            adjuntosxresenaDTO.setUsuarioCreador((adjuntosxresena.getUsuarioCreador() != null)
                ? adjuntosxresena.getUsuarioCreador() : null);
            adjuntosxresenaDTO.setUsuarioModificador((adjuntosxresena.getUsuarioModificador() != null)
                ? adjuntosxresena.getUsuarioModificador() : null);
            adjuntosxresenaDTO.setResId_Resena((adjuntosxresena.getResena()
                                                               .getResId() != null)
                ? adjuntosxresena.getResena().getResId() : null);

            return adjuntosxresenaDTO;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public Adjuntosxresena adjuntosxresenaDTOToAdjuntosxresena(
        AdjuntosxresenaDTO adjuntosxresenaDTO) throws Exception {
        try {
            Adjuntosxresena adjuntosxresena = new Adjuntosxresena();

            adjuntosxresena.setAdresId(adjuntosxresenaDTO.getAdresId());
            adjuntosxresena.setDescripcion((adjuntosxresenaDTO.getDescripcion() != null)
                ? adjuntosxresenaDTO.getDescripcion() : null);
            adjuntosxresena.setEstado((adjuntosxresenaDTO.getEstado() != null)
                ? adjuntosxresenaDTO.getEstado() : null);
            adjuntosxresena.setFechaCreacion(adjuntosxresenaDTO.getFechaCreacion());
            adjuntosxresena.setFechaModificacion(adjuntosxresenaDTO.getFechaModificacion());
            adjuntosxresena.setUrlFotoRes((adjuntosxresenaDTO.getUrlFotoRes() != null)
                ? adjuntosxresenaDTO.getUrlFotoRes() : null);
            adjuntosxresena.setUsuarioCreador((adjuntosxresenaDTO.getUsuarioCreador() != null)
                ? adjuntosxresenaDTO.getUsuarioCreador() : null);
            adjuntosxresena.setUsuarioModificador((adjuntosxresenaDTO.getUsuarioModificador() != null)
                ? adjuntosxresenaDTO.getUsuarioModificador() : null);

            Resena resena = new Resena();

            if (adjuntosxresenaDTO.getResId_Resena() != null) {
                resena = logicResena1.getResena(adjuntosxresenaDTO.getResId_Resena());
            }

            if (resena != null) {
                adjuntosxresena.setResena(resena);
            }

            return adjuntosxresena;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public List<AdjuntosxresenaDTO> listAdjuntosxresenaToListAdjuntosxresenaDTO(
        List<Adjuntosxresena> listAdjuntosxresena) throws Exception {
        try {
            List<AdjuntosxresenaDTO> adjuntosxresenaDTOs = new ArrayList<AdjuntosxresenaDTO>();

            for (Adjuntosxresena adjuntosxresena : listAdjuntosxresena) {
                AdjuntosxresenaDTO adjuntosxresenaDTO = adjuntosxresenaToAdjuntosxresenaDTO(adjuntosxresena);

                adjuntosxresenaDTOs.add(adjuntosxresenaDTO);
            }

            return adjuntosxresenaDTOs;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public List<Adjuntosxresena> listAdjuntosxresenaDTOToListAdjuntosxresena(
        List<AdjuntosxresenaDTO> listAdjuntosxresenaDTO)
        throws Exception {
        try {
            List<Adjuntosxresena> listAdjuntosxresena = new ArrayList<Adjuntosxresena>();

            for (AdjuntosxresenaDTO adjuntosxresenaDTO : listAdjuntosxresenaDTO) {
                Adjuntosxresena adjuntosxresena = adjuntosxresenaDTOToAdjuntosxresena(adjuntosxresenaDTO);

                listAdjuntosxresena.add(adjuntosxresena);
            }

            return listAdjuntosxresena;
        } catch (Exception e) {
            throw e;
        }
    }
}

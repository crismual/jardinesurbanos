package co.edu.usbcali.jardines.dto.mapper;

import co.edu.usbcali.jardines.model.Adjuntosxresena;
import co.edu.usbcali.jardines.model.dto.AdjuntosxresenaDTO;

import java.util.List;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public interface IAdjuntosxresenaMapper {
    public AdjuntosxresenaDTO adjuntosxresenaToAdjuntosxresenaDTO(
        Adjuntosxresena adjuntosxresena) throws Exception;

    public Adjuntosxresena adjuntosxresenaDTOToAdjuntosxresena(
        AdjuntosxresenaDTO adjuntosxresenaDTO) throws Exception;

    public List<AdjuntosxresenaDTO> listAdjuntosxresenaToListAdjuntosxresenaDTO(
        List<Adjuntosxresena> adjuntosxresenas) throws Exception;

    public List<Adjuntosxresena> listAdjuntosxresenaDTOToListAdjuntosxresena(
        List<AdjuntosxresenaDTO> adjuntosxresenaDTOs) throws Exception;
}

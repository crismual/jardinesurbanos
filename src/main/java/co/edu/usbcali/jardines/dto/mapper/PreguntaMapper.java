package co.edu.usbcali.jardines.dto.mapper;

import co.edu.usbcali.jardines.model.*;
import co.edu.usbcali.jardines.model.Pregunta;
import co.edu.usbcali.jardines.model.control.*;
import co.edu.usbcali.jardines.model.dto.PreguntaDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.Scope;

import org.springframework.stereotype.Component;

import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
@Component
@Scope("singleton")
public class PreguntaMapper implements IPreguntaMapper {
    private static final Logger log = LoggerFactory.getLogger(PreguntaMapper.class);

    @Transactional(readOnly = true)
    public PreguntaDTO preguntaToPreguntaDTO(Pregunta pregunta)
        throws Exception {
        try {
            PreguntaDTO preguntaDTO = new PreguntaDTO();

            preguntaDTO.setPreId(pregunta.getPreId());
            preguntaDTO.setDescripcion((pregunta.getDescripcion() != null)
                ? pregunta.getDescripcion() : null);
            preguntaDTO.setDescripcionPregunta((pregunta.getDescripcionPregunta() != null)
                ? pregunta.getDescripcionPregunta() : null);
            preguntaDTO.setEstado((pregunta.getEstado() != null)
                ? pregunta.getEstado() : null);
            preguntaDTO.setFechaCreacion(pregunta.getFechaCreacion());
            preguntaDTO.setFechaModificacion(pregunta.getFechaModificacion());
            preguntaDTO.setUsuarioCreador((pregunta.getUsuarioCreador() != null)
                ? pregunta.getUsuarioCreador() : null);
            preguntaDTO.setUsuarioModificador((pregunta.getUsuarioModificador() != null)
                ? pregunta.getUsuarioModificador() : null);

            return preguntaDTO;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public Pregunta preguntaDTOToPregunta(PreguntaDTO preguntaDTO)
        throws Exception {
        try {
            Pregunta pregunta = new Pregunta();

            pregunta.setPreId(preguntaDTO.getPreId());
            pregunta.setDescripcion((preguntaDTO.getDescripcion() != null)
                ? preguntaDTO.getDescripcion() : null);
            pregunta.setDescripcionPregunta((preguntaDTO.getDescripcionPregunta() != null)
                ? preguntaDTO.getDescripcionPregunta() : null);
            pregunta.setEstado((preguntaDTO.getEstado() != null)
                ? preguntaDTO.getEstado() : null);
            pregunta.setFechaCreacion(preguntaDTO.getFechaCreacion());
            pregunta.setFechaModificacion(preguntaDTO.getFechaModificacion());
            pregunta.setUsuarioCreador((preguntaDTO.getUsuarioCreador() != null)
                ? preguntaDTO.getUsuarioCreador() : null);
            pregunta.setUsuarioModificador((preguntaDTO.getUsuarioModificador() != null)
                ? preguntaDTO.getUsuarioModificador() : null);

            return pregunta;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public List<PreguntaDTO> listPreguntaToListPreguntaDTO(
        List<Pregunta> listPregunta) throws Exception {
        try {
            List<PreguntaDTO> preguntaDTOs = new ArrayList<PreguntaDTO>();

            for (Pregunta pregunta : listPregunta) {
                PreguntaDTO preguntaDTO = preguntaToPreguntaDTO(pregunta);

                preguntaDTOs.add(preguntaDTO);
            }

            return preguntaDTOs;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public List<Pregunta> listPreguntaDTOToListPregunta(
        List<PreguntaDTO> listPreguntaDTO) throws Exception {
        try {
            List<Pregunta> listPregunta = new ArrayList<Pregunta>();

            for (PreguntaDTO preguntaDTO : listPreguntaDTO) {
                Pregunta pregunta = preguntaDTOToPregunta(preguntaDTO);

                listPregunta.add(pregunta);
            }

            return listPregunta;
        } catch (Exception e) {
            throw e;
        }
    }
}

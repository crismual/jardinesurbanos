package co.edu.usbcali.jardines.dto.mapper;

import co.edu.usbcali.jardines.model.*;
import co.edu.usbcali.jardines.model.Adjuntoxpregunta;
import co.edu.usbcali.jardines.model.control.*;
import co.edu.usbcali.jardines.model.dto.AdjuntoxpreguntaDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.Scope;

import org.springframework.stereotype.Component;

import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
@Component
@Scope("singleton")
public class AdjuntoxpreguntaMapper implements IAdjuntoxpreguntaMapper {
    private static final Logger log = LoggerFactory.getLogger(AdjuntoxpreguntaMapper.class);

    /**
    * Logic injected by Spring that manages Pregunta entities
    *
    */
    @Autowired
    IPreguntaLogic logicPregunta1;

    @Transactional(readOnly = true)
    public AdjuntoxpreguntaDTO adjuntoxpreguntaToAdjuntoxpreguntaDTO(
        Adjuntoxpregunta adjuntoxpregunta) throws Exception {
        try {
            AdjuntoxpreguntaDTO adjuntoxpreguntaDTO = new AdjuntoxpreguntaDTO();

            adjuntoxpreguntaDTO.setAdjpreId(adjuntoxpregunta.getAdjpreId());
            adjuntoxpreguntaDTO.setDescripcion((adjuntoxpregunta.getDescripcion() != null)
                ? adjuntoxpregunta.getDescripcion() : null);
            adjuntoxpreguntaDTO.setEstado((adjuntoxpregunta.getEstado() != null)
                ? adjuntoxpregunta.getEstado() : null);
            adjuntoxpreguntaDTO.setFechaCreacion(adjuntoxpregunta.getFechaCreacion());
            adjuntoxpreguntaDTO.setFechaModificacion(adjuntoxpregunta.getFechaModificacion());
            adjuntoxpreguntaDTO.setUrlFotoPregunta((adjuntoxpregunta.getUrlFotoPregunta() != null)
                ? adjuntoxpregunta.getUrlFotoPregunta() : null);
            adjuntoxpreguntaDTO.setUsuarioCreador((adjuntoxpregunta.getUsuarioCreador() != null)
                ? adjuntoxpregunta.getUsuarioCreador() : null);
            adjuntoxpreguntaDTO.setUsuarioModificador((adjuntoxpregunta.getUsuarioModificador() != null)
                ? adjuntoxpregunta.getUsuarioModificador() : null);
            adjuntoxpreguntaDTO.setPreId_Pregunta((adjuntoxpregunta.getPregunta()
                                                                   .getPreId() != null)
                ? adjuntoxpregunta.getPregunta().getPreId() : null);

            return adjuntoxpreguntaDTO;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public Adjuntoxpregunta adjuntoxpreguntaDTOToAdjuntoxpregunta(
        AdjuntoxpreguntaDTO adjuntoxpreguntaDTO) throws Exception {
        try {
            Adjuntoxpregunta adjuntoxpregunta = new Adjuntoxpregunta();

            adjuntoxpregunta.setAdjpreId(adjuntoxpreguntaDTO.getAdjpreId());
            adjuntoxpregunta.setDescripcion((adjuntoxpreguntaDTO.getDescripcion() != null)
                ? adjuntoxpreguntaDTO.getDescripcion() : null);
            adjuntoxpregunta.setEstado((adjuntoxpreguntaDTO.getEstado() != null)
                ? adjuntoxpreguntaDTO.getEstado() : null);
            adjuntoxpregunta.setFechaCreacion(adjuntoxpreguntaDTO.getFechaCreacion());
            adjuntoxpregunta.setFechaModificacion(adjuntoxpreguntaDTO.getFechaModificacion());
            adjuntoxpregunta.setUrlFotoPregunta((adjuntoxpreguntaDTO.getUrlFotoPregunta() != null)
                ? adjuntoxpreguntaDTO.getUrlFotoPregunta() : null);
            adjuntoxpregunta.setUsuarioCreador((adjuntoxpreguntaDTO.getUsuarioCreador() != null)
                ? adjuntoxpreguntaDTO.getUsuarioCreador() : null);
            adjuntoxpregunta.setUsuarioModificador((adjuntoxpreguntaDTO.getUsuarioModificador() != null)
                ? adjuntoxpreguntaDTO.getUsuarioModificador() : null);

            Pregunta pregunta = new Pregunta();

            if (adjuntoxpreguntaDTO.getPreId_Pregunta() != null) {
                pregunta = logicPregunta1.getPregunta(adjuntoxpreguntaDTO.getPreId_Pregunta());
            }

            if (pregunta != null) {
                adjuntoxpregunta.setPregunta(pregunta);
            }

            return adjuntoxpregunta;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public List<AdjuntoxpreguntaDTO> listAdjuntoxpreguntaToListAdjuntoxpreguntaDTO(
        List<Adjuntoxpregunta> listAdjuntoxpregunta) throws Exception {
        try {
            List<AdjuntoxpreguntaDTO> adjuntoxpreguntaDTOs = new ArrayList<AdjuntoxpreguntaDTO>();

            for (Adjuntoxpregunta adjuntoxpregunta : listAdjuntoxpregunta) {
                AdjuntoxpreguntaDTO adjuntoxpreguntaDTO = adjuntoxpreguntaToAdjuntoxpreguntaDTO(adjuntoxpregunta);

                adjuntoxpreguntaDTOs.add(adjuntoxpreguntaDTO);
            }

            return adjuntoxpreguntaDTOs;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public List<Adjuntoxpregunta> listAdjuntoxpreguntaDTOToListAdjuntoxpregunta(
        List<AdjuntoxpreguntaDTO> listAdjuntoxpreguntaDTO)
        throws Exception {
        try {
            List<Adjuntoxpregunta> listAdjuntoxpregunta = new ArrayList<Adjuntoxpregunta>();

            for (AdjuntoxpreguntaDTO adjuntoxpreguntaDTO : listAdjuntoxpreguntaDTO) {
                Adjuntoxpregunta adjuntoxpregunta = adjuntoxpreguntaDTOToAdjuntoxpregunta(adjuntoxpreguntaDTO);

                listAdjuntoxpregunta.add(adjuntoxpregunta);
            }

            return listAdjuntoxpregunta;
        } catch (Exception e) {
            throw e;
        }
    }
}

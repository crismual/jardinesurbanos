package co.edu.usbcali.jardines.dto.mapper;

import co.edu.usbcali.jardines.model.Adjuntoxpregunta;
import co.edu.usbcali.jardines.model.dto.AdjuntoxpreguntaDTO;

import java.util.List;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public interface IAdjuntoxpreguntaMapper {
    public AdjuntoxpreguntaDTO adjuntoxpreguntaToAdjuntoxpreguntaDTO(
        Adjuntoxpregunta adjuntoxpregunta) throws Exception;

    public Adjuntoxpregunta adjuntoxpreguntaDTOToAdjuntoxpregunta(
        AdjuntoxpreguntaDTO adjuntoxpreguntaDTO) throws Exception;

    public List<AdjuntoxpreguntaDTO> listAdjuntoxpreguntaToListAdjuntoxpreguntaDTO(
        List<Adjuntoxpregunta> adjuntoxpreguntas) throws Exception;

    public List<Adjuntoxpregunta> listAdjuntoxpreguntaDTOToListAdjuntoxpregunta(
        List<AdjuntoxpreguntaDTO> adjuntoxpreguntaDTOs)
        throws Exception;
}

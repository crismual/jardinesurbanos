package co.edu.usbcali.jardines.dto.mapper;

import co.edu.usbcali.jardines.model.Galeria;
import co.edu.usbcali.jardines.model.dto.GaleriaDTO;

import java.util.List;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public interface IGaleriaMapper {
    public GaleriaDTO galeriaToGaleriaDTO(Galeria galeria)
        throws Exception;

    public Galeria galeriaDTOToGaleria(GaleriaDTO galeriaDTO)
        throws Exception;

    public List<GaleriaDTO> listGaleriaToListGaleriaDTO(List<Galeria> galerias)
        throws Exception;

    public List<Galeria> listGaleriaDTOToListGaleria(
        List<GaleriaDTO> galeriaDTOs) throws Exception;
}

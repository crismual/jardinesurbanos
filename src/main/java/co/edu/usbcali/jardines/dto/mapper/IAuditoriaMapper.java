package co.edu.usbcali.jardines.dto.mapper;

import co.edu.usbcali.jardines.model.Auditoria;
import co.edu.usbcali.jardines.model.dto.AuditoriaDTO;

import java.util.List;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public interface IAuditoriaMapper {
    public AuditoriaDTO auditoriaToAuditoriaDTO(Auditoria auditoria)
        throws Exception;

    public Auditoria auditoriaDTOToAuditoria(AuditoriaDTO auditoriaDTO)
        throws Exception;

    public List<AuditoriaDTO> listAuditoriaToListAuditoriaDTO(
        List<Auditoria> auditorias) throws Exception;

    public List<Auditoria> listAuditoriaDTOToListAuditoria(
        List<AuditoriaDTO> auditoriaDTOs) throws Exception;
}

package co.edu.usbcali.jardines.dto.mapper;

import co.edu.usbcali.jardines.model.*;
import co.edu.usbcali.jardines.model.Resena;
import co.edu.usbcali.jardines.model.control.*;
import co.edu.usbcali.jardines.model.dto.ResenaDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.Scope;

import org.springframework.stereotype.Component;

import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
@Component
@Scope("singleton")
public class ResenaMapper implements IResenaMapper {
    private static final Logger log = LoggerFactory.getLogger(ResenaMapper.class);

    /**
    * Logic injected by Spring that manages Jardin entities
    *
    */
    @Autowired
    IJardinLogic logicJardin1;

    /**
    * Logic injected by Spring that manages Usuario entities
    *
    */
    @Autowired
    IUsuarioLogic logicUsuario2;

    @Transactional(readOnly = true)
    public ResenaDTO resenaToResenaDTO(Resena resena) throws Exception {
        try {
            ResenaDTO resenaDTO = new ResenaDTO();

            resenaDTO.setResId(resena.getResId());
            resenaDTO.setDescripcion((resena.getDescripcion() != null)
                ? resena.getDescripcion() : null);
            resenaDTO.setEstado((resena.getEstado() != null)
                ? resena.getEstado() : null);
            resenaDTO.setEstadoJardin((resena.getEstadoJardin() != null)
                ? resena.getEstadoJardin() : null);
            resenaDTO.setFechaCreacion(resena.getFechaCreacion());
            resenaDTO.setFechaModificacion(resena.getFechaModificacion());
            resenaDTO.setUsuarioCreador((resena.getUsuarioCreador() != null)
                ? resena.getUsuarioCreador() : null);
            resenaDTO.setUsuarioModificador((resena.getUsuarioModificador() != null)
                ? resena.getUsuarioModificador() : null);
            resenaDTO.setJarId_Jardin((resena.getJardin().getJarId() != null)
                ? resena.getJardin().getJarId() : null);
            resenaDTO.setUsuId_Usuario((resena.getUsuario().getUsuId() != null)
                ? resena.getUsuario().getUsuId() : null);

            return resenaDTO;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public Resena resenaDTOToResena(ResenaDTO resenaDTO)
        throws Exception {
        try {
            Resena resena = new Resena();

            resena.setResId(resenaDTO.getResId());
            resena.setDescripcion((resenaDTO.getDescripcion() != null)
                ? resenaDTO.getDescripcion() : null);
            resena.setEstado((resenaDTO.getEstado() != null)
                ? resenaDTO.getEstado() : null);
            resena.setEstadoJardin((resenaDTO.getEstadoJardin() != null)
                ? resenaDTO.getEstadoJardin() : null);
            resena.setFechaCreacion(resenaDTO.getFechaCreacion());
            resena.setFechaModificacion(resenaDTO.getFechaModificacion());
            resena.setUsuarioCreador((resenaDTO.getUsuarioCreador() != null)
                ? resenaDTO.getUsuarioCreador() : null);
            resena.setUsuarioModificador((resenaDTO.getUsuarioModificador() != null)
                ? resenaDTO.getUsuarioModificador() : null);

            Jardin jardin = new Jardin();

            if (resenaDTO.getJarId_Jardin() != null) {
                jardin = logicJardin1.getJardin(resenaDTO.getJarId_Jardin());
            }

            if (jardin != null) {
                resena.setJardin(jardin);
            }

            Usuario usuario = new Usuario();

            if (resenaDTO.getUsuId_Usuario() != null) {
                usuario = logicUsuario2.getUsuario(resenaDTO.getUsuId_Usuario());
            }

            if (usuario != null) {
                resena.setUsuario(usuario);
            }

            return resena;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public List<ResenaDTO> listResenaToListResenaDTO(List<Resena> listResena)
        throws Exception {
        try {
            List<ResenaDTO> resenaDTOs = new ArrayList<ResenaDTO>();

            for (Resena resena : listResena) {
                ResenaDTO resenaDTO = resenaToResenaDTO(resena);

                resenaDTOs.add(resenaDTO);
            }

            return resenaDTOs;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public List<Resena> listResenaDTOToListResena(List<ResenaDTO> listResenaDTO)
        throws Exception {
        try {
            List<Resena> listResena = new ArrayList<Resena>();

            for (ResenaDTO resenaDTO : listResenaDTO) {
                Resena resena = resenaDTOToResena(resenaDTO);

                listResena.add(resena);
            }

            return listResena;
        } catch (Exception e) {
            throw e;
        }
    }
}

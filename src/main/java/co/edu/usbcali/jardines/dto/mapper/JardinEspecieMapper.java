package co.edu.usbcali.jardines.dto.mapper;

import co.edu.usbcali.jardines.model.*;
import co.edu.usbcali.jardines.model.JardinEspecie;
import co.edu.usbcali.jardines.model.control.*;
import co.edu.usbcali.jardines.model.dto.JardinEspecieDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.Scope;

import org.springframework.stereotype.Component;

import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
@Component
@Scope("singleton")
public class JardinEspecieMapper implements IJardinEspecieMapper {
    private static final Logger log = LoggerFactory.getLogger(JardinEspecieMapper.class);

    /**
    * Logic injected by Spring that manages Especie entities
    *
    */
    @Autowired
    IEspecieLogic logicEspecie1;

    /**
    * Logic injected by Spring that manages Jardin entities
    *
    */
    @Autowired
    IJardinLogic logicJardin2;

    @Transactional(readOnly = true)
    public JardinEspecieDTO jardinEspecieToJardinEspecieDTO(
        JardinEspecie jardinEspecie) throws Exception {
        try {
            JardinEspecieDTO jardinEspecieDTO = new JardinEspecieDTO();

            jardinEspecieDTO.setJarespId(jardinEspecie.getJarespId());
            jardinEspecieDTO.setDescripcion((jardinEspecie.getDescripcion() != null)
                ? jardinEspecie.getDescripcion() : null);
            jardinEspecieDTO.setEstado((jardinEspecie.getEstado() != null)
                ? jardinEspecie.getEstado() : null);
            jardinEspecieDTO.setFechaCreacion(jardinEspecie.getFechaCreacion());
            jardinEspecieDTO.setFechaModificacion(jardinEspecie.getFechaModificacion());
            jardinEspecieDTO.setUsuarioCreador((jardinEspecie.getUsuarioCreador() != null)
                ? jardinEspecie.getUsuarioCreador() : null);
            jardinEspecieDTO.setUsuarioModificador((jardinEspecie.getUsuarioModificador() != null)
                ? jardinEspecie.getUsuarioModificador() : null);
            jardinEspecieDTO.setEspeId_Especie((jardinEspecie.getEspecie()
                                                             .getEspeId() != null)
                ? jardinEspecie.getEspecie().getEspeId() : null);
            jardinEspecieDTO.setJarId_Jardin((jardinEspecie.getJardin()
                                                           .getJarId() != null)
                ? jardinEspecie.getJardin().getJarId() : null);

            return jardinEspecieDTO;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public JardinEspecie jardinEspecieDTOToJardinEspecie(
        JardinEspecieDTO jardinEspecieDTO) throws Exception {
        try {
            JardinEspecie jardinEspecie = new JardinEspecie();

            jardinEspecie.setJarespId(jardinEspecieDTO.getJarespId());
            jardinEspecie.setDescripcion((jardinEspecieDTO.getDescripcion() != null)
                ? jardinEspecieDTO.getDescripcion() : null);
            jardinEspecie.setEstado((jardinEspecieDTO.getEstado() != null)
                ? jardinEspecieDTO.getEstado() : null);
            jardinEspecie.setFechaCreacion(jardinEspecieDTO.getFechaCreacion());
            jardinEspecie.setFechaModificacion(jardinEspecieDTO.getFechaModificacion());
            jardinEspecie.setUsuarioCreador((jardinEspecieDTO.getUsuarioCreador() != null)
                ? jardinEspecieDTO.getUsuarioCreador() : null);
            jardinEspecie.setUsuarioModificador((jardinEspecieDTO.getUsuarioModificador() != null)
                ? jardinEspecieDTO.getUsuarioModificador() : null);

            Especie especie = new Especie();

            if (jardinEspecieDTO.getEspeId_Especie() != null) {
                especie = logicEspecie1.getEspecie(jardinEspecieDTO.getEspeId_Especie());
            }

            if (especie != null) {
                jardinEspecie.setEspecie(especie);
            }

            Jardin jardin = new Jardin();

            if (jardinEspecieDTO.getJarId_Jardin() != null) {
                jardin = logicJardin2.getJardin(jardinEspecieDTO.getJarId_Jardin());
            }

            if (jardin != null) {
                jardinEspecie.setJardin(jardin);
            }

            return jardinEspecie;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public List<JardinEspecieDTO> listJardinEspecieToListJardinEspecieDTO(
        List<JardinEspecie> listJardinEspecie) throws Exception {
        try {
            List<JardinEspecieDTO> jardinEspecieDTOs = new ArrayList<JardinEspecieDTO>();

            for (JardinEspecie jardinEspecie : listJardinEspecie) {
                JardinEspecieDTO jardinEspecieDTO = jardinEspecieToJardinEspecieDTO(jardinEspecie);

                jardinEspecieDTOs.add(jardinEspecieDTO);
            }

            return jardinEspecieDTOs;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public List<JardinEspecie> listJardinEspecieDTOToListJardinEspecie(
        List<JardinEspecieDTO> listJardinEspecieDTO) throws Exception {
        try {
            List<JardinEspecie> listJardinEspecie = new ArrayList<JardinEspecie>();

            for (JardinEspecieDTO jardinEspecieDTO : listJardinEspecieDTO) {
                JardinEspecie jardinEspecie = jardinEspecieDTOToJardinEspecie(jardinEspecieDTO);

                listJardinEspecie.add(jardinEspecie);
            }

            return listJardinEspecie;
        } catch (Exception e) {
            throw e;
        }
    }
}

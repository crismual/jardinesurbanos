package co.edu.usbcali.jardines.dto.mapper;

import co.edu.usbcali.jardines.model.*;
import co.edu.usbcali.jardines.model.Jardin;
import co.edu.usbcali.jardines.model.control.*;
import co.edu.usbcali.jardines.model.dto.JardinDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.Scope;

import org.springframework.stereotype.Component;

import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
@Component
@Scope("singleton")
public class JardinMapper implements IJardinMapper {
    private static final Logger log = LoggerFactory.getLogger(JardinMapper.class);

    /**
    * Logic injected by Spring that manages Usuario entities
    *
    */
    @Autowired
    IUsuarioLogic logicUsuario1;

    @Transactional(readOnly = true)
    public JardinDTO jardinToJardinDTO(Jardin jardin) throws Exception {
        try {
            JardinDTO jardinDTO = new JardinDTO();

            jardinDTO.setJarId(jardin.getJarId());
            jardinDTO.setAltitud((jardin.getAltitud() != null)
                ? jardin.getAltitud() : null);
            jardinDTO.setBarrio((jardin.getBarrio() != null)
                ? jardin.getBarrio() : null);
            jardinDTO.setDescripcion((jardin.getDescripcion() != null)
                ? jardin.getDescripcion() : null);
            jardinDTO.setDireccion((jardin.getDireccion() != null)
                ? jardin.getDireccion() : null);
            jardinDTO.setEstado((jardin.getEstado() != null)
                ? jardin.getEstado() : null);
            jardinDTO.setFechaCreacion(jardin.getFechaCreacion());
            jardinDTO.setFechaModificacion(jardin.getFechaModificacion());
            jardinDTO.setLatitud((jardin.getLatitud() != null)
                ? jardin.getLatitud() : null);
            jardinDTO.setNombreJardin((jardin.getNombreJardin() != null)
                ? jardin.getNombreJardin() : null);
            jardinDTO.setUsuarioCreador((jardin.getUsuarioCreador() != null)
                ? jardin.getUsuarioCreador() : null);
            jardinDTO.setUsuarioModificador((jardin.getUsuarioModificador() != null)
                ? jardin.getUsuarioModificador() : null);
            jardinDTO.setZona((jardin.getZona() != null) ? jardin.getZona() : null);
            jardinDTO.setUsuId_Usuario((jardin.getUsuario().getUsuId() != null)
                ? jardin.getUsuario().getUsuId() : null);

            return jardinDTO;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public Jardin jardinDTOToJardin(JardinDTO jardinDTO)
        throws Exception {
        try {
            Jardin jardin = new Jardin();

            jardin.setJarId(jardinDTO.getJarId());
            jardin.setAltitud((jardinDTO.getAltitud() != null)
                ? jardinDTO.getAltitud() : null);
            jardin.setBarrio((jardinDTO.getBarrio() != null)
                ? jardinDTO.getBarrio() : null);
            jardin.setDescripcion((jardinDTO.getDescripcion() != null)
                ? jardinDTO.getDescripcion() : null);
            jardin.setDireccion((jardinDTO.getDireccion() != null)
                ? jardinDTO.getDireccion() : null);
            jardin.setEstado((jardinDTO.getEstado() != null)
                ? jardinDTO.getEstado() : null);
            jardin.setFechaCreacion(jardinDTO.getFechaCreacion());
            jardin.setFechaModificacion(jardinDTO.getFechaModificacion());
            jardin.setLatitud((jardinDTO.getLatitud() != null)
                ? jardinDTO.getLatitud() : null);
            jardin.setNombreJardin((jardinDTO.getNombreJardin() != null)
                ? jardinDTO.getNombreJardin() : null);
            jardin.setUsuarioCreador((jardinDTO.getUsuarioCreador() != null)
                ? jardinDTO.getUsuarioCreador() : null);
            jardin.setUsuarioModificador((jardinDTO.getUsuarioModificador() != null)
                ? jardinDTO.getUsuarioModificador() : null);
            jardin.setZona((jardinDTO.getZona() != null) ? jardinDTO.getZona()
                                                         : null);

            Usuario usuario = new Usuario();

            if (jardinDTO.getUsuId_Usuario() != null) {
                usuario = logicUsuario1.getUsuario(jardinDTO.getUsuId_Usuario());
            }

            if (usuario != null) {
                jardin.setUsuario(usuario);
            }

            return jardin;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public List<JardinDTO> listJardinToListJardinDTO(List<Jardin> listJardin)
        throws Exception {
        try {
            List<JardinDTO> jardinDTOs = new ArrayList<JardinDTO>();

            for (Jardin jardin : listJardin) {
                JardinDTO jardinDTO = jardinToJardinDTO(jardin);

                jardinDTOs.add(jardinDTO);
            }

            return jardinDTOs;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public List<Jardin> listJardinDTOToListJardin(List<JardinDTO> listJardinDTO)
        throws Exception {
        try {
            List<Jardin> listJardin = new ArrayList<Jardin>();

            for (JardinDTO jardinDTO : listJardinDTO) {
                Jardin jardin = jardinDTOToJardin(jardinDTO);

                listJardin.add(jardin);
            }

            return listJardin;
        } catch (Exception e) {
            throw e;
        }
    }
}

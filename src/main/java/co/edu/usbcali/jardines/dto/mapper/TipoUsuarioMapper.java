package co.edu.usbcali.jardines.dto.mapper;

import co.edu.usbcali.jardines.model.*;
import co.edu.usbcali.jardines.model.TipoUsuario;
import co.edu.usbcali.jardines.model.control.*;
import co.edu.usbcali.jardines.model.dto.TipoUsuarioDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.Scope;

import org.springframework.stereotype.Component;

import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
@Component
@Scope("singleton")
public class TipoUsuarioMapper implements ITipoUsuarioMapper {
    private static final Logger log = LoggerFactory.getLogger(TipoUsuarioMapper.class);

    @Transactional(readOnly = true)
    public TipoUsuarioDTO tipoUsuarioToTipoUsuarioDTO(TipoUsuario tipoUsuario)
        throws Exception {
        try {
            TipoUsuarioDTO tipoUsuarioDTO = new TipoUsuarioDTO();

            tipoUsuarioDTO.setTiusId(tipoUsuario.getTiusId());
            tipoUsuarioDTO.setDescripcion((tipoUsuario.getDescripcion() != null)
                ? tipoUsuario.getDescripcion() : null);
            tipoUsuarioDTO.setEstado((tipoUsuario.getEstado() != null)
                ? tipoUsuario.getEstado() : null);
            tipoUsuarioDTO.setFechaCreacion(tipoUsuario.getFechaCreacion());
            tipoUsuarioDTO.setFechaModificacion(tipoUsuario.getFechaModificacion());
            tipoUsuarioDTO.setUsuarioCreador((tipoUsuario.getUsuarioCreador() != null)
                ? tipoUsuario.getUsuarioCreador() : null);
            tipoUsuarioDTO.setUsuarioModificador((tipoUsuario.getUsuarioModificador() != null)
                ? tipoUsuario.getUsuarioModificador() : null);

            return tipoUsuarioDTO;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public TipoUsuario tipoUsuarioDTOToTipoUsuario(
        TipoUsuarioDTO tipoUsuarioDTO) throws Exception {
        try {
            TipoUsuario tipoUsuario = new TipoUsuario();

            tipoUsuario.setTiusId(tipoUsuarioDTO.getTiusId());
            tipoUsuario.setDescripcion((tipoUsuarioDTO.getDescripcion() != null)
                ? tipoUsuarioDTO.getDescripcion() : null);
            tipoUsuario.setEstado((tipoUsuarioDTO.getEstado() != null)
                ? tipoUsuarioDTO.getEstado() : null);
            tipoUsuario.setFechaCreacion(tipoUsuarioDTO.getFechaCreacion());
            tipoUsuario.setFechaModificacion(tipoUsuarioDTO.getFechaModificacion());
            tipoUsuario.setUsuarioCreador((tipoUsuarioDTO.getUsuarioCreador() != null)
                ? tipoUsuarioDTO.getUsuarioCreador() : null);
            tipoUsuario.setUsuarioModificador((tipoUsuarioDTO.getUsuarioModificador() != null)
                ? tipoUsuarioDTO.getUsuarioModificador() : null);

            return tipoUsuario;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public List<TipoUsuarioDTO> listTipoUsuarioToListTipoUsuarioDTO(
        List<TipoUsuario> listTipoUsuario) throws Exception {
        try {
            List<TipoUsuarioDTO> tipoUsuarioDTOs = new ArrayList<TipoUsuarioDTO>();

            for (TipoUsuario tipoUsuario : listTipoUsuario) {
                TipoUsuarioDTO tipoUsuarioDTO = tipoUsuarioToTipoUsuarioDTO(tipoUsuario);

                tipoUsuarioDTOs.add(tipoUsuarioDTO);
            }

            return tipoUsuarioDTOs;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public List<TipoUsuario> listTipoUsuarioDTOToListTipoUsuario(
        List<TipoUsuarioDTO> listTipoUsuarioDTO) throws Exception {
        try {
            List<TipoUsuario> listTipoUsuario = new ArrayList<TipoUsuario>();

            for (TipoUsuarioDTO tipoUsuarioDTO : listTipoUsuarioDTO) {
                TipoUsuario tipoUsuario = tipoUsuarioDTOToTipoUsuario(tipoUsuarioDTO);

                listTipoUsuario.add(tipoUsuario);
            }

            return listTipoUsuario;
        } catch (Exception e) {
            throw e;
        }
    }
}

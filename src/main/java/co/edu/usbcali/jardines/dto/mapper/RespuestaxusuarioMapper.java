package co.edu.usbcali.jardines.dto.mapper;

import co.edu.usbcali.jardines.model.*;
import co.edu.usbcali.jardines.model.Respuestaxusuario;
import co.edu.usbcali.jardines.model.control.*;
import co.edu.usbcali.jardines.model.dto.RespuestaxusuarioDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.Scope;

import org.springframework.stereotype.Component;

import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
@Component
@Scope("singleton")
public class RespuestaxusuarioMapper implements IRespuestaxusuarioMapper {
    private static final Logger log = LoggerFactory.getLogger(RespuestaxusuarioMapper.class);

    /**
    * Logic injected by Spring that manages Jardin entities
    *
    */
    @Autowired
    IJardinLogic logicJardin1;

    /**
    * Logic injected by Spring that manages Respuesta entities
    *
    */
    @Autowired
    IRespuestaLogic logicRespuesta2;

    /**
    * Logic injected by Spring that manages Usuario entities
    *
    */
    @Autowired
    IUsuarioLogic logicUsuario3;

    @Transactional(readOnly = true)
    public RespuestaxusuarioDTO respuestaxusuarioToRespuestaxusuarioDTO(
        Respuestaxusuario respuestaxusuario) throws Exception {
        try {
            RespuestaxusuarioDTO respuestaxusuarioDTO = new RespuestaxusuarioDTO();

            respuestaxusuarioDTO.setRespusuId(respuestaxusuario.getRespusuId());
            respuestaxusuarioDTO.setDescripcion((respuestaxusuario.getDescripcion() != null)
                ? respuestaxusuario.getDescripcion() : null);
            respuestaxusuarioDTO.setDescripcionPregunta((respuestaxusuario.getDescripcionPregunta() != null)
                ? respuestaxusuario.getDescripcionPregunta() : null);
            respuestaxusuarioDTO.setEstado((respuestaxusuario.getEstado() != null)
                ? respuestaxusuario.getEstado() : null);
            respuestaxusuarioDTO.setFechaCreacion(respuestaxusuario.getFechaCreacion());
            respuestaxusuarioDTO.setFechaModificacion(respuestaxusuario.getFechaModificacion());
            respuestaxusuarioDTO.setUsuarioCreador((respuestaxusuario.getUsuarioCreador() != null)
                ? respuestaxusuario.getUsuarioCreador() : null);
            respuestaxusuarioDTO.setUsuarioModificador((respuestaxusuario.getUsuarioModificador() != null)
                ? respuestaxusuario.getUsuarioModificador() : null);
            respuestaxusuarioDTO.setJarId_Jardin((respuestaxusuario.getJardin()
                                                                   .getJarId() != null)
                ? respuestaxusuario.getJardin().getJarId() : null);
            respuestaxusuarioDTO.setRespId_Respuesta((respuestaxusuario.getRespuesta()
                                                                       .getRespId() != null)
                ? respuestaxusuario.getRespuesta().getRespId() : null);
            respuestaxusuarioDTO.setUsuId_Usuario((respuestaxusuario.getUsuario()
                                                                    .getUsuId() != null)
                ? respuestaxusuario.getUsuario().getUsuId() : null);

            return respuestaxusuarioDTO;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public Respuestaxusuario respuestaxusuarioDTOToRespuestaxusuario(
        RespuestaxusuarioDTO respuestaxusuarioDTO) throws Exception {
        try {
            Respuestaxusuario respuestaxusuario = new Respuestaxusuario();

            respuestaxusuario.setRespusuId(respuestaxusuarioDTO.getRespusuId());
            respuestaxusuario.setDescripcion((respuestaxusuarioDTO.getDescripcion() != null)
                ? respuestaxusuarioDTO.getDescripcion() : null);
            respuestaxusuario.setDescripcionPregunta((respuestaxusuarioDTO.getDescripcionPregunta() != null)
                ? respuestaxusuarioDTO.getDescripcionPregunta() : null);
            respuestaxusuario.setEstado((respuestaxusuarioDTO.getEstado() != null)
                ? respuestaxusuarioDTO.getEstado() : null);
            respuestaxusuario.setFechaCreacion(respuestaxusuarioDTO.getFechaCreacion());
            respuestaxusuario.setFechaModificacion(respuestaxusuarioDTO.getFechaModificacion());
            respuestaxusuario.setUsuarioCreador((respuestaxusuarioDTO.getUsuarioCreador() != null)
                ? respuestaxusuarioDTO.getUsuarioCreador() : null);
            respuestaxusuario.setUsuarioModificador((respuestaxusuarioDTO.getUsuarioModificador() != null)
                ? respuestaxusuarioDTO.getUsuarioModificador() : null);

            Jardin jardin = new Jardin();

            if (respuestaxusuarioDTO.getJarId_Jardin() != null) {
                jardin = logicJardin1.getJardin(respuestaxusuarioDTO.getJarId_Jardin());
            }

            if (jardin != null) {
                respuestaxusuario.setJardin(jardin);
            }

            Respuesta respuesta = new Respuesta();

            if (respuestaxusuarioDTO.getRespId_Respuesta() != null) {
                respuesta = logicRespuesta2.getRespuesta(respuestaxusuarioDTO.getRespId_Respuesta());
            }

            if (respuesta != null) {
                respuestaxusuario.setRespuesta(respuesta);
            }

            Usuario usuario = new Usuario();

            if (respuestaxusuarioDTO.getUsuId_Usuario() != null) {
                usuario = logicUsuario3.getUsuario(respuestaxusuarioDTO.getUsuId_Usuario());
            }

            if (usuario != null) {
                respuestaxusuario.setUsuario(usuario);
            }

            return respuestaxusuario;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public List<RespuestaxusuarioDTO> listRespuestaxusuarioToListRespuestaxusuarioDTO(
        List<Respuestaxusuario> listRespuestaxusuario)
        throws Exception {
        try {
            List<RespuestaxusuarioDTO> respuestaxusuarioDTOs = new ArrayList<RespuestaxusuarioDTO>();

            for (Respuestaxusuario respuestaxusuario : listRespuestaxusuario) {
                RespuestaxusuarioDTO respuestaxusuarioDTO = respuestaxusuarioToRespuestaxusuarioDTO(respuestaxusuario);

                respuestaxusuarioDTOs.add(respuestaxusuarioDTO);
            }

            return respuestaxusuarioDTOs;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public List<Respuestaxusuario> listRespuestaxusuarioDTOToListRespuestaxusuario(
        List<RespuestaxusuarioDTO> listRespuestaxusuarioDTO)
        throws Exception {
        try {
            List<Respuestaxusuario> listRespuestaxusuario = new ArrayList<Respuestaxusuario>();

            for (RespuestaxusuarioDTO respuestaxusuarioDTO : listRespuestaxusuarioDTO) {
                Respuestaxusuario respuestaxusuario = respuestaxusuarioDTOToRespuestaxusuario(respuestaxusuarioDTO);

                listRespuestaxusuario.add(respuestaxusuario);
            }

            return listRespuestaxusuario;
        } catch (Exception e) {
            throw e;
        }
    }
}

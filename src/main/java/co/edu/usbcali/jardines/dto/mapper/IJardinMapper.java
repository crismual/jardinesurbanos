package co.edu.usbcali.jardines.dto.mapper;

import co.edu.usbcali.jardines.model.Jardin;
import co.edu.usbcali.jardines.model.dto.JardinDTO;

import java.util.List;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public interface IJardinMapper {
    public JardinDTO jardinToJardinDTO(Jardin jardin) throws Exception;

    public Jardin jardinDTOToJardin(JardinDTO jardinDTO)
        throws Exception;

    public List<JardinDTO> listJardinToListJardinDTO(List<Jardin> jardins)
        throws Exception;

    public List<Jardin> listJardinDTOToListJardin(List<JardinDTO> jardinDTOs)
        throws Exception;
}

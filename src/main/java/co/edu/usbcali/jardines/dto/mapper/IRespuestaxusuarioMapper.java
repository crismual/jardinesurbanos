package co.edu.usbcali.jardines.dto.mapper;

import co.edu.usbcali.jardines.model.Respuestaxusuario;
import co.edu.usbcali.jardines.model.dto.RespuestaxusuarioDTO;

import java.util.List;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public interface IRespuestaxusuarioMapper {
    public RespuestaxusuarioDTO respuestaxusuarioToRespuestaxusuarioDTO(
        Respuestaxusuario respuestaxusuario) throws Exception;

    public Respuestaxusuario respuestaxusuarioDTOToRespuestaxusuario(
        RespuestaxusuarioDTO respuestaxusuarioDTO) throws Exception;

    public List<RespuestaxusuarioDTO> listRespuestaxusuarioToListRespuestaxusuarioDTO(
        List<Respuestaxusuario> respuestaxusuarios) throws Exception;

    public List<Respuestaxusuario> listRespuestaxusuarioDTOToListRespuestaxusuario(
        List<RespuestaxusuarioDTO> respuestaxusuarioDTOs)
        throws Exception;
}

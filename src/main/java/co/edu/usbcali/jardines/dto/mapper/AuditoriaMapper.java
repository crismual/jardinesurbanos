package co.edu.usbcali.jardines.dto.mapper;

import co.edu.usbcali.jardines.model.*;
import co.edu.usbcali.jardines.model.Auditoria;
import co.edu.usbcali.jardines.model.control.*;
import co.edu.usbcali.jardines.model.dto.AuditoriaDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.Scope;

import org.springframework.stereotype.Component;

import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
@Component
@Scope("singleton")
public class AuditoriaMapper implements IAuditoriaMapper {
    private static final Logger log = LoggerFactory.getLogger(AuditoriaMapper.class);

    /**
    * Logic injected by Spring that manages Usuario entities
    *
    */
    @Autowired
    IUsuarioLogic logicUsuario1;

    @Transactional(readOnly = true)
    public AuditoriaDTO auditoriaToAuditoriaDTO(Auditoria auditoria)
        throws Exception {
        try {
            AuditoriaDTO auditoriaDTO = new AuditoriaDTO();

            auditoriaDTO.setAudId(auditoria.getAudId());
            auditoriaDTO.setDescripcion((auditoria.getDescripcion() != null)
                ? auditoria.getDescripcion() : null);
            auditoriaDTO.setEstado((auditoria.getEstado() != null)
                ? auditoria.getEstado() : null);
            auditoriaDTO.setFechaCreacion(auditoria.getFechaCreacion());
            auditoriaDTO.setFechaModificacion(auditoria.getFechaModificacion());
            auditoriaDTO.setModificacion((auditoria.getModificacion() != null)
                ? auditoria.getModificacion() : null);
            auditoriaDTO.setUsuarioCreador((auditoria.getUsuarioCreador() != null)
                ? auditoria.getUsuarioCreador() : null);
            auditoriaDTO.setUsuarioModificador((auditoria.getUsuarioModificador() != null)
                ? auditoria.getUsuarioModificador() : null);
            auditoriaDTO.setUsuId_Usuario((auditoria.getUsuario().getUsuId() != null)
                ? auditoria.getUsuario().getUsuId() : null);

            return auditoriaDTO;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public Auditoria auditoriaDTOToAuditoria(AuditoriaDTO auditoriaDTO)
        throws Exception {
        try {
            Auditoria auditoria = new Auditoria();

            auditoria.setAudId(auditoriaDTO.getAudId());
            auditoria.setDescripcion((auditoriaDTO.getDescripcion() != null)
                ? auditoriaDTO.getDescripcion() : null);
            auditoria.setEstado((auditoriaDTO.getEstado() != null)
                ? auditoriaDTO.getEstado() : null);
            auditoria.setFechaCreacion(auditoriaDTO.getFechaCreacion());
            auditoria.setFechaModificacion(auditoriaDTO.getFechaModificacion());
            auditoria.setModificacion((auditoriaDTO.getModificacion() != null)
                ? auditoriaDTO.getModificacion() : null);
            auditoria.setUsuarioCreador((auditoriaDTO.getUsuarioCreador() != null)
                ? auditoriaDTO.getUsuarioCreador() : null);
            auditoria.setUsuarioModificador((auditoriaDTO.getUsuarioModificador() != null)
                ? auditoriaDTO.getUsuarioModificador() : null);

            Usuario usuario = new Usuario();

            if (auditoriaDTO.getUsuId_Usuario() != null) {
                usuario = logicUsuario1.getUsuario(auditoriaDTO.getUsuId_Usuario());
            }

            if (usuario != null) {
                auditoria.setUsuario(usuario);
            }

            return auditoria;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public List<AuditoriaDTO> listAuditoriaToListAuditoriaDTO(
        List<Auditoria> listAuditoria) throws Exception {
        try {
            List<AuditoriaDTO> auditoriaDTOs = new ArrayList<AuditoriaDTO>();

            for (Auditoria auditoria : listAuditoria) {
                AuditoriaDTO auditoriaDTO = auditoriaToAuditoriaDTO(auditoria);

                auditoriaDTOs.add(auditoriaDTO);
            }

            return auditoriaDTOs;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public List<Auditoria> listAuditoriaDTOToListAuditoria(
        List<AuditoriaDTO> listAuditoriaDTO) throws Exception {
        try {
            List<Auditoria> listAuditoria = new ArrayList<Auditoria>();

            for (AuditoriaDTO auditoriaDTO : listAuditoriaDTO) {
                Auditoria auditoria = auditoriaDTOToAuditoria(auditoriaDTO);

                listAuditoria.add(auditoria);
            }

            return listAuditoria;
        } catch (Exception e) {
            throw e;
        }
    }
}

package co.edu.usbcali.jardines.dto.mapper;

import co.edu.usbcali.jardines.model.*;
import co.edu.usbcali.jardines.model.Respuesta;
import co.edu.usbcali.jardines.model.control.*;
import co.edu.usbcali.jardines.model.dto.RespuestaDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.Scope;

import org.springframework.stereotype.Component;

import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
@Component
@Scope("singleton")
public class RespuestaMapper implements IRespuestaMapper {
    private static final Logger log = LoggerFactory.getLogger(RespuestaMapper.class);

    /**
    * Logic injected by Spring that manages Pregunta entities
    *
    */
    @Autowired
    IPreguntaLogic logicPregunta1;

    @Transactional(readOnly = true)
    public RespuestaDTO respuestaToRespuestaDTO(Respuesta respuesta)
        throws Exception {
        try {
            RespuestaDTO respuestaDTO = new RespuestaDTO();

            respuestaDTO.setRespId(respuesta.getRespId());
            respuestaDTO.setDescripcion((respuesta.getDescripcion() != null)
                ? respuesta.getDescripcion() : null);
            respuestaDTO.setDescripcionRespuesta((respuesta.getDescripcionRespuesta() != null)
                ? respuesta.getDescripcionRespuesta() : null);
            respuestaDTO.setEstado((respuesta.getEstado() != null)
                ? respuesta.getEstado() : null);
            respuestaDTO.setFechaCreacion(respuesta.getFechaCreacion());
            respuestaDTO.setFechaModificacion(respuesta.getFechaModificacion());
            respuestaDTO.setUsuarioCreador((respuesta.getUsuarioCreador() != null)
                ? respuesta.getUsuarioCreador() : null);
            respuestaDTO.setUsuarioModificador((respuesta.getUsuarioModificador() != null)
                ? respuesta.getUsuarioModificador() : null);
            respuestaDTO.setPreId_Pregunta((respuesta.getPregunta().getPreId() != null)
                ? respuesta.getPregunta().getPreId() : null);

            return respuestaDTO;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public Respuesta respuestaDTOToRespuesta(RespuestaDTO respuestaDTO)
        throws Exception {
        try {
            Respuesta respuesta = new Respuesta();

            respuesta.setRespId(respuestaDTO.getRespId());
            respuesta.setDescripcion((respuestaDTO.getDescripcion() != null)
                ? respuestaDTO.getDescripcion() : null);
            respuesta.setDescripcionRespuesta((respuestaDTO.getDescripcionRespuesta() != null)
                ? respuestaDTO.getDescripcionRespuesta() : null);
            respuesta.setEstado((respuestaDTO.getEstado() != null)
                ? respuestaDTO.getEstado() : null);
            respuesta.setFechaCreacion(respuestaDTO.getFechaCreacion());
            respuesta.setFechaModificacion(respuestaDTO.getFechaModificacion());
            respuesta.setUsuarioCreador((respuestaDTO.getUsuarioCreador() != null)
                ? respuestaDTO.getUsuarioCreador() : null);
            respuesta.setUsuarioModificador((respuestaDTO.getUsuarioModificador() != null)
                ? respuestaDTO.getUsuarioModificador() : null);

            Pregunta pregunta = new Pregunta();

            if (respuestaDTO.getPreId_Pregunta() != null) {
                pregunta = logicPregunta1.getPregunta(respuestaDTO.getPreId_Pregunta());
            }

            if (pregunta != null) {
                respuesta.setPregunta(pregunta);
            }

            return respuesta;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public List<RespuestaDTO> listRespuestaToListRespuestaDTO(
        List<Respuesta> listRespuesta) throws Exception {
        try {
            List<RespuestaDTO> respuestaDTOs = new ArrayList<RespuestaDTO>();

            for (Respuesta respuesta : listRespuesta) {
                RespuestaDTO respuestaDTO = respuestaToRespuestaDTO(respuesta);

                respuestaDTOs.add(respuestaDTO);
            }

            return respuestaDTOs;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public List<Respuesta> listRespuestaDTOToListRespuesta(
        List<RespuestaDTO> listRespuestaDTO) throws Exception {
        try {
            List<Respuesta> listRespuesta = new ArrayList<Respuesta>();

            for (RespuestaDTO respuestaDTO : listRespuestaDTO) {
                Respuesta respuesta = respuestaDTOToRespuesta(respuestaDTO);

                listRespuesta.add(respuesta);
            }

            return listRespuesta;
        } catch (Exception e) {
            throw e;
        }
    }
}

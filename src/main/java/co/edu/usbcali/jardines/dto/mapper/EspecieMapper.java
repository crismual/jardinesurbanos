package co.edu.usbcali.jardines.dto.mapper;

import co.edu.usbcali.jardines.model.*;
import co.edu.usbcali.jardines.model.Especie;
import co.edu.usbcali.jardines.model.control.*;
import co.edu.usbcali.jardines.model.dto.EspecieDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.Scope;

import org.springframework.stereotype.Component;

import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
@Component
@Scope("singleton")
public class EspecieMapper implements IEspecieMapper {
    private static final Logger log = LoggerFactory.getLogger(EspecieMapper.class);

    /**
    * Logic injected by Spring that manages Usuario entities
    *
    */
    @Autowired
    IUsuarioLogic logicUsuario1;

    @Transactional(readOnly = true)
    public EspecieDTO especieToEspecieDTO(Especie especie)
        throws Exception {
        try {
            EspecieDTO especieDTO = new EspecieDTO();

            especieDTO.setEspeId(especie.getEspeId());
            especieDTO.setAltitud((especie.getAltitud() != null)
                ? especie.getAltitud() : null);
            especieDTO.setCuidados((especie.getCuidados() != null)
                ? especie.getCuidados() : null);
            especieDTO.setDescripcion((especie.getDescripcion() != null)
                ? especie.getDescripcion() : null);
            especieDTO.setEstado((especie.getEstado() != null)
                ? especie.getEstado() : null);
            especieDTO.setFechaCreacion(especie.getFechaCreacion());
            especieDTO.setFechaModificacion(especie.getFechaModificacion());
            especieDTO.setFlor((especie.getFlor() != null) ? especie.getFlor()
                                                           : null);
            especieDTO.setFruto((especie.getFruto() != null)
                ? especie.getFruto() : null);
            especieDTO.setNombreCientifico((especie.getNombreCientifico() != null)
                ? especie.getNombreCientifico() : null);
            especieDTO.setNombreComun((especie.getNombreComun() != null)
                ? especie.getNombreComun() : null);
            especieDTO.setPlagas((especie.getPlagas() != null)
                ? especie.getPlagas() : null);
            especieDTO.setRaiz((especie.getRaiz() != null) ? especie.getRaiz()
                                                           : null);
            especieDTO.setRiego((especie.getRiego() != null)
                ? especie.getRiego() : null);
            especieDTO.setSilueta((especie.getSilueta() != null)
                ? especie.getSilueta() : null);
            especieDTO.setSol((especie.getSol() != null) ? especie.getSol() : null);
            especieDTO.setSuelo((especie.getSuelo() != null)
                ? especie.getSuelo() : null);
            especieDTO.setUsuarioCreador((especie.getUsuarioCreador() != null)
                ? especie.getUsuarioCreador() : null);
            especieDTO.setUsuarioModificador((especie.getUsuarioModificador() != null)
                ? especie.getUsuarioModificador() : null);
            especieDTO.setUsuId_Usuario((especie.getUsuario().getUsuId() != null)
                ? especie.getUsuario().getUsuId() : null);

            return especieDTO;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public Especie especieDTOToEspecie(EspecieDTO especieDTO)
        throws Exception {
        try {
            Especie especie = new Especie();

            especie.setEspeId(especieDTO.getEspeId());
            especie.setAltitud((especieDTO.getAltitud() != null)
                ? especieDTO.getAltitud() : null);
            especie.setCuidados((especieDTO.getCuidados() != null)
                ? especieDTO.getCuidados() : null);
            especie.setDescripcion((especieDTO.getDescripcion() != null)
                ? especieDTO.getDescripcion() : null);
            especie.setEstado((especieDTO.getEstado() != null)
                ? especieDTO.getEstado() : null);
            especie.setFechaCreacion(especieDTO.getFechaCreacion());
            especie.setFechaModificacion(especieDTO.getFechaModificacion());
            especie.setFlor((especieDTO.getFlor() != null)
                ? especieDTO.getFlor() : null);
            especie.setFruto((especieDTO.getFruto() != null)
                ? especieDTO.getFruto() : null);
            especie.setNombreCientifico((especieDTO.getNombreCientifico() != null)
                ? especieDTO.getNombreCientifico() : null);
            especie.setNombreComun((especieDTO.getNombreComun() != null)
                ? especieDTO.getNombreComun() : null);
            especie.setPlagas((especieDTO.getPlagas() != null)
                ? especieDTO.getPlagas() : null);
            especie.setRaiz((especieDTO.getRaiz() != null)
                ? especieDTO.getRaiz() : null);
            especie.setRiego((especieDTO.getRiego() != null)
                ? especieDTO.getRiego() : null);
            especie.setSilueta((especieDTO.getSilueta() != null)
                ? especieDTO.getSilueta() : null);
            especie.setSol((especieDTO.getSol() != null) ? especieDTO.getSol()
                                                         : null);
            especie.setSuelo((especieDTO.getSuelo() != null)
                ? especieDTO.getSuelo() : null);
            especie.setUsuarioCreador((especieDTO.getUsuarioCreador() != null)
                ? especieDTO.getUsuarioCreador() : null);
            especie.setUsuarioModificador((especieDTO.getUsuarioModificador() != null)
                ? especieDTO.getUsuarioModificador() : null);

            Usuario usuario = new Usuario();

            if (especieDTO.getUsuId_Usuario() != null) {
                usuario = logicUsuario1.getUsuario(especieDTO.getUsuId_Usuario());
            }

            if (usuario != null) {
                especie.setUsuario(usuario);
            }

            return especie;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public List<EspecieDTO> listEspecieToListEspecieDTO(
        List<Especie> listEspecie) throws Exception {
        try {
            List<EspecieDTO> especieDTOs = new ArrayList<EspecieDTO>();

            for (Especie especie : listEspecie) {
                EspecieDTO especieDTO = especieToEspecieDTO(especie);

                especieDTOs.add(especieDTO);
            }

            return especieDTOs;
        } catch (Exception e) {
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public List<Especie> listEspecieDTOToListEspecie(
        List<EspecieDTO> listEspecieDTO) throws Exception {
        try {
            List<Especie> listEspecie = new ArrayList<Especie>();

            for (EspecieDTO especieDTO : listEspecieDTO) {
                Especie especie = especieDTOToEspecie(especieDTO);

                listEspecie.add(especie);
            }

            return listEspecie;
        } catch (Exception e) {
            throw e;
        }
    }
}

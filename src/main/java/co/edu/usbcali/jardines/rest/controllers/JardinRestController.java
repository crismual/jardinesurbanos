package co.edu.usbcali.jardines.rest.controllers;

import co.edu.usbcali.jardines.dto.mapper.IJardinMapper;
import co.edu.usbcali.jardines.model.*;
import co.edu.usbcali.jardines.model.dto.JardinDTO;
import co.edu.usbcali.jardines.presentation.businessDelegate.IBusinessDelegatorView;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/jardin")
public class JardinRestController {
    private static final Logger log = LoggerFactory.getLogger(JardinRestController.class);
    @Autowired
    private IBusinessDelegatorView businessDelegatorView;
    @Autowired
    private IJardinMapper jardinMapper;

    @PostMapping(value = "/saveJardin")
    public void saveJardin(@RequestBody
    JardinDTO jardinDTO) throws Exception {
        try {
            Jardin jardin = jardinMapper.jardinDTOToJardin(jardinDTO);

            businessDelegatorView.saveJardin(jardin);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @DeleteMapping(value = "/deleteJardin/{jarId}")
    public void deleteJardin(@PathVariable("jarId")
    Long jarId) throws Exception {
        try {
            Jardin jardin = businessDelegatorView.getJardin(jarId);

            businessDelegatorView.deleteJardin(jardin);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @PutMapping(value = "/updateJardin/")
    public void updateJardin(@RequestBody
    JardinDTO jardinDTO) throws Exception {
        try {
            Jardin jardin = jardinMapper.jardinDTOToJardin(jardinDTO);

            businessDelegatorView.updateJardin(jardin);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @GetMapping(value = "/getDataJardin")
    public List<JardinDTO> getDataJardin() throws Exception {
        try {
            return businessDelegatorView.getDataJardin();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @GetMapping(value = "/getJardin/{jarId}")
    public JardinDTO getJardin(@PathVariable("jarId")
    Long jarId) throws Exception {
        try {
            Jardin jardin = businessDelegatorView.getJardin(jarId);

            return jardinMapper.jardinToJardinDTO(jardin);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return null;
    }
}

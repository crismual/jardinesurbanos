package co.edu.usbcali.jardines.rest.controllers;

import co.edu.usbcali.jardines.dto.mapper.IResenaMapper;
import co.edu.usbcali.jardines.model.*;
import co.edu.usbcali.jardines.model.dto.ResenaDTO;
import co.edu.usbcali.jardines.presentation.businessDelegate.IBusinessDelegatorView;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/resena")
public class ResenaRestController {
    private static final Logger log = LoggerFactory.getLogger(ResenaRestController.class);
    @Autowired
    private IBusinessDelegatorView businessDelegatorView;
    @Autowired
    private IResenaMapper resenaMapper;

    @PostMapping(value = "/saveResena")
    public void saveResena(@RequestBody
    ResenaDTO resenaDTO) throws Exception {
        try {
            Resena resena = resenaMapper.resenaDTOToResena(resenaDTO);

            businessDelegatorView.saveResena(resena);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @DeleteMapping(value = "/deleteResena/{resId}")
    public void deleteResena(@PathVariable("resId")
    Long resId) throws Exception {
        try {
            Resena resena = businessDelegatorView.getResena(resId);

            businessDelegatorView.deleteResena(resena);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @PutMapping(value = "/updateResena/")
    public void updateResena(@RequestBody
    ResenaDTO resenaDTO) throws Exception {
        try {
            Resena resena = resenaMapper.resenaDTOToResena(resenaDTO);

            businessDelegatorView.updateResena(resena);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @GetMapping(value = "/getDataResena")
    public List<ResenaDTO> getDataResena() throws Exception {
        try {
            return businessDelegatorView.getDataResena();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @GetMapping(value = "/getResena/{resId}")
    public ResenaDTO getResena(@PathVariable("resId")
    Long resId) throws Exception {
        try {
            Resena resena = businessDelegatorView.getResena(resId);

            return resenaMapper.resenaToResenaDTO(resena);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return null;
    }
}

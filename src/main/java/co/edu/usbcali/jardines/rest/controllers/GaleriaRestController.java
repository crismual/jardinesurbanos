package co.edu.usbcali.jardines.rest.controllers;

import co.edu.usbcali.jardines.dto.mapper.IGaleriaMapper;
import co.edu.usbcali.jardines.model.*;
import co.edu.usbcali.jardines.model.dto.GaleriaDTO;
import co.edu.usbcali.jardines.presentation.businessDelegate.IBusinessDelegatorView;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;


@RestController
@RequestMapping("/galeria")
public class GaleriaRestController {
    private static final Logger log = LoggerFactory.getLogger(GaleriaRestController.class);
    @Autowired
    private IBusinessDelegatorView businessDelegatorView;
    @Autowired
    private IGaleriaMapper galeriaMapper;

    @PostMapping(value = "/saveGaleria")
    public void saveGaleria(@RequestBody
    GaleriaDTO galeriaDTO) throws Exception {
        try {
            Galeria galeria = galeriaMapper.galeriaDTOToGaleria(galeriaDTO);

            businessDelegatorView.saveGaleria(galeria);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @DeleteMapping(value = "/deleteGaleria/{galId}/{fechaCreacion}/{fechaModificacion}/{usuarioCreador}/{usuarioModificador}/{estado}/{descripcion}/{urlFotoGaleria}/{jardinJarId}/{usuarioUsuId}")
    public void deleteGaleria(@PathVariable("galId")
    Long galId, @PathVariable("fechaCreacion")
    Date fechaCreacion,
        @PathVariable("fechaModificacion")
    Date fechaModificacion,
        @PathVariable("usuarioCreador")
    String usuarioCreador,
        @PathVariable("usuarioModificador")
    String usuarioModificador, @PathVariable("estado")
    String estado, @PathVariable("descripcion")
    String descripcion, @PathVariable("urlFotoGaleria")
    String urlFotoGaleria, @PathVariable("jardinJarId")
    Long jardinJarId, @PathVariable("usuarioUsuId")
    Long usuarioUsuId) throws Exception {
        try {
            GaleriaId id = new GaleriaId();

            id.setGalId(galId);
            id.setFechaCreacion(fechaCreacion);
            id.setFechaModificacion(fechaModificacion);
            id.setUsuarioCreador(usuarioCreador);
            id.setUsuarioModificador(usuarioModificador);
            id.setEstado(estado);
            id.setDescripcion(descripcion);
            id.setUrlFotoGaleria(urlFotoGaleria);
            id.setJardinJarId(jardinJarId);
            id.setUsuarioUsuId(usuarioUsuId);

            Galeria galeria = businessDelegatorView.getGaleria(id);

            businessDelegatorView.deleteGaleria(galeria);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @PutMapping(value = "/updateGaleria/")
    public void updateGaleria(@RequestBody
    GaleriaDTO galeriaDTO) throws Exception {
        try {
            Galeria galeria = galeriaMapper.galeriaDTOToGaleria(galeriaDTO);

            businessDelegatorView.updateGaleria(galeria);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @GetMapping(value = "/getDataGaleria")
    public List<GaleriaDTO> getDataGaleria() throws Exception {
        try {
            return businessDelegatorView.getDataGaleria();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @GetMapping(value = "/getGaleria/{galId}/{fechaCreacion}/{fechaModificacion}/{usuarioCreador}/{usuarioModificador}/{estado}/{descripcion}/{urlFotoGaleria}/{jardinJarId}/{usuarioUsuId}")
    public GaleriaDTO getGaleria(@PathVariable("galId")
    Long galId, @PathVariable("fechaCreacion")
    Date fechaCreacion,
        @PathVariable("fechaModificacion")
    Date fechaModificacion,
        @PathVariable("usuarioCreador")
    String usuarioCreador,
        @PathVariable("usuarioModificador")
    String usuarioModificador, @PathVariable("estado")
    String estado, @PathVariable("descripcion")
    String descripcion, @PathVariable("urlFotoGaleria")
    String urlFotoGaleria, @PathVariable("jardinJarId")
    Long jardinJarId, @PathVariable("usuarioUsuId")
    Long usuarioUsuId) throws Exception {
        try {
            GaleriaId id = new GaleriaId();

            id.setGalId(galId);
            id.setFechaCreacion(fechaCreacion);
            id.setFechaModificacion(fechaModificacion);
            id.setUsuarioCreador(usuarioCreador);
            id.setUsuarioModificador(usuarioModificador);
            id.setEstado(estado);
            id.setDescripcion(descripcion);
            id.setUrlFotoGaleria(urlFotoGaleria);
            id.setJardinJarId(jardinJarId);
            id.setUsuarioUsuId(usuarioUsuId);

            Galeria galeria = businessDelegatorView.getGaleria(id);

            return galeriaMapper.galeriaToGaleriaDTO(galeria);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return null;
    }
}

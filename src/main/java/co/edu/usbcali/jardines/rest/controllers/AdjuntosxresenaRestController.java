package co.edu.usbcali.jardines.rest.controllers;

import co.edu.usbcali.jardines.dto.mapper.IAdjuntosxresenaMapper;
import co.edu.usbcali.jardines.model.*;
import co.edu.usbcali.jardines.model.dto.AdjuntosxresenaDTO;
import co.edu.usbcali.jardines.presentation.businessDelegate.IBusinessDelegatorView;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/adjuntosxresena")
public class AdjuntosxresenaRestController {
    private static final Logger log = LoggerFactory.getLogger(AdjuntosxresenaRestController.class);
    @Autowired
    private IBusinessDelegatorView businessDelegatorView;
    @Autowired
    private IAdjuntosxresenaMapper adjuntosxresenaMapper;

    @PostMapping(value = "/saveAdjuntosxresena")
    public void saveAdjuntosxresena(
        @RequestBody
    AdjuntosxresenaDTO adjuntosxresenaDTO) throws Exception {
        try {
            Adjuntosxresena adjuntosxresena = adjuntosxresenaMapper.adjuntosxresenaDTOToAdjuntosxresena(adjuntosxresenaDTO);

            businessDelegatorView.saveAdjuntosxresena(adjuntosxresena);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @DeleteMapping(value = "/deleteAdjuntosxresena/{adresId}")
    public void deleteAdjuntosxresena(@PathVariable("adresId")
    Long adresId) throws Exception {
        try {
            Adjuntosxresena adjuntosxresena = businessDelegatorView.getAdjuntosxresena(adresId);

            businessDelegatorView.deleteAdjuntosxresena(adjuntosxresena);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @PutMapping(value = "/updateAdjuntosxresena/")
    public void updateAdjuntosxresena(
        @RequestBody
    AdjuntosxresenaDTO adjuntosxresenaDTO) throws Exception {
        try {
            Adjuntosxresena adjuntosxresena = adjuntosxresenaMapper.adjuntosxresenaDTOToAdjuntosxresena(adjuntosxresenaDTO);

            businessDelegatorView.updateAdjuntosxresena(adjuntosxresena);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @GetMapping(value = "/getDataAdjuntosxresena")
    public List<AdjuntosxresenaDTO> getDataAdjuntosxresena()
        throws Exception {
        try {
            return businessDelegatorView.getDataAdjuntosxresena();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @GetMapping(value = "/getAdjuntosxresena/{adresId}")
    public AdjuntosxresenaDTO getAdjuntosxresena(
        @PathVariable("adresId")
    Long adresId) throws Exception {
        try {
            Adjuntosxresena adjuntosxresena = businessDelegatorView.getAdjuntosxresena(adresId);

            return adjuntosxresenaMapper.adjuntosxresenaToAdjuntosxresenaDTO(adjuntosxresena);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return null;
    }
}

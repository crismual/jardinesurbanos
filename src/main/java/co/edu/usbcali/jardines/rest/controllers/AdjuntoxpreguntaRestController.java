package co.edu.usbcali.jardines.rest.controllers;

import co.edu.usbcali.jardines.dto.mapper.IAdjuntoxpreguntaMapper;
import co.edu.usbcali.jardines.model.*;
import co.edu.usbcali.jardines.model.dto.AdjuntoxpreguntaDTO;
import co.edu.usbcali.jardines.presentation.businessDelegate.IBusinessDelegatorView;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/adjuntoxpregunta")
public class AdjuntoxpreguntaRestController {
    private static final Logger log = LoggerFactory.getLogger(AdjuntoxpreguntaRestController.class);
    @Autowired
    private IBusinessDelegatorView businessDelegatorView;
    @Autowired
    private IAdjuntoxpreguntaMapper adjuntoxpreguntaMapper;

    @PostMapping(value = "/saveAdjuntoxpregunta")
    public void saveAdjuntoxpregunta(
        @RequestBody
    AdjuntoxpreguntaDTO adjuntoxpreguntaDTO) throws Exception {
        try {
            Adjuntoxpregunta adjuntoxpregunta = adjuntoxpreguntaMapper.adjuntoxpreguntaDTOToAdjuntoxpregunta(adjuntoxpreguntaDTO);

            businessDelegatorView.saveAdjuntoxpregunta(adjuntoxpregunta);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @DeleteMapping(value = "/deleteAdjuntoxpregunta/{adjpreId}")
    public void deleteAdjuntoxpregunta(@PathVariable("adjpreId")
    Long adjpreId) throws Exception {
        try {
            Adjuntoxpregunta adjuntoxpregunta = businessDelegatorView.getAdjuntoxpregunta(adjpreId);

            businessDelegatorView.deleteAdjuntoxpregunta(adjuntoxpregunta);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @PutMapping(value = "/updateAdjuntoxpregunta/")
    public void updateAdjuntoxpregunta(
        @RequestBody
    AdjuntoxpreguntaDTO adjuntoxpreguntaDTO) throws Exception {
        try {
            Adjuntoxpregunta adjuntoxpregunta = adjuntoxpreguntaMapper.adjuntoxpreguntaDTOToAdjuntoxpregunta(adjuntoxpreguntaDTO);

            businessDelegatorView.updateAdjuntoxpregunta(adjuntoxpregunta);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @GetMapping(value = "/getDataAdjuntoxpregunta")
    public List<AdjuntoxpreguntaDTO> getDataAdjuntoxpregunta()
        throws Exception {
        try {
            return businessDelegatorView.getDataAdjuntoxpregunta();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @GetMapping(value = "/getAdjuntoxpregunta/{adjpreId}")
    public AdjuntoxpreguntaDTO getAdjuntoxpregunta(
        @PathVariable("adjpreId")
    Long adjpreId) throws Exception {
        try {
            Adjuntoxpregunta adjuntoxpregunta = businessDelegatorView.getAdjuntoxpregunta(adjpreId);

            return adjuntoxpreguntaMapper.adjuntoxpreguntaToAdjuntoxpreguntaDTO(adjuntoxpregunta);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return null;
    }
}

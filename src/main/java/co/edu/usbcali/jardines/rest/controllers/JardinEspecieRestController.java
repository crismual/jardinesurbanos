package co.edu.usbcali.jardines.rest.controllers;

import co.edu.usbcali.jardines.dto.mapper.IJardinEspecieMapper;
import co.edu.usbcali.jardines.model.*;
import co.edu.usbcali.jardines.model.dto.JardinEspecieDTO;
import co.edu.usbcali.jardines.presentation.businessDelegate.IBusinessDelegatorView;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/jardinEspecie")
public class JardinEspecieRestController {
    private static final Logger log = LoggerFactory.getLogger(JardinEspecieRestController.class);
    @Autowired
    private IBusinessDelegatorView businessDelegatorView;
    @Autowired
    private IJardinEspecieMapper jardinEspecieMapper;

    @PostMapping(value = "/saveJardinEspecie")
    public void saveJardinEspecie(@RequestBody
    JardinEspecieDTO jardinEspecieDTO) throws Exception {
        try {
            JardinEspecie jardinEspecie = jardinEspecieMapper.jardinEspecieDTOToJardinEspecie(jardinEspecieDTO);

            businessDelegatorView.saveJardinEspecie(jardinEspecie);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @DeleteMapping(value = "/deleteJardinEspecie/{jarespId}")
    public void deleteJardinEspecie(@PathVariable("jarespId")
    Long jarespId) throws Exception {
        try {
            JardinEspecie jardinEspecie = businessDelegatorView.getJardinEspecie(jarespId);

            businessDelegatorView.deleteJardinEspecie(jardinEspecie);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @PutMapping(value = "/updateJardinEspecie/")
    public void updateJardinEspecie(
        @RequestBody
    JardinEspecieDTO jardinEspecieDTO) throws Exception {
        try {
            JardinEspecie jardinEspecie = jardinEspecieMapper.jardinEspecieDTOToJardinEspecie(jardinEspecieDTO);

            businessDelegatorView.updateJardinEspecie(jardinEspecie);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @GetMapping(value = "/getDataJardinEspecie")
    public List<JardinEspecieDTO> getDataJardinEspecie()
        throws Exception {
        try {
            return businessDelegatorView.getDataJardinEspecie();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @GetMapping(value = "/getJardinEspecie/{jarespId}")
    public JardinEspecieDTO getJardinEspecie(
        @PathVariable("jarespId")
    Long jarespId) throws Exception {
        try {
            JardinEspecie jardinEspecie = businessDelegatorView.getJardinEspecie(jarespId);

            return jardinEspecieMapper.jardinEspecieToJardinEspecieDTO(jardinEspecie);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return null;
    }
}

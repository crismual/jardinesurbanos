package co.edu.usbcali.jardines.rest.controllers;

import co.edu.usbcali.jardines.dto.mapper.IRespuestaMapper;
import co.edu.usbcali.jardines.model.*;
import co.edu.usbcali.jardines.model.dto.RespuestaDTO;
import co.edu.usbcali.jardines.presentation.businessDelegate.IBusinessDelegatorView;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/respuesta")
public class RespuestaRestController {
    private static final Logger log = LoggerFactory.getLogger(RespuestaRestController.class);
    @Autowired
    private IBusinessDelegatorView businessDelegatorView;
    @Autowired
    private IRespuestaMapper respuestaMapper;

    @PostMapping(value = "/saveRespuesta")
    public void saveRespuesta(@RequestBody
    RespuestaDTO respuestaDTO) throws Exception {
        try {
            Respuesta respuesta = respuestaMapper.respuestaDTOToRespuesta(respuestaDTO);

            businessDelegatorView.saveRespuesta(respuesta);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @DeleteMapping(value = "/deleteRespuesta/{respId}")
    public void deleteRespuesta(@PathVariable("respId")
    Long respId) throws Exception {
        try {
            Respuesta respuesta = businessDelegatorView.getRespuesta(respId);

            businessDelegatorView.deleteRespuesta(respuesta);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @PutMapping(value = "/updateRespuesta/")
    public void updateRespuesta(@RequestBody
    RespuestaDTO respuestaDTO) throws Exception {
        try {
            Respuesta respuesta = respuestaMapper.respuestaDTOToRespuesta(respuestaDTO);

            businessDelegatorView.updateRespuesta(respuesta);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @GetMapping(value = "/getDataRespuesta")
    public List<RespuestaDTO> getDataRespuesta() throws Exception {
        try {
            return businessDelegatorView.getDataRespuesta();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @GetMapping(value = "/getRespuesta/{respId}")
    public RespuestaDTO getRespuesta(@PathVariable("respId")
    Long respId) throws Exception {
        try {
            Respuesta respuesta = businessDelegatorView.getRespuesta(respId);

            return respuestaMapper.respuestaToRespuestaDTO(respuesta);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return null;
    }
}

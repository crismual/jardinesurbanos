package co.edu.usbcali.jardines.rest.controllers;

import co.edu.usbcali.jardines.dto.mapper.IEspecieMapper;
import co.edu.usbcali.jardines.model.*;
import co.edu.usbcali.jardines.model.dto.EspecieDTO;
import co.edu.usbcali.jardines.presentation.businessDelegate.IBusinessDelegatorView;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/especie")
public class EspecieRestController {
    private static final Logger log = LoggerFactory.getLogger(EspecieRestController.class);
    @Autowired
    private IBusinessDelegatorView businessDelegatorView;
    @Autowired
    private IEspecieMapper especieMapper;

    @PostMapping(value = "/saveEspecie")
    public void saveEspecie(@RequestBody
    EspecieDTO especieDTO) throws Exception {
        try {
            Especie especie = especieMapper.especieDTOToEspecie(especieDTO);

            businessDelegatorView.saveEspecie(especie);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @DeleteMapping(value = "/deleteEspecie/{espeId}")
    public void deleteEspecie(@PathVariable("espeId")
    Long espeId) throws Exception {
        try {
            Especie especie = businessDelegatorView.getEspecie(espeId);

            businessDelegatorView.deleteEspecie(especie);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @PutMapping(value = "/updateEspecie/")
    public void updateEspecie(@RequestBody
    EspecieDTO especieDTO) throws Exception {
        try {
            Especie especie = especieMapper.especieDTOToEspecie(especieDTO);

            businessDelegatorView.updateEspecie(especie);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @GetMapping(value = "/getDataEspecie")
    public List<EspecieDTO> getDataEspecie() throws Exception {
        try {
            return businessDelegatorView.getDataEspecie();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @GetMapping(value = "/getEspecie/{espeId}")
    public EspecieDTO getEspecie(@PathVariable("espeId")
    Long espeId) throws Exception {
        try {
            Especie especie = businessDelegatorView.getEspecie(espeId);

            return especieMapper.especieToEspecieDTO(especie);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return null;
    }
}

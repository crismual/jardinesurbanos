package co.edu.usbcali.jardines.rest.controllers;

import co.edu.usbcali.jardines.dto.mapper.IRespuestaxusuarioMapper;
import co.edu.usbcali.jardines.model.*;
import co.edu.usbcali.jardines.model.dto.RespuestaxusuarioDTO;
import co.edu.usbcali.jardines.presentation.businessDelegate.IBusinessDelegatorView;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/respuestaxusuario")
public class RespuestaxusuarioRestController {
    private static final Logger log = LoggerFactory.getLogger(RespuestaxusuarioRestController.class);
    @Autowired
    private IBusinessDelegatorView businessDelegatorView;
    @Autowired
    private IRespuestaxusuarioMapper respuestaxusuarioMapper;

    @PostMapping(value = "/saveRespuestaxusuario")
    public void saveRespuestaxusuario(
        @RequestBody
    RespuestaxusuarioDTO respuestaxusuarioDTO) throws Exception {
        try {
            Respuestaxusuario respuestaxusuario = respuestaxusuarioMapper.respuestaxusuarioDTOToRespuestaxusuario(respuestaxusuarioDTO);

            businessDelegatorView.saveRespuestaxusuario(respuestaxusuario);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @DeleteMapping(value = "/deleteRespuestaxusuario/{respusuId}")
    public void deleteRespuestaxusuario(
        @PathVariable("respusuId")
    Long respusuId) throws Exception {
        try {
            Respuestaxusuario respuestaxusuario = businessDelegatorView.getRespuestaxusuario(respusuId);

            businessDelegatorView.deleteRespuestaxusuario(respuestaxusuario);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @PutMapping(value = "/updateRespuestaxusuario/")
    public void updateRespuestaxusuario(
        @RequestBody
    RespuestaxusuarioDTO respuestaxusuarioDTO) throws Exception {
        try {
            Respuestaxusuario respuestaxusuario = respuestaxusuarioMapper.respuestaxusuarioDTOToRespuestaxusuario(respuestaxusuarioDTO);

            businessDelegatorView.updateRespuestaxusuario(respuestaxusuario);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @GetMapping(value = "/getDataRespuestaxusuario")
    public List<RespuestaxusuarioDTO> getDataRespuestaxusuario()
        throws Exception {
        try {
            return businessDelegatorView.getDataRespuestaxusuario();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @GetMapping(value = "/getRespuestaxusuario/{respusuId}")
    public RespuestaxusuarioDTO getRespuestaxusuario(
        @PathVariable("respusuId")
    Long respusuId) throws Exception {
        try {
            Respuestaxusuario respuestaxusuario = businessDelegatorView.getRespuestaxusuario(respusuId);

            return respuestaxusuarioMapper.respuestaxusuarioToRespuestaxusuarioDTO(respuestaxusuario);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return null;
    }
}

package co.edu.usbcali.jardines.rest.controllers;

import co.edu.usbcali.jardines.dto.mapper.IAuditoriaMapper;
import co.edu.usbcali.jardines.model.*;
import co.edu.usbcali.jardines.model.dto.AuditoriaDTO;
import co.edu.usbcali.jardines.presentation.businessDelegate.IBusinessDelegatorView;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/auditoria")
public class AuditoriaRestController {
    private static final Logger log = LoggerFactory.getLogger(AuditoriaRestController.class);
    @Autowired
    private IBusinessDelegatorView businessDelegatorView;
    @Autowired
    private IAuditoriaMapper auditoriaMapper;

    @PostMapping(value = "/saveAuditoria")
    public void saveAuditoria(@RequestBody
    AuditoriaDTO auditoriaDTO) throws Exception {
        try {
            Auditoria auditoria = auditoriaMapper.auditoriaDTOToAuditoria(auditoriaDTO);

            businessDelegatorView.saveAuditoria(auditoria);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @DeleteMapping(value = "/deleteAuditoria/{audId}")
    public void deleteAuditoria(@PathVariable("audId")
    Long audId) throws Exception {
        try {
            Auditoria auditoria = businessDelegatorView.getAuditoria(audId);

            businessDelegatorView.deleteAuditoria(auditoria);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @PutMapping(value = "/updateAuditoria/")
    public void updateAuditoria(@RequestBody
    AuditoriaDTO auditoriaDTO) throws Exception {
        try {
            Auditoria auditoria = auditoriaMapper.auditoriaDTOToAuditoria(auditoriaDTO);

            businessDelegatorView.updateAuditoria(auditoria);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @GetMapping(value = "/getDataAuditoria")
    public List<AuditoriaDTO> getDataAuditoria() throws Exception {
        try {
            return businessDelegatorView.getDataAuditoria();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw e;
        }
    }

    @GetMapping(value = "/getAuditoria/{audId}")
    public AuditoriaDTO getAuditoria(@PathVariable("audId")
    Long audId) throws Exception {
        try {
            Auditoria auditoria = businessDelegatorView.getAuditoria(audId);

            return auditoriaMapper.auditoriaToAuditoriaDTO(auditoria);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return null;
    }
}

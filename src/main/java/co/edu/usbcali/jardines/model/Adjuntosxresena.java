package co.edu.usbcali.jardines.model;

import org.hibernate.validator.constraints.*;

import java.util.Date;

import javax.persistence.*;

import javax.validation.constraints.*;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
@Entity
@Table(name = "adjuntosxresena", schema = "public")
public class Adjuntosxresena implements java.io.Serializable {
    @NotNull
    private Long adresId;
    @NotNull
    private Resena resena;
    private String descripcion;
    private String estado;
    private Date fechaCreacion;
    private Date fechaModificacion;
    private String urlFotoRes;
    private String usuarioCreador;
    private String usuarioModificador;

    public Adjuntosxresena() {
    }

    public Adjuntosxresena(Long adresId, String descripcion, String estado,
        Date fechaCreacion, Date fechaModificacion, Resena resena,
        String urlFotoRes, String usuarioCreador, String usuarioModificador) {
        this.adresId = adresId;
        this.resena = resena;
        this.descripcion = descripcion;
        this.estado = estado;
        this.fechaCreacion = fechaCreacion;
        this.fechaModificacion = fechaModificacion;
        this.urlFotoRes = urlFotoRes;
        this.usuarioCreador = usuarioCreador;
        this.usuarioModificador = usuarioModificador;
    }

    @Id
    @Column(name = "adres_id", unique = true, nullable = false)
    public Long getAdresId() {
        return this.adresId;
    }

    public void setAdresId(Long adresId) {
        this.adresId = adresId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "res_id")
    public Resena getResena() {
        return this.resena;
    }

    public void setResena(Resena resena) {
        this.resena = resena;
    }

    @Column(name = "descripcion")
    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Column(name = "estado")
    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Column(name = "fecha_creacion")
    public Date getFechaCreacion() {
        return this.fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    @Column(name = "fecha_modificacion")
    public Date getFechaModificacion() {
        return this.fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    @Column(name = "url_foto_res")
    public String getUrlFotoRes() {
        return this.urlFotoRes;
    }

    public void setUrlFotoRes(String urlFotoRes) {
        this.urlFotoRes = urlFotoRes;
    }

    @Column(name = "usuario_creador")
    public String getUsuarioCreador() {
        return this.usuarioCreador;
    }

    public void setUsuarioCreador(String usuarioCreador) {
        this.usuarioCreador = usuarioCreador;
    }

    @Column(name = "usuario_modificador")
    public String getUsuarioModificador() {
        return this.usuarioModificador;
    }

    public void setUsuarioModificador(String usuarioModificador) {
        this.usuarioModificador = usuarioModificador;
    }
}

package co.edu.usbcali.jardines.model;

import org.hibernate.validator.constraints.*;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import javax.validation.constraints.*;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
@Entity
@Table(name = "especie", schema = "public")
public class Especie implements java.io.Serializable {
    @NotNull
    private Long espeId;
    @NotNull
    private Usuario usuario;
    private String altitud;
    private String cuidados;
    private String descripcion;
    @NotNull
    @NotEmpty
    @Size(max = 5)
    private String estado;
    private Date fechaCreacion;
    private Date fechaModificacion;
    private String flor;
    private String fruto;
    private String nombreCientifico;
    private String nombreComun;
    private String plagas;
    private String raiz;
    private String riego;
    private String silueta;
    private String sol;
    private String suelo;
    private String usuarioCreador;
    private String usuarioModificador;
    private Set<JardinEspecie> jardinEspecies = new HashSet<JardinEspecie>(0);

    public Especie() {
    }

    public Especie(Long espeId, String altitud, String cuidados,
        String descripcion, String estado, Date fechaCreacion,
        Date fechaModificacion, String flor, String fruto,
        Set<JardinEspecie> jardinEspecies, String nombreCientifico,
        String nombreComun, String plagas, String raiz, String riego,
        String silueta, String sol, String suelo, Usuario usuario,
        String usuarioCreador, String usuarioModificador) {
        this.espeId = espeId;
        this.usuario = usuario;
        this.altitud = altitud;
        this.cuidados = cuidados;
        this.descripcion = descripcion;
        this.estado = estado;
        this.fechaCreacion = fechaCreacion;
        this.fechaModificacion = fechaModificacion;
        this.flor = flor;
        this.fruto = fruto;
        this.nombreCientifico = nombreCientifico;
        this.nombreComun = nombreComun;
        this.plagas = plagas;
        this.raiz = raiz;
        this.riego = riego;
        this.silueta = silueta;
        this.sol = sol;
        this.suelo = suelo;
        this.usuarioCreador = usuarioCreador;
        this.usuarioModificador = usuarioModificador;
        this.jardinEspecies = jardinEspecies;
    }

    @Id
    @Column(name = "espe_id", unique = true, nullable = false)
    public Long getEspeId() {
        return this.espeId;
    }

    public void setEspeId(Long espeId) {
        this.espeId = espeId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usu_id")
    public Usuario getUsuario() {
        return this.usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    @Column(name = "altitud")
    public String getAltitud() {
        return this.altitud;
    }

    public void setAltitud(String altitud) {
        this.altitud = altitud;
    }

    @Column(name = "cuidados")
    public String getCuidados() {
        return this.cuidados;
    }

    public void setCuidados(String cuidados) {
        this.cuidados = cuidados;
    }

    @Column(name = "descripcion")
    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Column(name = "estado", nullable = false)
    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Column(name = "fecha_creacion")
    public Date getFechaCreacion() {
        return this.fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    @Column(name = "fecha_modificacion")
    public Date getFechaModificacion() {
        return this.fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    @Column(name = "flor")
    public String getFlor() {
        return this.flor;
    }

    public void setFlor(String flor) {
        this.flor = flor;
    }

    @Column(name = "fruto")
    public String getFruto() {
        return this.fruto;
    }

    public void setFruto(String fruto) {
        this.fruto = fruto;
    }

    @Column(name = "nombre_cientifico")
    public String getNombreCientifico() {
        return this.nombreCientifico;
    }

    public void setNombreCientifico(String nombreCientifico) {
        this.nombreCientifico = nombreCientifico;
    }

    @Column(name = "nombre_comun")
    public String getNombreComun() {
        return this.nombreComun;
    }

    public void setNombreComun(String nombreComun) {
        this.nombreComun = nombreComun;
    }

    @Column(name = "plagas")
    public String getPlagas() {
        return this.plagas;
    }

    public void setPlagas(String plagas) {
        this.plagas = plagas;
    }

    @Column(name = "raiz")
    public String getRaiz() {
        return this.raiz;
    }

    public void setRaiz(String raiz) {
        this.raiz = raiz;
    }

    @Column(name = "riego")
    public String getRiego() {
        return this.riego;
    }

    public void setRiego(String riego) {
        this.riego = riego;
    }

    @Column(name = "silueta")
    public String getSilueta() {
        return this.silueta;
    }

    public void setSilueta(String silueta) {
        this.silueta = silueta;
    }

    @Column(name = "sol")
    public String getSol() {
        return this.sol;
    }

    public void setSol(String sol) {
        this.sol = sol;
    }

    @Column(name = "suelo")
    public String getSuelo() {
        return this.suelo;
    }

    public void setSuelo(String suelo) {
        this.suelo = suelo;
    }

    @Column(name = "usuario_creador")
    public String getUsuarioCreador() {
        return this.usuarioCreador;
    }

    public void setUsuarioCreador(String usuarioCreador) {
        this.usuarioCreador = usuarioCreador;
    }

    @Column(name = "usuario_modificador")
    public String getUsuarioModificador() {
        return this.usuarioModificador;
    }

    public void setUsuarioModificador(String usuarioModificador) {
        this.usuarioModificador = usuarioModificador;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "especie")
    public Set<JardinEspecie> getJardinEspecies() {
        return this.jardinEspecies;
    }

    public void setJardinEspecies(Set<JardinEspecie> jardinEspecies) {
        this.jardinEspecies = jardinEspecies;
    }
}

package co.edu.usbcali.jardines.model.control;

import co.edu.usbcali.jardines.model.Respuestaxusuario;
import co.edu.usbcali.jardines.model.dto.RespuestaxusuarioDTO;

import java.math.BigDecimal;

import java.util.*;
import java.util.Date;
import java.util.List;
import java.util.Set;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public interface IRespuestaxusuarioLogic {
    public List<Respuestaxusuario> getRespuestaxusuario()
        throws Exception;

    /**
         * Save an new Respuestaxusuario entity
         */
    public void saveRespuestaxusuario(Respuestaxusuario entity)
        throws Exception;

    /**
         * Delete an existing Respuestaxusuario entity
         *
         */
    public void deleteRespuestaxusuario(Respuestaxusuario entity)
        throws Exception;

    /**
        * Update an existing Respuestaxusuario entity
        *
        */
    public void updateRespuestaxusuario(Respuestaxusuario entity)
        throws Exception;

    /**
         * Load an existing Respuestaxusuario entity
         *
         */
    public Respuestaxusuario getRespuestaxusuario(Long respusuId)
        throws Exception;

    public List<Respuestaxusuario> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<Respuestaxusuario> findPageRespuestaxusuario(
        String sortColumnName, boolean sortAscending, int startRow,
        int maxResults) throws Exception;

    public Long findTotalNumberRespuestaxusuario() throws Exception;

    public List<RespuestaxusuarioDTO> getDataRespuestaxusuario()
        throws Exception;

    public void validateRespuestaxusuario(Respuestaxusuario respuestaxusuario)
        throws Exception;
}

package co.edu.usbcali.jardines.model;

import org.hibernate.validator.constraints.*;

import java.util.Date;

import javax.persistence.*;

import javax.validation.constraints.*;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
@Entity
@Table(name = "auditoria", schema = "public")
public class Auditoria implements java.io.Serializable {
    @NotNull
    private Long audId;
    @NotNull
    private Usuario usuario;
    private String descripcion;
    private String estado;
    private Date fechaCreacion;
    private Date fechaModificacion;
    private String modificacion;
    private String usuarioCreador;
    private String usuarioModificador;

    public Auditoria() {
    }

    public Auditoria(Long audId, String descripcion, String estado,
        Date fechaCreacion, Date fechaModificacion, String modificacion,
        Usuario usuario, String usuarioCreador, String usuarioModificador) {
        this.audId = audId;
        this.usuario = usuario;
        this.descripcion = descripcion;
        this.estado = estado;
        this.fechaCreacion = fechaCreacion;
        this.fechaModificacion = fechaModificacion;
        this.modificacion = modificacion;
        this.usuarioCreador = usuarioCreador;
        this.usuarioModificador = usuarioModificador;
    }

    @Id
    @Column(name = "aud_id", unique = true, nullable = false)
    public Long getAudId() {
        return this.audId;
    }

    public void setAudId(Long audId) {
        this.audId = audId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usu_id")
    public Usuario getUsuario() {
        return this.usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    @Column(name = "descripcion")
    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Column(name = "estado")
    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Column(name = "fecha_creacion")
    public Date getFechaCreacion() {
        return this.fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    @Column(name = "fecha_modificacion")
    public Date getFechaModificacion() {
        return this.fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    @Column(name = "modificacion")
    public String getModificacion() {
        return this.modificacion;
    }

    public void setModificacion(String modificacion) {
        this.modificacion = modificacion;
    }

    @Column(name = "usuario_creador")
    public String getUsuarioCreador() {
        return this.usuarioCreador;
    }

    public void setUsuarioCreador(String usuarioCreador) {
        this.usuarioCreador = usuarioCreador;
    }

    @Column(name = "usuario_modificador")
    public String getUsuarioModificador() {
        return this.usuarioModificador;
    }

    public void setUsuarioModificador(String usuarioModificador) {
        this.usuarioModificador = usuarioModificador;
    }
}

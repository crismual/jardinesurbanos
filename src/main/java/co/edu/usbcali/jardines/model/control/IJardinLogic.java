package co.edu.usbcali.jardines.model.control;

import co.edu.usbcali.jardines.model.Jardin;
import co.edu.usbcali.jardines.model.dto.JardinDTO;

import java.math.BigDecimal;

import java.util.*;
import java.util.Date;
import java.util.List;
import java.util.Set;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public interface IJardinLogic {
    public List<Jardin> getJardin() throws Exception;

    /**
         * Save an new Jardin entity
         */
    public void saveJardin(Jardin entity) throws Exception;

    /**
         * Delete an existing Jardin entity
         *
         */
    public void deleteJardin(Jardin entity) throws Exception;

    /**
        * Update an existing Jardin entity
        *
        */
    public void updateJardin(Jardin entity) throws Exception;

    /**
         * Load an existing Jardin entity
         *
         */
    public Jardin getJardin(Long jarId) throws Exception;

    public List<Jardin> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<Jardin> findPageJardin(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception;

    public Long findTotalNumberJardin() throws Exception;

    public List<JardinDTO> getDataJardin() throws Exception;

    public void validateJardin(Jardin jardin) throws Exception;
}

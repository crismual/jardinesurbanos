package co.edu.usbcali.jardines.model.dto;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

import java.sql.*;

import java.util.Date;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public class AdjuntoxpreguntaDTO implements Serializable {
    private static final long serialVersionUID = 1L;
    private static final Logger log = LoggerFactory.getLogger(AdjuntoxpreguntaDTO.class);
    private Long adjpreId;
    private String descripcion;
    private String estado;
    private Date fechaCreacion;
    private Date fechaModificacion;
    private String urlFotoPregunta;
    private String usuarioCreador;
    private String usuarioModificador;
    private Long preId_Pregunta;

    public Long getAdjpreId() {
        return adjpreId;
    }

    public void setAdjpreId(Long adjpreId) {
        this.adjpreId = adjpreId;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public String getUrlFotoPregunta() {
        return urlFotoPregunta;
    }

    public void setUrlFotoPregunta(String urlFotoPregunta) {
        this.urlFotoPregunta = urlFotoPregunta;
    }

    public String getUsuarioCreador() {
        return usuarioCreador;
    }

    public void setUsuarioCreador(String usuarioCreador) {
        this.usuarioCreador = usuarioCreador;
    }

    public String getUsuarioModificador() {
        return usuarioModificador;
    }

    public void setUsuarioModificador(String usuarioModificador) {
        this.usuarioModificador = usuarioModificador;
    }

    public Long getPreId_Pregunta() {
        return preId_Pregunta;
    }

    public void setPreId_Pregunta(Long preId_Pregunta) {
        this.preId_Pregunta = preId_Pregunta;
    }
}

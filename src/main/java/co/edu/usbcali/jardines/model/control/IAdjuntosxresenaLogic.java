package co.edu.usbcali.jardines.model.control;

import co.edu.usbcali.jardines.model.Adjuntosxresena;
import co.edu.usbcali.jardines.model.dto.AdjuntosxresenaDTO;

import java.math.BigDecimal;

import java.util.*;
import java.util.Date;
import java.util.List;
import java.util.Set;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public interface IAdjuntosxresenaLogic {
    public List<Adjuntosxresena> getAdjuntosxresena() throws Exception;

    /**
         * Save an new Adjuntosxresena entity
         */
    public void saveAdjuntosxresena(Adjuntosxresena entity)
        throws Exception;

    /**
         * Delete an existing Adjuntosxresena entity
         *
         */
    public void deleteAdjuntosxresena(Adjuntosxresena entity)
        throws Exception;

    /**
        * Update an existing Adjuntosxresena entity
        *
        */
    public void updateAdjuntosxresena(Adjuntosxresena entity)
        throws Exception;

    /**
         * Load an existing Adjuntosxresena entity
         *
         */
    public Adjuntosxresena getAdjuntosxresena(Long adresId)
        throws Exception;

    public List<Adjuntosxresena> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<Adjuntosxresena> findPageAdjuntosxresena(
        String sortColumnName, boolean sortAscending, int startRow,
        int maxResults) throws Exception;

    public Long findTotalNumberAdjuntosxresena() throws Exception;

    public List<AdjuntosxresenaDTO> getDataAdjuntosxresena()
        throws Exception;

    public void validateAdjuntosxresena(Adjuntosxresena adjuntosxresena)
        throws Exception;
}

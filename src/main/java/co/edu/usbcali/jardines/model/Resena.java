package co.edu.usbcali.jardines.model;

import org.hibernate.validator.constraints.*;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import javax.validation.constraints.*;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
@Entity
@Table(name = "resena", schema = "public")
public class Resena implements java.io.Serializable {
    @NotNull
    private Long resId;
    @NotNull
    private Jardin jardin;
    @NotNull
    private Usuario usuario;
    private String descripcion;
    private String estado;
    private String estadoJardin;
    private Date fechaCreacion;
    private Date fechaModificacion;
    private String usuarioCreador;
    private String usuarioModificador;
    private Set<Adjuntosxresena> adjuntosxresenas = new HashSet<Adjuntosxresena>(0);

    public Resena() {
    }

    public Resena(Long resId, Set<Adjuntosxresena> adjuntosxresenas,
        String descripcion, String estado, String estadoJardin,
        Date fechaCreacion, Date fechaModificacion, Jardin jardin,
        Usuario usuario, String usuarioCreador, String usuarioModificador) {
        this.resId = resId;
        this.jardin = jardin;
        this.usuario = usuario;
        this.descripcion = descripcion;
        this.estado = estado;
        this.estadoJardin = estadoJardin;
        this.fechaCreacion = fechaCreacion;
        this.fechaModificacion = fechaModificacion;
        this.usuarioCreador = usuarioCreador;
        this.usuarioModificador = usuarioModificador;
        this.adjuntosxresenas = adjuntosxresenas;
    }

    @Id
    @Column(name = "res_id", unique = true, nullable = false)
    public Long getResId() {
        return this.resId;
    }

    public void setResId(Long resId) {
        this.resId = resId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "jar_id")
    public Jardin getJardin() {
        return this.jardin;
    }

    public void setJardin(Jardin jardin) {
        this.jardin = jardin;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usu_id")
    public Usuario getUsuario() {
        return this.usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    @Column(name = "descripcion")
    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Column(name = "estado")
    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Column(name = "estado_jardin")
    public String getEstadoJardin() {
        return this.estadoJardin;
    }

    public void setEstadoJardin(String estadoJardin) {
        this.estadoJardin = estadoJardin;
    }

    @Column(name = "fecha_creacion")
    public Date getFechaCreacion() {
        return this.fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    @Column(name = "fecha_modificacion")
    public Date getFechaModificacion() {
        return this.fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    @Column(name = "usuario_creador")
    public String getUsuarioCreador() {
        return this.usuarioCreador;
    }

    public void setUsuarioCreador(String usuarioCreador) {
        this.usuarioCreador = usuarioCreador;
    }

    @Column(name = "usuario_modificador")
    public String getUsuarioModificador() {
        return this.usuarioModificador;
    }

    public void setUsuarioModificador(String usuarioModificador) {
        this.usuarioModificador = usuarioModificador;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "resena")
    public Set<Adjuntosxresena> getAdjuntosxresenas() {
        return this.adjuntosxresenas;
    }

    public void setAdjuntosxresenas(Set<Adjuntosxresena> adjuntosxresenas) {
        this.adjuntosxresenas = adjuntosxresenas;
    }
}

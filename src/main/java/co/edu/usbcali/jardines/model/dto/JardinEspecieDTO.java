package co.edu.usbcali.jardines.model.dto;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

import java.sql.*;

import java.util.Date;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public class JardinEspecieDTO implements Serializable {
    private static final long serialVersionUID = 1L;
    private static final Logger log = LoggerFactory.getLogger(JardinEspecieDTO.class);
    private String descripcion;
    private String estado;
    private Date fechaCreacion;
    private Date fechaModificacion;
    private Long jarespId;
    private String usuarioCreador;
    private String usuarioModificador;
    private Long espeId_Especie;
    private Long jarId_Jardin;

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public Long getJarespId() {
        return jarespId;
    }

    public void setJarespId(Long jarespId) {
        this.jarespId = jarespId;
    }

    public String getUsuarioCreador() {
        return usuarioCreador;
    }

    public void setUsuarioCreador(String usuarioCreador) {
        this.usuarioCreador = usuarioCreador;
    }

    public String getUsuarioModificador() {
        return usuarioModificador;
    }

    public void setUsuarioModificador(String usuarioModificador) {
        this.usuarioModificador = usuarioModificador;
    }

    public Long getEspeId_Especie() {
        return espeId_Especie;
    }

    public void setEspeId_Especie(Long espeId_Especie) {
        this.espeId_Especie = espeId_Especie;
    }

    public Long getJarId_Jardin() {
        return jarId_Jardin;
    }

    public void setJarId_Jardin(Long jarId_Jardin) {
        this.jarId_Jardin = jarId_Jardin;
    }
}

package co.edu.usbcali.jardines.model;

import org.hibernate.validator.constraints.*;

import java.util.Date;

import javax.persistence.*;

import javax.validation.constraints.*;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
@Entity
@Table(name = "respuestaxusuario", schema = "public")
public class Respuestaxusuario implements java.io.Serializable {
    @NotNull
    private Long respusuId;
    @NotNull
    private Jardin jardin;
    @NotNull
    private Respuesta respuesta;
    @NotNull
    private Usuario usuario;
    private String descripcion;
    private String descripcionPregunta;
    private String estado;
    private Date fechaCreacion;
    private Date fechaModificacion;
    private String usuarioCreador;
    private String usuarioModificador;

    public Respuestaxusuario() {
    }

    public Respuestaxusuario(Long respusuId, String descripcion,
        String descripcionPregunta, String estado, Date fechaCreacion,
        Date fechaModificacion, Jardin jardin, Respuesta respuesta,
        Usuario usuario, String usuarioCreador, String usuarioModificador) {
        this.respusuId = respusuId;
        this.jardin = jardin;
        this.respuesta = respuesta;
        this.usuario = usuario;
        this.descripcion = descripcion;
        this.descripcionPregunta = descripcionPregunta;
        this.estado = estado;
        this.fechaCreacion = fechaCreacion;
        this.fechaModificacion = fechaModificacion;
        this.usuarioCreador = usuarioCreador;
        this.usuarioModificador = usuarioModificador;
    }

    @Id
    @Column(name = "respusu_id", unique = true, nullable = false)
    public Long getRespusuId() {
        return this.respusuId;
    }

    public void setRespusuId(Long respusuId) {
        this.respusuId = respusuId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "jar_id")
    public Jardin getJardin() {
        return this.jardin;
    }

    public void setJardin(Jardin jardin) {
        this.jardin = jardin;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "resp_id")
    public Respuesta getRespuesta() {
        return this.respuesta;
    }

    public void setRespuesta(Respuesta respuesta) {
        this.respuesta = respuesta;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usu_id")
    public Usuario getUsuario() {
        return this.usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    @Column(name = "descripcion")
    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Column(name = "descripcion_pregunta")
    public String getDescripcionPregunta() {
        return this.descripcionPregunta;
    }

    public void setDescripcionPregunta(String descripcionPregunta) {
        this.descripcionPregunta = descripcionPregunta;
    }

    @Column(name = "estado")
    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Column(name = "fecha_creacion")
    public Date getFechaCreacion() {
        return this.fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    @Column(name = "fecha_modificacion")
    public Date getFechaModificacion() {
        return this.fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    @Column(name = "usuario_creador")
    public String getUsuarioCreador() {
        return this.usuarioCreador;
    }

    public void setUsuarioCreador(String usuarioCreador) {
        this.usuarioCreador = usuarioCreador;
    }

    @Column(name = "usuario_modificador")
    public String getUsuarioModificador() {
        return this.usuarioModificador;
    }

    public void setUsuarioModificador(String usuarioModificador) {
        this.usuarioModificador = usuarioModificador;
    }
}

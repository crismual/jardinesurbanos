package co.edu.usbcali.jardines.model.control;

import co.edu.usbcali.jardines.model.Especie;
import co.edu.usbcali.jardines.model.dto.EspecieDTO;

import java.math.BigDecimal;

import java.util.*;
import java.util.Date;
import java.util.List;
import java.util.Set;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public interface IEspecieLogic {
    public List<Especie> getEspecie() throws Exception;

    /**
         * Save an new Especie entity
         */
    public void saveEspecie(Especie entity) throws Exception;

    /**
         * Delete an existing Especie entity
         *
         */
    public void deleteEspecie(Especie entity) throws Exception;

    /**
        * Update an existing Especie entity
        *
        */
    public void updateEspecie(Especie entity) throws Exception;

    /**
         * Load an existing Especie entity
         *
         */
    public Especie getEspecie(Long espeId) throws Exception;

    public List<Especie> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<Especie> findPageEspecie(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception;

    public Long findTotalNumberEspecie() throws Exception;

    public List<EspecieDTO> getDataEspecie() throws Exception;

    public void validateEspecie(Especie especie) throws Exception;
}

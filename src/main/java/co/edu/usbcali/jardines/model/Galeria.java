package co.edu.usbcali.jardines.model;

import org.hibernate.validator.constraints.*;

import java.util.Date;

import javax.persistence.*;

import javax.validation.constraints.*;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
@Entity
@Table(name = "galeria", schema = "public")
public class Galeria implements java.io.Serializable {
    @NotNull
    private GaleriaId id;
    @NotNull
    private Jardin jardin;
    @NotNull
    private Usuario usuario;

    public Galeria() {
    }

    public Galeria(GaleriaId id, Jardin jardin, Usuario usuario) {
        this.id = id;
        this.jardin = jardin;
        this.usuario = usuario;
    }

    @EmbeddedId
    @AttributeOverrides({@AttributeOverride(name = "galId",column = @Column(name = "gal_id",nullable = false)
        )
        , @AttributeOverride(name = "fechaCreacion",column = @Column(name = "fecha_creacion",nullable = false)
        )
        , @AttributeOverride(name = "fechaModificacion",column = @Column(name = "fecha_modificacion",nullable = false)
        )
        , @AttributeOverride(name = "usuarioCreador",column = @Column(name = "usuario_creador",nullable = false)
        )
        , @AttributeOverride(name = "usuarioModificador",column = @Column(name = "usuario_modificador",nullable = false)
        )
        , @AttributeOverride(name = "estado",column = @Column(name = "estado",nullable = false)
        )
        , @AttributeOverride(name = "descripcion",column = @Column(name = "descripcion",nullable = false)
        )
        , @AttributeOverride(name = "urlFotoGaleria",column = @Column(name = "url_foto_galeria",nullable = false)
        )
        , @AttributeOverride(name = "jardinJarId",column = @Column(name = "jardin_jar_id",nullable = false)
        )
        , @AttributeOverride(name = "usuarioUsuId",column = @Column(name = "usuario_usu_id",nullable = false)
        )
    })
    public GaleriaId getId() {
        return this.id;
    }

    public void setId(GaleriaId id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "jar_id")
    public Jardin getJardin() {
        return this.jardin;
    }

    public void setJardin(Jardin jardin) {
        this.jardin = jardin;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usu_id")
    public Usuario getUsuario() {
        return this.usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
}

package co.edu.usbcali.jardines.model;

import org.hibernate.validator.constraints.*;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import javax.validation.constraints.*;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
@Entity
@Table(name = "respuesta", schema = "public")
public class Respuesta implements java.io.Serializable {
    @NotNull
    private Long respId;
    @NotNull
    private Pregunta pregunta;
    private String descripcion;
    private String descripcionRespuesta;
    private String estado;
    private Date fechaCreacion;
    private Date fechaModificacion;
    private String usuarioCreador;
    private String usuarioModificador;
    private Set<Respuestaxusuario> respuestaxusuarios = new HashSet<Respuestaxusuario>(0);

    public Respuesta() {
    }

    public Respuesta(Long respId, String descripcion,
        String descripcionRespuesta, String estado, Date fechaCreacion,
        Date fechaModificacion, Pregunta pregunta,
        Set<Respuestaxusuario> respuestaxusuarios, String usuarioCreador,
        String usuarioModificador) {
        this.respId = respId;
        this.pregunta = pregunta;
        this.descripcion = descripcion;
        this.descripcionRespuesta = descripcionRespuesta;
        this.estado = estado;
        this.fechaCreacion = fechaCreacion;
        this.fechaModificacion = fechaModificacion;
        this.usuarioCreador = usuarioCreador;
        this.usuarioModificador = usuarioModificador;
        this.respuestaxusuarios = respuestaxusuarios;
    }

    @Id
    @Column(name = "resp_id", unique = true, nullable = false)
    public Long getRespId() {
        return this.respId;
    }

    public void setRespId(Long respId) {
        this.respId = respId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pre_id")
    public Pregunta getPregunta() {
        return this.pregunta;
    }

    public void setPregunta(Pregunta pregunta) {
        this.pregunta = pregunta;
    }

    @Column(name = "descripcion")
    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Column(name = "descripcion_respuesta")
    public String getDescripcionRespuesta() {
        return this.descripcionRespuesta;
    }

    public void setDescripcionRespuesta(String descripcionRespuesta) {
        this.descripcionRespuesta = descripcionRespuesta;
    }

    @Column(name = "estado")
    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Column(name = "fecha_creacion")
    public Date getFechaCreacion() {
        return this.fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    @Column(name = "fecha_modificacion")
    public Date getFechaModificacion() {
        return this.fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    @Column(name = "usuario_creador")
    public String getUsuarioCreador() {
        return this.usuarioCreador;
    }

    public void setUsuarioCreador(String usuarioCreador) {
        this.usuarioCreador = usuarioCreador;
    }

    @Column(name = "usuario_modificador")
    public String getUsuarioModificador() {
        return this.usuarioModificador;
    }

    public void setUsuarioModificador(String usuarioModificador) {
        this.usuarioModificador = usuarioModificador;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "respuesta")
    public Set<Respuestaxusuario> getRespuestaxusuarios() {
        return this.respuestaxusuarios;
    }

    public void setRespuestaxusuarios(Set<Respuestaxusuario> respuestaxusuarios) {
        this.respuestaxusuarios = respuestaxusuarios;
    }
}

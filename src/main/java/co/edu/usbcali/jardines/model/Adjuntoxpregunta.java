package co.edu.usbcali.jardines.model;

import org.hibernate.validator.constraints.*;

import java.util.Date;

import javax.persistence.*;

import javax.validation.constraints.*;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
@Entity
@Table(name = "adjuntoxpregunta", schema = "public")
public class Adjuntoxpregunta implements java.io.Serializable {
    @NotNull
    private Long adjpreId;
    @NotNull
    private Pregunta pregunta;
    private String descripcion;
    private String estado;
    private Date fechaCreacion;
    private Date fechaModificacion;
    private String urlFotoPregunta;
    private String usuarioCreador;
    private String usuarioModificador;

    public Adjuntoxpregunta() {
    }

    public Adjuntoxpregunta(Long adjpreId, String descripcion, String estado,
        Date fechaCreacion, Date fechaModificacion, Pregunta pregunta,
        String urlFotoPregunta, String usuarioCreador, String usuarioModificador) {
        this.adjpreId = adjpreId;
        this.pregunta = pregunta;
        this.descripcion = descripcion;
        this.estado = estado;
        this.fechaCreacion = fechaCreacion;
        this.fechaModificacion = fechaModificacion;
        this.urlFotoPregunta = urlFotoPregunta;
        this.usuarioCreador = usuarioCreador;
        this.usuarioModificador = usuarioModificador;
    }

    @Id
    @Column(name = "adjpre_id", unique = true, nullable = false)
    public Long getAdjpreId() {
        return this.adjpreId;
    }

    public void setAdjpreId(Long adjpreId) {
        this.adjpreId = adjpreId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pre_id")
    public Pregunta getPregunta() {
        return this.pregunta;
    }

    public void setPregunta(Pregunta pregunta) {
        this.pregunta = pregunta;
    }

    @Column(name = "descripcion")
    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Column(name = "estado")
    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Column(name = "fecha_creacion")
    public Date getFechaCreacion() {
        return this.fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    @Column(name = "fecha_modificacion")
    public Date getFechaModificacion() {
        return this.fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    @Column(name = "url_foto_pregunta")
    public String getUrlFotoPregunta() {
        return this.urlFotoPregunta;
    }

    public void setUrlFotoPregunta(String urlFotoPregunta) {
        this.urlFotoPregunta = urlFotoPregunta;
    }

    @Column(name = "usuario_creador")
    public String getUsuarioCreador() {
        return this.usuarioCreador;
    }

    public void setUsuarioCreador(String usuarioCreador) {
        this.usuarioCreador = usuarioCreador;
    }

    @Column(name = "usuario_modificador")
    public String getUsuarioModificador() {
        return this.usuarioModificador;
    }

    public void setUsuarioModificador(String usuarioModificador) {
        this.usuarioModificador = usuarioModificador;
    }
}

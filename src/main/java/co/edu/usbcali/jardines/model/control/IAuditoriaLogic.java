package co.edu.usbcali.jardines.model.control;

import co.edu.usbcali.jardines.model.Auditoria;
import co.edu.usbcali.jardines.model.dto.AuditoriaDTO;

import java.math.BigDecimal;

import java.util.*;
import java.util.Date;
import java.util.List;
import java.util.Set;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public interface IAuditoriaLogic {
    public List<Auditoria> getAuditoria() throws Exception;

    /**
         * Save an new Auditoria entity
         */
    public void saveAuditoria(Auditoria entity) throws Exception;

    /**
         * Delete an existing Auditoria entity
         *
         */
    public void deleteAuditoria(Auditoria entity) throws Exception;

    /**
        * Update an existing Auditoria entity
        *
        */
    public void updateAuditoria(Auditoria entity) throws Exception;

    /**
         * Load an existing Auditoria entity
         *
         */
    public Auditoria getAuditoria(Long audId) throws Exception;

    public List<Auditoria> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<Auditoria> findPageAuditoria(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception;

    public Long findTotalNumberAuditoria() throws Exception;

    public List<AuditoriaDTO> getDataAuditoria() throws Exception;

    public void validateAuditoria(Auditoria auditoria)
        throws Exception;
}

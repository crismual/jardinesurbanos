package co.edu.usbcali.jardines.model;

import org.hibernate.validator.constraints.*;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import javax.validation.constraints.*;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
@Entity
@Table(name = "pregunta", schema = "public")
public class Pregunta implements java.io.Serializable {
    @NotNull
    private Long preId;
    private String descripcion;
    private String descripcionPregunta;
    private String estado;
    private Date fechaCreacion;
    private Date fechaModificacion;
    private String usuarioCreador;
    private String usuarioModificador;
    private Set<Adjuntoxpregunta> adjuntoxpreguntas = new HashSet<Adjuntoxpregunta>(0);
    private Set<Respuesta> respuestas = new HashSet<Respuesta>(0);

    public Pregunta() {
    }

    public Pregunta(Long preId, Set<Adjuntoxpregunta> adjuntoxpreguntas,
        String descripcion, String descripcionPregunta, String estado,
        Date fechaCreacion, Date fechaModificacion, Set<Respuesta> respuestas,
        String usuarioCreador, String usuarioModificador) {
        this.preId = preId;
        this.descripcion = descripcion;
        this.descripcionPregunta = descripcionPregunta;
        this.estado = estado;
        this.fechaCreacion = fechaCreacion;
        this.fechaModificacion = fechaModificacion;
        this.usuarioCreador = usuarioCreador;
        this.usuarioModificador = usuarioModificador;
        this.adjuntoxpreguntas = adjuntoxpreguntas;
        this.respuestas = respuestas;
    }

    @Id
    @Column(name = "pre_id", unique = true, nullable = false)
    public Long getPreId() {
        return this.preId;
    }

    public void setPreId(Long preId) {
        this.preId = preId;
    }

    @Column(name = "descripcion")
    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Column(name = "descripcion_pregunta")
    public String getDescripcionPregunta() {
        return this.descripcionPregunta;
    }

    public void setDescripcionPregunta(String descripcionPregunta) {
        this.descripcionPregunta = descripcionPregunta;
    }

    @Column(name = "estado")
    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Column(name = "fecha_creacion")
    public Date getFechaCreacion() {
        return this.fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    @Column(name = "fecha_modificacion")
    public Date getFechaModificacion() {
        return this.fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    @Column(name = "usuario_creador")
    public String getUsuarioCreador() {
        return this.usuarioCreador;
    }

    public void setUsuarioCreador(String usuarioCreador) {
        this.usuarioCreador = usuarioCreador;
    }

    @Column(name = "usuario_modificador")
    public String getUsuarioModificador() {
        return this.usuarioModificador;
    }

    public void setUsuarioModificador(String usuarioModificador) {
        this.usuarioModificador = usuarioModificador;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "pregunta")
    public Set<Adjuntoxpregunta> getAdjuntoxpreguntas() {
        return this.adjuntoxpreguntas;
    }

    public void setAdjuntoxpreguntas(Set<Adjuntoxpregunta> adjuntoxpreguntas) {
        this.adjuntoxpreguntas = adjuntoxpreguntas;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "pregunta")
    public Set<Respuesta> getRespuestas() {
        return this.respuestas;
    }

    public void setRespuestas(Set<Respuesta> respuestas) {
        this.respuestas = respuestas;
    }
}

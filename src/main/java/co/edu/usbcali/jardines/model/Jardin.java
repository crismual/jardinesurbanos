package co.edu.usbcali.jardines.model;

import org.hibernate.validator.constraints.*;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import javax.validation.constraints.*;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
@Entity
@Table(name = "jardin", schema = "public")
public class Jardin implements java.io.Serializable {
    @NotNull
    private Long jarId;
    @NotNull
    private Usuario usuario;
    private String altitud;
    private String barrio;
    private String descripcion;
    private String direccion;
    private String estado;
    private Date fechaCreacion;
    private Date fechaModificacion;
    private String latitud;
    private String nombreJardin;
    private String usuarioCreador;
    private String usuarioModificador;
    private String zona;
    private Set<Galeria> galerias = new HashSet<Galeria>(0);
    private Set<JardinEspecie> jardinEspecies = new HashSet<JardinEspecie>(0);
    private Set<Resena> resenas = new HashSet<Resena>(0);
    private Set<Respuestaxusuario> respuestaxusuarios = new HashSet<Respuestaxusuario>(0);

    public Jardin() {
    }

    public Jardin(Long jarId, String altitud, String barrio,
        String descripcion, String direccion, String estado,
        Date fechaCreacion, Date fechaModificacion, Set<Galeria> galerias,
        Set<JardinEspecie> jardinEspecies, String latitud, String nombreJardin,
        Set<Resena> resenas, Set<Respuestaxusuario> respuestaxusuarios,
        Usuario usuario, String usuarioCreador, String usuarioModificador,
        String zona) {
        this.jarId = jarId;
        this.usuario = usuario;
        this.altitud = altitud;
        this.barrio = barrio;
        this.descripcion = descripcion;
        this.direccion = direccion;
        this.estado = estado;
        this.fechaCreacion = fechaCreacion;
        this.fechaModificacion = fechaModificacion;
        this.latitud = latitud;
        this.nombreJardin = nombreJardin;
        this.usuarioCreador = usuarioCreador;
        this.usuarioModificador = usuarioModificador;
        this.zona = zona;
        this.galerias = galerias;
        this.jardinEspecies = jardinEspecies;
        this.resenas = resenas;
        this.respuestaxusuarios = respuestaxusuarios;
    }

    @Id
    @Column(name = "jar_id", unique = true, nullable = false)
    public Long getJarId() {
        return this.jarId;
    }

    public void setJarId(Long jarId) {
        this.jarId = jarId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usu_id")
    public Usuario getUsuario() {
        return this.usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    @Column(name = "altitud")
    public String getAltitud() {
        return this.altitud;
    }

    public void setAltitud(String altitud) {
        this.altitud = altitud;
    }

    @Column(name = "barrio")
    public String getBarrio() {
        return this.barrio;
    }

    public void setBarrio(String barrio) {
        this.barrio = barrio;
    }

    @Column(name = "descripcion")
    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Column(name = "direccion")
    public String getDireccion() {
        return this.direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Column(name = "estado")
    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Column(name = "fecha_creacion")
    public Date getFechaCreacion() {
        return this.fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    @Column(name = "fecha_modificacion")
    public Date getFechaModificacion() {
        return this.fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    @Column(name = "latitud")
    public String getLatitud() {
        return this.latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    @Column(name = "nombre_jardin")
    public String getNombreJardin() {
        return this.nombreJardin;
    }

    public void setNombreJardin(String nombreJardin) {
        this.nombreJardin = nombreJardin;
    }

    @Column(name = "usuario_creador")
    public String getUsuarioCreador() {
        return this.usuarioCreador;
    }

    public void setUsuarioCreador(String usuarioCreador) {
        this.usuarioCreador = usuarioCreador;
    }

    @Column(name = "usuario_modificador")
    public String getUsuarioModificador() {
        return this.usuarioModificador;
    }

    public void setUsuarioModificador(String usuarioModificador) {
        this.usuarioModificador = usuarioModificador;
    }

    @Column(name = "zona")
    public String getZona() {
        return this.zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "jardin")
    public Set<Galeria> getGalerias() {
        return this.galerias;
    }

    public void setGalerias(Set<Galeria> galerias) {
        this.galerias = galerias;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "jardin")
    public Set<JardinEspecie> getJardinEspecies() {
        return this.jardinEspecies;
    }

    public void setJardinEspecies(Set<JardinEspecie> jardinEspecies) {
        this.jardinEspecies = jardinEspecies;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "jardin")
    public Set<Resena> getResenas() {
        return this.resenas;
    }

    public void setResenas(Set<Resena> resenas) {
        this.resenas = resenas;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "jardin")
    public Set<Respuestaxusuario> getRespuestaxusuarios() {
        return this.respuestaxusuarios;
    }

    public void setRespuestaxusuarios(Set<Respuestaxusuario> respuestaxusuarios) {
        this.respuestaxusuarios = respuestaxusuarios;
    }
}

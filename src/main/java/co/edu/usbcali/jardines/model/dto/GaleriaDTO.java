package co.edu.usbcali.jardines.model.dto;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

import java.sql.*;

import java.util.Date;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public class GaleriaDTO implements Serializable {
    private static final long serialVersionUID = 1L;
    private static final Logger log = LoggerFactory.getLogger(GaleriaDTO.class);
    private Long galId;
    private Date fechaCreacion;
    private Date fechaModificacion;
    private String usuarioCreador;
    private String usuarioModificador;
    private String estado;
    private String descripcion;
    private String urlFotoGaleria;
    private Long jardinJarId;
    private Long usuarioUsuId;
    private Long jarId_Jardin;
    private Long usuId_Usuario;

    public Long getGalId() {
        return galId;
    }

    public void setGalId(Long galId) {
        this.galId = galId;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public String getUsuarioCreador() {
        return usuarioCreador;
    }

    public void setUsuarioCreador(String usuarioCreador) {
        this.usuarioCreador = usuarioCreador;
    }

    public String getUsuarioModificador() {
        return usuarioModificador;
    }

    public void setUsuarioModificador(String usuarioModificador) {
        this.usuarioModificador = usuarioModificador;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getUrlFotoGaleria() {
        return urlFotoGaleria;
    }

    public void setUrlFotoGaleria(String urlFotoGaleria) {
        this.urlFotoGaleria = urlFotoGaleria;
    }

    public Long getJardinJarId() {
        return jardinJarId;
    }

    public void setJardinJarId(Long jardinJarId) {
        this.jardinJarId = jardinJarId;
    }

    public Long getUsuarioUsuId() {
        return usuarioUsuId;
    }

    public void setUsuarioUsuId(Long usuarioUsuId) {
        this.usuarioUsuId = usuarioUsuId;
    }

    public Long getJarId_Jardin() {
        return jarId_Jardin;
    }

    public void setJarId_Jardin(Long jarId_Jardin) {
        this.jarId_Jardin = jarId_Jardin;
    }

    public Long getUsuId_Usuario() {
        return usuId_Usuario;
    }

    public void setUsuId_Usuario(Long usuId_Usuario) {
        this.usuId_Usuario = usuId_Usuario;
    }
}

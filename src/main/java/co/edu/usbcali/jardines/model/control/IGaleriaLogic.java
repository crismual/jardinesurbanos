package co.edu.usbcali.jardines.model.control;

import co.edu.usbcali.jardines.model.Galeria;
import co.edu.usbcali.jardines.model.GaleriaId;
import co.edu.usbcali.jardines.model.dto.GaleriaDTO;

import java.math.BigDecimal;

import java.util.*;
import java.util.Date;
import java.util.List;
import java.util.Set;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public interface IGaleriaLogic {
    public List<Galeria> getGaleria() throws Exception;

    /**
         * Save an new Galeria entity
         */
    public void saveGaleria(Galeria entity) throws Exception;

    /**
         * Delete an existing Galeria entity
         *
         */
    public void deleteGaleria(Galeria entity) throws Exception;

    /**
        * Update an existing Galeria entity
        *
        */
    public void updateGaleria(Galeria entity) throws Exception;

    /**
         * Load an existing Galeria entity
         *
         */
    public Galeria getGaleria(GaleriaId id) throws Exception;

    public List<Galeria> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<Galeria> findPageGaleria(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception;

    public Long findTotalNumberGaleria() throws Exception;

    public List<GaleriaDTO> getDataGaleria() throws Exception;

    public void validateGaleria(Galeria galeria) throws Exception;
}

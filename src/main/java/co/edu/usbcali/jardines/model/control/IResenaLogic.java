package co.edu.usbcali.jardines.model.control;

import co.edu.usbcali.jardines.model.Resena;
import co.edu.usbcali.jardines.model.dto.ResenaDTO;

import java.math.BigDecimal;

import java.util.*;
import java.util.Date;
import java.util.List;
import java.util.Set;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public interface IResenaLogic {
    public List<Resena> getResena() throws Exception;

    /**
         * Save an new Resena entity
         */
    public void saveResena(Resena entity) throws Exception;

    /**
         * Delete an existing Resena entity
         *
         */
    public void deleteResena(Resena entity) throws Exception;

    /**
        * Update an existing Resena entity
        *
        */
    public void updateResena(Resena entity) throws Exception;

    /**
         * Load an existing Resena entity
         *
         */
    public Resena getResena(Long resId) throws Exception;

    public List<Resena> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<Resena> findPageResena(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception;

    public Long findTotalNumberResena() throws Exception;

    public List<ResenaDTO> getDataResena() throws Exception;

    public void validateResena(Resena resena) throws Exception;
}

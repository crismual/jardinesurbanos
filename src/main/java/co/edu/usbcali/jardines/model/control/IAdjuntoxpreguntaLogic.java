package co.edu.usbcali.jardines.model.control;

import co.edu.usbcali.jardines.model.Adjuntoxpregunta;
import co.edu.usbcali.jardines.model.dto.AdjuntoxpreguntaDTO;

import java.math.BigDecimal;

import java.util.*;
import java.util.Date;
import java.util.List;
import java.util.Set;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public interface IAdjuntoxpreguntaLogic {
    public List<Adjuntoxpregunta> getAdjuntoxpregunta()
        throws Exception;

    /**
         * Save an new Adjuntoxpregunta entity
         */
    public void saveAdjuntoxpregunta(Adjuntoxpregunta entity)
        throws Exception;

    /**
         * Delete an existing Adjuntoxpregunta entity
         *
         */
    public void deleteAdjuntoxpregunta(Adjuntoxpregunta entity)
        throws Exception;

    /**
        * Update an existing Adjuntoxpregunta entity
        *
        */
    public void updateAdjuntoxpregunta(Adjuntoxpregunta entity)
        throws Exception;

    /**
         * Load an existing Adjuntoxpregunta entity
         *
         */
    public Adjuntoxpregunta getAdjuntoxpregunta(Long adjpreId)
        throws Exception;

    public List<Adjuntoxpregunta> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<Adjuntoxpregunta> findPageAdjuntoxpregunta(
        String sortColumnName, boolean sortAscending, int startRow,
        int maxResults) throws Exception;

    public Long findTotalNumberAdjuntoxpregunta() throws Exception;

    public List<AdjuntoxpreguntaDTO> getDataAdjuntoxpregunta()
        throws Exception;

    public void validateAdjuntoxpregunta(Adjuntoxpregunta adjuntoxpregunta)
        throws Exception;
}

package co.edu.usbcali.jardines.model.control;

import co.edu.usbcali.jardines.model.JardinEspecie;
import co.edu.usbcali.jardines.model.dto.JardinEspecieDTO;

import java.math.BigDecimal;

import java.util.*;
import java.util.Date;
import java.util.List;
import java.util.Set;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public interface IJardinEspecieLogic {
    public List<JardinEspecie> getJardinEspecie() throws Exception;

    /**
         * Save an new JardinEspecie entity
         */
    public void saveJardinEspecie(JardinEspecie entity)
        throws Exception;

    /**
         * Delete an existing JardinEspecie entity
         *
         */
    public void deleteJardinEspecie(JardinEspecie entity)
        throws Exception;

    /**
        * Update an existing JardinEspecie entity
        *
        */
    public void updateJardinEspecie(JardinEspecie entity)
        throws Exception;

    /**
         * Load an existing JardinEspecie entity
         *
         */
    public JardinEspecie getJardinEspecie(Long jarespId)
        throws Exception;

    public List<JardinEspecie> findByCriteria(Object[] variables,
        Object[] variablesBetween, Object[] variablesBetweenDates)
        throws Exception;

    public List<JardinEspecie> findPageJardinEspecie(String sortColumnName,
        boolean sortAscending, int startRow, int maxResults)
        throws Exception;

    public Long findTotalNumberJardinEspecie() throws Exception;

    public List<JardinEspecieDTO> getDataJardinEspecie()
        throws Exception;

    public void validateJardinEspecie(JardinEspecie jardinEspecie)
        throws Exception;
}

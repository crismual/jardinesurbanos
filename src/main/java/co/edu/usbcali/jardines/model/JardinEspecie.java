package co.edu.usbcali.jardines.model;

import org.hibernate.validator.constraints.*;

import java.util.Date;

import javax.persistence.*;

import javax.validation.constraints.*;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
@Entity
@Table(name = "jardin_especie", schema = "public")
public class JardinEspecie implements java.io.Serializable {
    @NotNull
    private Long jarespId;
    @NotNull
    private Especie especie;
    @NotNull
    private Jardin jardin;
    private String descripcion;
    private String estado;
    private Date fechaCreacion;
    private Date fechaModificacion;
    private String usuarioCreador;
    private String usuarioModificador;

    public JardinEspecie() {
    }

    public JardinEspecie(Long jarespId, String descripcion, Especie especie,
        String estado, Date fechaCreacion, Date fechaModificacion,
        Jardin jardin, String usuarioCreador, String usuarioModificador) {
        this.jarespId = jarespId;
        this.especie = especie;
        this.jardin = jardin;
        this.descripcion = descripcion;
        this.estado = estado;
        this.fechaCreacion = fechaCreacion;
        this.fechaModificacion = fechaModificacion;
        this.usuarioCreador = usuarioCreador;
        this.usuarioModificador = usuarioModificador;
    }

    @Id
    @Column(name = "jaresp_id", unique = true, nullable = false)
    public Long getJarespId() {
        return this.jarespId;
    }

    public void setJarespId(Long jarespId) {
        this.jarespId = jarespId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "espe_id")
    public Especie getEspecie() {
        return this.especie;
    }

    public void setEspecie(Especie especie) {
        this.especie = especie;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "jar_id")
    public Jardin getJardin() {
        return this.jardin;
    }

    public void setJardin(Jardin jardin) {
        this.jardin = jardin;
    }

    @Column(name = "descripcion")
    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Column(name = "estado")
    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Column(name = "fecha_creacion")
    public Date getFechaCreacion() {
        return this.fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    @Column(name = "fecha_modificacion")
    public Date getFechaModificacion() {
        return this.fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    @Column(name = "usuario_creador")
    public String getUsuarioCreador() {
        return this.usuarioCreador;
    }

    public void setUsuarioCreador(String usuarioCreador) {
        this.usuarioCreador = usuarioCreador;
    }

    @Column(name = "usuario_modificador")
    public String getUsuarioModificador() {
        return this.usuarioModificador;
    }

    public void setUsuarioModificador(String usuarioModificador) {
        this.usuarioModificador = usuarioModificador;
    }
}

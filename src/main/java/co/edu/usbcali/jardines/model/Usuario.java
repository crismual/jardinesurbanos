package co.edu.usbcali.jardines.model;

import org.hibernate.validator.constraints.*;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import javax.validation.constraints.*;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
@Entity
@Table(name = "usuario", schema = "public")
public class Usuario implements java.io.Serializable {
    @NotNull
    private Long usuId;
    @NotNull
    private TipoUsuario tipoUsuario;
    private String apellidos;
    private String celular;
    private String contrasena;
    private String correo;
    private String descripcion;
    private String edad;
    private String estado;
    private Date fechaCreacion;
    private Date fechaModificacion;
    private Date fechaNacimiento;
    private String nombres;
    private String sexo;
    private String usuarioCreador;
    private String usuarioModificador;
    private Set<Auditoria> auditorias = new HashSet<Auditoria>(0);
    private Set<Especie> especies = new HashSet<Especie>(0);
    private Set<Galeria> galerias = new HashSet<Galeria>(0);
    private Set<Jardin> jardins = new HashSet<Jardin>(0);
    private Set<Resena> resenas = new HashSet<Resena>(0);
    private Set<Respuestaxusuario> respuestaxusuarios = new HashSet<Respuestaxusuario>(0);

    public Usuario() {
    }

    public Usuario(Long usuId, String apellidos, Set<Auditoria> auditorias,
        String celular, String contrasena, String correo, String descripcion,
        String edad, Set<Especie> especies, String estado, Date fechaCreacion,
        Date fechaModificacion, Date fechaNacimiento, Set<Galeria> galerias,
        Set<Jardin> jardins, String nombres, Set<Resena> resenas,
        Set<Respuestaxusuario> respuestaxusuarios, String sexo,
        TipoUsuario tipoUsuario, String usuarioCreador,
        String usuarioModificador) {
        this.usuId = usuId;
        this.tipoUsuario = tipoUsuario;
        this.apellidos = apellidos;
        this.celular = celular;
        this.contrasena = contrasena;
        this.correo = correo;
        this.descripcion = descripcion;
        this.edad = edad;
        this.estado = estado;
        this.fechaCreacion = fechaCreacion;
        this.fechaModificacion = fechaModificacion;
        this.fechaNacimiento = fechaNacimiento;
        this.nombres = nombres;
        this.sexo = sexo;
        this.usuarioCreador = usuarioCreador;
        this.usuarioModificador = usuarioModificador;
        this.auditorias = auditorias;
        this.especies = especies;
        this.galerias = galerias;
        this.jardins = jardins;
        this.resenas = resenas;
        this.respuestaxusuarios = respuestaxusuarios;
    }

    @Id
    @Column(name = "usu_id", unique = true, nullable = false)
    public Long getUsuId() {
        return this.usuId;
    }

    public void setUsuId(Long usuId) {
        this.usuId = usuId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tius_id")
    public TipoUsuario getTipoUsuario() {
        return this.tipoUsuario;
    }

    public void setTipoUsuario(TipoUsuario tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }

    @Column(name = "apellidos")
    public String getApellidos() {
        return this.apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    @Column(name = "celular")
    public String getCelular() {
        return this.celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    @Column(name = "contrasena")
    public String getContrasena() {
        return this.contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    @Column(name = "correo")
    public String getCorreo() {
        return this.correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    @Column(name = "descripcion")
    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Column(name = "edad")
    public String getEdad() {
        return this.edad;
    }

    public void setEdad(String edad) {
        this.edad = edad;
    }

    @Column(name = "estado")
    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Column(name = "fecha_creacion")
    public Date getFechaCreacion() {
        return this.fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    @Column(name = "fecha_modificacion")
    public Date getFechaModificacion() {
        return this.fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    @Column(name = "fecha_nacimiento")
    public Date getFechaNacimiento() {
        return this.fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    @Column(name = "nombres")
    public String getNombres() {
        return this.nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    @Column(name = "sexo")
    public String getSexo() {
        return this.sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    @Column(name = "usuario_creador")
    public String getUsuarioCreador() {
        return this.usuarioCreador;
    }

    public void setUsuarioCreador(String usuarioCreador) {
        this.usuarioCreador = usuarioCreador;
    }

    @Column(name = "usuario_modificador")
    public String getUsuarioModificador() {
        return this.usuarioModificador;
    }

    public void setUsuarioModificador(String usuarioModificador) {
        this.usuarioModificador = usuarioModificador;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "usuario")
    public Set<Auditoria> getAuditorias() {
        return this.auditorias;
    }

    public void setAuditorias(Set<Auditoria> auditorias) {
        this.auditorias = auditorias;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "usuario")
    public Set<Especie> getEspecies() {
        return this.especies;
    }

    public void setEspecies(Set<Especie> especies) {
        this.especies = especies;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "usuario")
    public Set<Galeria> getGalerias() {
        return this.galerias;
    }

    public void setGalerias(Set<Galeria> galerias) {
        this.galerias = galerias;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "usuario")
    public Set<Jardin> getJardins() {
        return this.jardins;
    }

    public void setJardins(Set<Jardin> jardins) {
        this.jardins = jardins;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "usuario")
    public Set<Resena> getResenas() {
        return this.resenas;
    }

    public void setResenas(Set<Resena> resenas) {
        this.resenas = resenas;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "usuario")
    public Set<Respuestaxusuario> getRespuestaxusuarios() {
        return this.respuestaxusuarios;
    }

    public void setRespuestaxusuarios(Set<Respuestaxusuario> respuestaxusuarios) {
        this.respuestaxusuarios = respuestaxusuarios;
    }
}

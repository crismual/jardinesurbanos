package co.edu.usbcali.jardines.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Id;

import javax.validation.constraints.*;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
@Embeddable
public class GaleriaId implements java.io.Serializable {
    @NotNull
    private Long galId;
    @NotNull
    private Date fechaCreacion;
    @NotNull
    private Date fechaModificacion;
    @NotNull
    private String usuarioCreador;
    @NotNull
    private String usuarioModificador;
    @NotNull
    private String estado;
    @NotNull
    private String descripcion;
    @NotNull
    private String urlFotoGaleria;
    @NotNull
    private Long jardinJarId;
    @NotNull
    private Long usuarioUsuId;

    public GaleriaId() {
    }

    @Column(name = "gal_id", nullable = false)
    public Long getGalId() {
        return this.galId;
    }

    public void setGalId(Long galId) {
        this.galId = galId;
    }

    @Column(name = "fecha_creacion", nullable = false)
    public Date getFechaCreacion() {
        return this.fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    @Column(name = "fecha_modificacion", nullable = false)
    public Date getFechaModificacion() {
        return this.fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    @Column(name = "usuario_creador", nullable = false)
    public String getUsuarioCreador() {
        return this.usuarioCreador;
    }

    public void setUsuarioCreador(String usuarioCreador) {
        this.usuarioCreador = usuarioCreador;
    }

    @Column(name = "usuario_modificador", nullable = false)
    public String getUsuarioModificador() {
        return this.usuarioModificador;
    }

    public void setUsuarioModificador(String usuarioModificador) {
        this.usuarioModificador = usuarioModificador;
    }

    @Column(name = "estado", nullable = false)
    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Column(name = "descripcion", nullable = false)
    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Column(name = "url_foto_galeria", nullable = false)
    public String getUrlFotoGaleria() {
        return this.urlFotoGaleria;
    }

    public void setUrlFotoGaleria(String urlFotoGaleria) {
        this.urlFotoGaleria = urlFotoGaleria;
    }

    @Column(name = "jardin_jar_id", nullable = false)
    public Long getJardinJarId() {
        return this.jardinJarId;
    }

    public void setJardinJarId(Long jardinJarId) {
        this.jardinJarId = jardinJarId;
    }

    @Column(name = "usuario_usu_id", nullable = false)
    public Long getUsuarioUsuId() {
        return this.usuarioUsuId;
    }

    public void setUsuarioUsuId(Long usuarioUsuId) {
        this.usuarioUsuId = usuarioUsuId;
    }

    public boolean equals(Object other) {
        if ((this == other)) {
            return true;
        }

        if ((other == null)) {
            return false;
        }

        if (!(other instanceof GaleriaId)) {
            return false;
        }

        GaleriaId castOther = (GaleriaId) other;

        return ((this.getGalId() == castOther.getGalId()) ||
        ((this.getGalId() != null) && (castOther.getGalId() != null) &&
        this.getGalId().equals(castOther.getGalId()))) &&
        ((this.getFechaCreacion() == castOther.getFechaCreacion()) ||
        ((this.getFechaCreacion() != null) &&
        (castOther.getFechaCreacion() != null) &&
        this.getFechaCreacion().equals(castOther.getFechaCreacion()))) &&
        ((this.getFechaModificacion() == castOther.getFechaModificacion()) ||
        ((this.getFechaModificacion() != null) &&
        (castOther.getFechaModificacion() != null) &&
        this.getFechaModificacion().equals(castOther.getFechaModificacion()))) &&
        ((this.getUsuarioCreador() == castOther.getUsuarioCreador()) ||
        ((this.getUsuarioCreador() != null) &&
        (castOther.getUsuarioCreador() != null) &&
        this.getUsuarioCreador().equals(castOther.getUsuarioCreador()))) &&
        ((this.getUsuarioModificador() == castOther.getUsuarioModificador()) ||
        ((this.getUsuarioModificador() != null) &&
        (castOther.getUsuarioModificador() != null) &&
        this.getUsuarioModificador().equals(castOther.getUsuarioModificador()))) &&
        ((this.getEstado() == castOther.getEstado()) ||
        ((this.getEstado() != null) && (castOther.getEstado() != null) &&
        this.getEstado().equals(castOther.getEstado()))) &&
        ((this.getDescripcion() == castOther.getDescripcion()) ||
        ((this.getDescripcion() != null) &&
        (castOther.getDescripcion() != null) &&
        this.getDescripcion().equals(castOther.getDescripcion()))) &&
        ((this.getUrlFotoGaleria() == castOther.getUrlFotoGaleria()) ||
        ((this.getUrlFotoGaleria() != null) &&
        (castOther.getUrlFotoGaleria() != null) &&
        this.getUrlFotoGaleria().equals(castOther.getUrlFotoGaleria()))) &&
        ((this.getJardinJarId() == castOther.getJardinJarId()) ||
        ((this.getJardinJarId() != null) &&
        (castOther.getJardinJarId() != null) &&
        this.getJardinJarId().equals(castOther.getJardinJarId()))) &&
        ((this.getUsuarioUsuId() == castOther.getUsuarioUsuId()) ||
        ((this.getUsuarioUsuId() != null) &&
        (castOther.getUsuarioUsuId() != null) &&
        this.getUsuarioUsuId().equals(castOther.getUsuarioUsuId())));
    }

    public int hashCode() {
        int result = 17;

        result = (37 * result) +
            ((getGalId() == null) ? 0 : this.getGalId().hashCode());
        result = (37 * result) +
            ((getFechaCreacion() == null) ? 0 : this.getFechaCreacion()
                                                    .hashCode());
        result = (37 * result) +
            ((getFechaModificacion() == null) ? 0
                                              : this.getFechaModificacion()
                                                    .hashCode());
        result = (37 * result) +
            ((getUsuarioCreador() == null) ? 0
                                           : this.getUsuarioCreador().hashCode());
        result = (37 * result) +
            ((getUsuarioModificador() == null) ? 0
                                               : this.getUsuarioModificador()
                                                     .hashCode());
        result = (37 * result) +
            ((getEstado() == null) ? 0 : this.getEstado().hashCode());
        result = (37 * result) +
            ((getDescripcion() == null) ? 0 : this.getDescripcion().hashCode());
        result = (37 * result) +
            ((getUrlFotoGaleria() == null) ? 0
                                           : this.getUrlFotoGaleria().hashCode());
        result = (37 * result) +
            ((getJardinJarId() == null) ? 0 : this.getJardinJarId().hashCode());
        result = (37 * result) +
            ((getUsuarioUsuId() == null) ? 0 : this.getUsuarioUsuId().hashCode());

        return result;
    }
}

package co.edu.usbcali.jardines.model.dto;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

import java.sql.*;

import java.util.Date;


/**
* @author Zathura Code Generator http://zathuracode.org
* www.zathuracode.org
*
*/
public class ResenaDTO implements Serializable {
    private static final long serialVersionUID = 1L;
    private static final Logger log = LoggerFactory.getLogger(ResenaDTO.class);
    private String descripcion;
    private String estado;
    private String estadoJardin;
    private Date fechaCreacion;
    private Date fechaModificacion;
    private Long resId;
    private String usuarioCreador;
    private String usuarioModificador;
    private Long jarId_Jardin;
    private Long usuId_Usuario;

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getEstadoJardin() {
        return estadoJardin;
    }

    public void setEstadoJardin(String estadoJardin) {
        this.estadoJardin = estadoJardin;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public Long getResId() {
        return resId;
    }

    public void setResId(Long resId) {
        this.resId = resId;
    }

    public String getUsuarioCreador() {
        return usuarioCreador;
    }

    public void setUsuarioCreador(String usuarioCreador) {
        this.usuarioCreador = usuarioCreador;
    }

    public String getUsuarioModificador() {
        return usuarioModificador;
    }

    public void setUsuarioModificador(String usuarioModificador) {
        this.usuarioModificador = usuarioModificador;
    }

    public Long getJarId_Jardin() {
        return jarId_Jardin;
    }

    public void setJarId_Jardin(Long jarId_Jardin) {
        this.jarId_Jardin = jarId_Jardin;
    }

    public Long getUsuId_Usuario() {
        return usuId_Usuario;
    }

    public void setUsuId_Usuario(Long usuId_Usuario) {
        this.usuId_Usuario = usuId_Usuario;
    }
}
